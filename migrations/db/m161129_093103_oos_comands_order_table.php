<?php
require_once('vendor/stdlib-migrations/Migrations/src/Migrations/Tools/MigrationTpl.php');
use Migrations\Tools\MigrationTpl;

class m161129_093103_oos_comands_order_table extends MigrationTpl
{
    public function safeUp()
    {
        //Ввод номера телефона
        $this->insert('oos_comands', [
            'id' => 400,
            'title_en' => 'Send phone number',
            'title_es' => 'Enviar el número de teléfono',
            'title_it' => 'Invia numero di telefono',
            'title_fr' => 'Envoyer un numéro de téléphone',
            'title_de' => 'Telefonnummer senden',
            'title_ru' => 'Отправить номер телефона',
            'parent' => 400,
            'description_en' => 'To start, please enter your phone number',
            'description_es' => 'Para iniciar, por favor, indique Vuestro número de teléfono',
            'description_it' => 'Per iniziare, inserisci il numero di telefono',
            'description_fr' => "Pour commencer le travail, induquez Votre numéro de téléphone, s'il vous plait",
            'description_de' => 'Vor dem Start geben Sie bitte Ihre Telefonnummer an',
            'description_ru' => 'Для начала работы, пожалуйста, укажите Ваш номер телефона',
            'request_contact' => 1,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 3,
            'is_prev' => 0,
            'next' => 0
        ]);

        $this->insert('oos_comands', [
            'id' => 402,
            'title_en' => '',
            'title_es' => '',
            'title_it' => '',
            'title_fr' => '',
            'title_de' => '',
            'title_ru' => '',
            'parent' => 400,
            'description_en' => 'To start, please enter your phone number',
            'description_es' => 'Para iniciar, por favor, indique Vuestro número de teléfono',
            'description_it' => 'Per iniziare, inserisci il numero di telefono',
            'description_fr' => "Pour commencer le travail, induquez Votre numéro de téléphone, s'il vous plait	",
            'description_de' => 'Vor dem Start geben Sie bitte Ihre Telefonnummer an',
            'description_ru' => 'Для начала работы, пожалуйста, укажите Ваш номер телефона',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 1,
            'sort' => 2,
            'is_prev' => 0,
            'next' => 0
        ]);

        $this->insert('oos_comands', [
            'id' => 403,
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => 400,
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 1,
            'is_prev' => 0,
            'is_main' => 1
        ]);

        //Ввод кода подтверждения

        $this->insert('oos_comands', [
            'id' => 405,
            'title_en' => '',
            'title_es' => '',
            'title_it' => '',
            'title_fr' => '',
            'title_de' => '',
            'title_ru' => '',
            'parent' => 405,
            'description_en' => 'An sms with verification code has been sent to your phone number. To proceed, please enter this code.',
            'description_es' => 'A vuestro número de teléfono se le ha enviado un mensaje con el código. Para continuar, indique el código que recibió.',
            'description_it' => 'Un SMS con il codice di verifica è stato inviato al numero di telefono fornito. Si prega di inserire il codice per procedere.',
            'description_fr' => "Un sms avec le code est envoyé à Votre numéro. Pour continuer il est nécessaire d'indiquer ce code.",
            'description_de' => 'Sie erhalten in Kürze einen SMS-Code auf Ihr Mobiltelefon. Zum weiteren Vorgehen geben Sie bitte diesen Code ein.	',
            'description_ru' => 'На Ваш номер телефона отправлено сообщение с кодом. Для продолжения необходимо указать этот код',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 1,
            'sort' => 1,
            'is_prev' => 0,
            'next' => 0
        ]);

        $this->insert('oos_comands', [
            'id' => 406,
            'title_en' => 'Send code once again',
            'title_es' => 'Enviar el código de nuevo',
            'title_it' => 'Reinvia codice',
            'title_fr' => 'Envoyer le code encore une fois',
            'title_de' => 'Code erneut senden',
            'title_ru' => 'Отправить код ещё раз',
            'parent' => 405,
            'description_en' => 'An sms with verification code has been sent to your phone number. To proceed, please enter this code.',
            'description_es' => 'A vuestro número de teléfono se le ha enviado un mensaje con el código. Para continuar, indique el código que recibió.',
            'description_it' => 'Un SMS con il codice di verifica è stato inviato al numero di telefono fornito. Si prega di inserire il codice per procedere.',
            'description_fr' => "Un sms avec le code est envoyé à Votre numéro. Pour continuer il est nécessaire d'indiquer ce code.",
            'description_de' => 'Sie erhalten in Kürze einen SMS-Code auf Ihr Mobiltelefon. Zum weiteren Vorgehen geben Sie bitte diesen Code ein.	',
            'description_ru' => 'На Ваш номер телефона отправлено сообщение с кодом. Для продолжения необходимо указать этот код',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 3,
            'is_prev' => 0,
            'next' => 0
        ]);

        $this->insert('oos_comands', [
            'id' => 407,
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => 405,
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 2,
            'is_prev' => 0,
            'is_main' => 1
        ]);

        //Заказ автомобиля
        $this->insert('oos_comands', [
            'id' => '500',
            'title_en' => '',
            'title_es' => '',
            'title_it' => '',
            'title_fr' => '',
            'title_de' => '',
            'title_ru' => '',
            'parent' => '2',
            'description_en' => 'Please specify destination address',
            'description_es' => 'Por favor, especifique la dirección de destino',
            'description_it' => "Inserisci l'indirizzo di destinazione",
            'description_fr' => "Indiquez, s'il vous plait, l'adresse de destination",
            'description_de' => 'Geben Sie bitte die Zieladresse an',
            'description_ru' => 'Укажите, пожалуйста, адрес места назначения',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 1,
            'sort' => 803,
            'is_prev' => 0,
            'next' => 503
        ]);

        $this->insert('oos_comands', [
            'id' => '501',
            'title_en' => 'Send coordinates',
            'title_es' => 'Enviar coordenadas',
            'title_it' => 'Invia posizione',
            'title_fr' => 'Envoyer des coordonnées',
            'title_de' => 'Koordinaten senden',
            'title_ru' => 'Отправить координаты',
            'parent' => '2',
            'description_en' => 'Please specify destination address',
            'description_es' => 'Por favor, especifique la dirección de destino',
            'description_it' => "Inserisci l'indirizzo di destinazione",
            'description_fr' => "Indiquez, s'il vous plait, l'adresse de destination",
            'description_de' => 'Geben Sie bitte die Zieladresse an',
            'description_ru' => 'Укажите, пожалуйста, адрес места назначения',
            'request_contact' => 0,
            'request_location' => 1,
            'is_read' => 0,
            'sort' => 806,
            'is_prev' => 0,
            'next' => 503
        ]);

        $this->insert('oos_comands', [
            'id' => '502',
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => '2',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 805,
            'is_prev' => 1
        ]);

        $this->insert('oos_comands', [
            'id' => '504',
            'title_en' => '',
            'title_es' => '',
            'title_it' => '',
            'title_fr' => '',
            'title_de' => '',
            'title_ru' => '',
            'parent' => '503',
            'description_en' => 'Please choose a tariff rate',
            'description_es' => 'Por favor, seleccione una tarifa',
            'description_it' => 'Sceglire la tariffa',
            'description_fr' => "Coisissez le tarif, s'il vous plait",
            'description_de' => 'Wählen Sie bitte den Tarif aus',
            'description_ru' => 'Выберите, пожалуйста, тариф',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 1,
            'sort' => 803,
            'is_prev' => 0,
            'next' => 506
        ]);

        $this->insert('oos_comands', [
            'id' => '4000',
            'title_en' => 'Send coordinates',
            'title_es' => 'Enviar coordenadas',
            'title_it' => 'Invia posizione',
            'title_fr' => 'Envoyer des coordonnées',
            'title_de' => 'Koordinaten senden',
            'title_ru' => 'Отправить координаты',
            'parent' => '503',
            'description_en' => 'Please choose a tariff rate',
            'description_es' => 'Por favor, seleccione una tarifa',
            'description_it' => 'Sceglire la tariffa',
            'description_fr' => "Coisissez le tarif, s'il vous plait",
            'description_de' => 'Wählen Sie bitte den Tarif aus',
            'description_ru' => 'Выберите, пожалуйста, тариф',
            'request_contact' => 0,
            'request_location' => 1,
            'is_read' => 0,
            'sort' => 802,
            'is_prev' => 0,
            'next' => 506
        ]);

        $this->insert('oos_comands', [
            'id' => '505',
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => '503',
            'description_en' => 'Please specify pick-up address',
            'description_es' => 'Por favor, indique la dirección adonde el coche debe llegar',
            'description_it' => "Indica l'indirizzo del prelievo",
            'description_fr' => "Indiquez, s'il vous plait, l'adresse de pirse de voiture",
            'description_de' => 'Geben Sie bitte die Abholadresse an',
            'description_ru' => 'Укажите, пожалуйста, адрес подачи автомобиля',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 800,
            'is_prev' => 0,
            'next' => 2
        ]);


        $this->insert('oos_comands', [
            'id' => '507',
            'title_en' => 'Economy',
            'title_es' => 'Ecónomo',
            'title_it' => 'Classe economica',
            'title_fr' => 'Économe',
            'title_de' => 'Ekonom-Klasse',
            'title_ru' => 'Эконом',
            'parent' => '506',
            'description_en' => 'Please specify pick-up date and time (Example: 21.05.2017 11:00)',
            'description_es' => 'Укажите, пожалуйста, дату и время подачи автомобиля (Пример: 21.05.2017 11:00)',
            'description_it' => "Si prega di indicare la data e l'ora del prelievo (ad esempio 21.05.2017 11:00)",
            'description_fr' => "Indiquez, s'il vous plait, la date et l'heure de prise de voiture (Exemple: 21.05.2017 11:00)",
            'description_de' => 'Geben Sie bitte das Abholdatum und -uhrzeit an (Beispiel: 21.05.2017 11:00)',
            'description_ru' => 'Укажите, пожалуйста, дату и время подачи автомобиля (Пример: 21.05.2017 11:00)',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 806,
            'is_prev' => 0,
            'next' => 511
        ]);


        $this->insert('oos_comands', [
            'id' => '508',
            'title_en' => 'Comfort',
            'title_es' => 'Confort',
            'title_it' => 'Classe di comfort',
            'title_fr' => 'Confort',
            'title_de' => 'Komfort-Klasse',
            'title_ru' => 'Комфорт',
            'parent' => '506',
            'description_en' => 'Please specify pick-up date and time (Example: 21.05.2017 11:00)',
            'description_es' => 'Por favor, indique la fecha y hora cuando el coche debe llegar (Ejemplo: 21.05.2017 11:00)',
            'description_it' => "Si prega di indicare la data e l'ora del prelievo (ad esempio 21.05.2017 11:00)",
            'description_fr' => "Indiquez, s'il vous plait, la date et l'heure de prise de voiture (Exemple: 21.05.2017 11:00)",
            'description_de' => 'Geben Sie bitte das Abholdatum und -uhrzeit an (Beispiel: 21.05.2017 11:00)',
            'description_ru' => 'Укажите, пожалуйста, дату и время подачи автомобиля (Пример: 21.05.2017 11:00)',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 805,
            'is_prev' => 0,
            'next' => 511
        ]);

        $this->insert('oos_comands', [
            'id' => '509',
            'title_en' => 'Executive',
            'title_es' => 'Ejecutiva',
            'title_it' => 'Classe esecutiva',
            'title_fr' => 'Business',
            'title_de' => 'Business-Klasse',
            'title_ru' => 'Бизнес',
            'parent' => '506',
            'description_en' => 'Please specify pick-up date and time (Example: 21.05.2017 11:00)',
            'description_es' => 'Por favor, indique la fecha y hora cuando el coche debe llegar (Ejemplo: 21.05.2017 11:00)',
            'description_it' => "Si prega di indicare la data e l'ora del prelievo (ad esempio 21.05.2017 11:00)",
            'description_fr' => "Indiquez, s'il vous plait, la date et l'heure de prise de voiture (Exemple: 21.05.2017 11:00)",
            'description_de' => 'Geben Sie bitte das Abholdatum und -uhrzeit an (Beispiel: 21.05.2017 11:00)',
            'description_ru' => 'Укажите, пожалуйста, дату и время подачи автомобиля (Пример: 21.05.2017 11:00)',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 804,
            'is_prev' => 0,
            'next' => 511
        ]);


        $this->insert('oos_comands', [
            'id' => '4001',
            'title_en' => 'Cтандарт',
            'title_es' => 'Cтандарт',
            'title_it' => 'Cтандарт',
            'title_fr' => 'Cтандарт',
            'title_de' => 'Cтандарт',
            'title_ru' => 'Cтандарт',
            'parent' => '506',
            'description_en' => 'Please specify pick-up date and time (Example: 21.05.2017 11:00)',
            'description_es' => 'Por favor, indique la fecha y hora cuando el coche debe llegar (Ejemplo: 21.05.2017 11:00)',
            'description_it' => "Si prega di indicare la data e l'ora del prelievo (ad esempio 21.05.2017 11:00)",
            'description_fr' => "Indiquez, s'il vous plait, la date et l'heure de prise de voiture (Exemple: 21.05.2017 11:00)",
            'description_de' => 'Geben Sie bitte das Abholdatum und -uhrzeit an (Beispiel: 21.05.2017 11:00)',
            'description_ru' => 'Укажите, пожалуйста, дату и время подачи автомобиля (Пример: 21.05.2017 11:00)',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 804,
            'is_prev' => 0,
            'next' => 511
        ]);


        $this->insert('oos_comands', [
            'id' => '510',
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => '506',
            'description_en' => 'Please specify destination address',
            'description_es' => 'Por favor, especifique la dirección de destino',
            'description_it' => "Inserisci l'indirizzo di destinazione",
            'description_fr' => "Indiquez, s'il vous plait, l'adresse de destination",
            'description_de' => 'Geben Sie bitte die Zieladresse an',
            'description_ru' => 'Укажите, пожалуйста, адрес места назначения',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 803,
            'is_prev' => 0,
            'next' => 503
        ]);


        //Время
        $this->insert('oos_comands', [
            'id' => '512',
            'title_en' => '',
            'title_es' => '',
            'title_it' => '',
            'title_fr' => '',
            'title_de' => '',
            'title_ru' => '',
            'parent' => '511',
            'description_en' => '{order}',
            'description_es' => '{order}',
            'description_it' => '{order}',
            'description_fr' => '{order}',
            'description_de' => '{order}',
            'description_ru' => '{order}',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 1,
            'sort' => 800,
            'is_prev' => 0,
            'next' => 515
        ]);

        $this->insert('oos_comands', [
            'id' => '513',
            'title_en' => 'Current time',
            'title_es' => 'Tiempo actual',
            'title_it' => 'Ora attuale',
            'title_fr' => "Heure qu'il est",
            'title_de' => 'Aktuelle Uhrzeit',
            'title_ru' => 'Текущее время',
            'parent' => '511',
            'description_en' => '{order}',
            'description_es' => '{order}',
            'description_it' => '{order}',
            'description_fr' => '{order}',
            'description_de' => '{order}',
            'description_ru' => '{order}',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 802,
            'is_prev' => 0,
            'next' => 515
        ]);

        $this->insert('oos_comands', [
            'id' => '514',
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => '511',
            'description_en' => 'Please choose a tariff rate',
            'description_es' => 'Por favor, seleccione una tarifa',
            'description_it' => 'Sceglire la tariffa',
            'description_fr' => "Coisissez le tarif, s'il vous plait",
            'description_de' => 'Wählen Sie bitte den Tarif aus',
            'description_ru' => 'Выберите, пожалуйста, тариф',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 801,
            'is_prev' => 0,
            'next' => 506
        ]);

        //Оформление
        $this->insert('oos_comands', [
            'id' => '516',
            'title_en' => 'Make an order',
            'title_es' => 'Hacer el pedido',
            'title_it' => 'Effettuare un ordine',
            'title_fr' => 'Enregistrer une commande',
            'title_de' => 'Bestellung ausstellen',
            'title_ru' => 'Оформить заказ',
            'parent' => '515',
            'description_en' => 'Thank you, your order has been registered. Please wait for your driver.',
            'description_es' => 'Gracias, Vuestro pedido ya está listo. Espere al chofer',
            'description_it' => 'Grazie. Il tuo ordine è confermato. Aspetta l\'arrivo dell\'autista',
            'description_fr' => 'Mérci, Votre commande est enregistrée',
            'description_de' => 'Vielen Dank, Ihre Bestellung wurde registriert. Warten Sie bitte auf den Fahrer',
            'description_ru' => 'Спасибо, Ваш заказ оформлен. Ожидайте водителя',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 806,
            'is_prev' => 0
        ]);

        $this->insert('oos_comands', [
            'id' => '518',
            'title_en' => 'Main menu',
            'title_es' => 'Menú principal',
            'title_it' => 'Menu principale',
            'title_fr' => 'Menu principal',
            'title_de' => 'Hauptmenü',
            'title_ru' => 'Главное меню',
            'parent' => '516',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 805,
            'is_main' => 1
        ]);

        $this->insert('oos_comands', [
            'id' => '10000',
            'title_en' => 'Cancel order',
            'title_es' => 'Cancelar el pedido',
            'title_it' => 'Annulla ordine',
            'title_fr' => 'Décommander',
            'title_de' => 'Bestellung abbrechen',
            'title_ru' => 'Отменить заказ',
            'parent' => '516',
            'description_en' => 'Your order has been cancelled',
            'description_es' => 'Su pedido ha sido cancelado',
            'description_it' => 'Il tuo ordine è stato annullato',
            'description_fr' => 'Votre commande est annulée',
            'description_de' => 'Ihr Auftrag ist abgebrochen',
            'description_ru' => 'Ваш заказ отменен',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 802,
            'is_prev' => 0
        ]);

        $this->insert('oos_comands', [
            'id' => '10001',
            'title_en' => 'Place an order',
            'title_es' => 'Hacer el pedido',
            'title_it' => 'Ordina',
            'title_fr' => 'Passer une commande',
            'title_de' => 'Bestellung machen',
            'title_ru' => 'Сделать заказ',
            'parent' => '10000',
            'description_en' => 'Please specify pick-up address',
            'description_es' => 'Por favor, indique la dirección adonde el coche debe llegar',
            'description_it' => "Indica l'indirizzo del prelievo",
            'description_fr' => "Indiquez, s'il vous plait, l'adresse de pirse de voiture",
            'description_de' => 'Geben Sie bitte die Abholadresse an	',
            'description_ru' => 'Укажите, пожалуйста, адрес подачи автомобиля',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 807,
            'is_main' => 0,
            'next' => 2
        ]);

        $this->insert('oos_comands', [
            'id' => '10002',
            'title_en' => 'Main menu',
            'title_es' => 'Menú principal',
            'title_it' => 'Menu principale',
            'title_fr' => 'Menu principal',
            'title_de' => 'Hauptmenü',
            'title_ru' => 'Главное меню',
            'parent' => '10000',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 805,
            'is_main' => 1
        ]);

        //Отмена заказа

        $this->insert('oos_comands', [
            'id' => '519',
            'title_en' => 'Cancel order',
            'title_es' => 'Cancelar el pedido',
            'title_it' => 'Annulla ordine',
            'title_fr' => 'Décommander',
            'title_de' => 'Bestellung abbrechen',
            'title_ru' => 'Отменить заказ',
            'parent' => '515',
            'description_en' => 'Your order has been cancelled',
            'description_es' => 'Su pedido ha sido cancelado',
            'description_it' => 'Il tuo ordine è stato annullato',
            'description_fr' => 'Votre commande est annulée',
            'description_de' => 'Ihr Auftrag ist abgebrochen',
            'description_ru' => 'Ваш заказ отменен',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 802,
            'is_prev' => 0
        ]);

        $this->insert('oos_comands', [
            'id' => '520',
            'title_en' => 'Place an order',
            'title_es' => 'Hacer el pedido',
            'title_it' => 'Ordina',
            'title_fr' => 'Passer une commande',
            'title_de' => 'Bestellung machen',
            'title_ru' => 'Сделать заказ',
            'parent' => '519',
            'description_en' => 'Please specify pick-up address',
            'description_es' => 'Por favor, indique la dirección adonde el coche debe llegar',
            'description_it' => "Indica l'indirizzo del prelievo",
            'description_fr' => "Indiquez, s'il vous plait, l'adresse de pirse de voiture",
            'description_de' => 'Geben Sie bitte die Abholadresse an	',
            'description_ru' => 'Укажите, пожалуйста, адрес подачи автомобиля',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 807,
            'is_main' => 0,
            'next' => 2
        ]);

        $this->insert('oos_comands', [
            'id' => '521',
            'title_en' => 'Main menu',
            'title_es' => 'Menú principal',
            'title_it' => 'Menu principale',
            'title_fr' => 'Menu principal',
            'title_de' => 'Hauptmenü',
            'title_ru' => 'Главное меню',
            'parent' => '519',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 805,
            'is_main' => 1
        ]);

        ///Изменить время подачи

        $this->insert('oos_comands', [
            'id' => '522',
            'title_en' => 'Change pick-up time',
            'title_es' => 'Cambiar la hora de entrega',
            'title_it' => "Cambia l’orario di prelievo",
            'title_fr' => "Changer l'heure de prise",
            'title_de' => 'Abholzeit ändern',
            'title_ru' => 'Изменить время подачи',
            'parent' => '515',
            'description_en' => 'Please specify pick-up date and time (Example: 21.05.2017 11:00)',
            'description_es' => 'Por favor, indique la fecha y hora cuando el coche debe llegar (Ejemplo: 21.05.2017 11:00)',
            'description_it' => "Si prega di indicare la data e l'ora del prelievo (ad esempio 21.05.2017 11:00)",
            'description_fr' => "Indiquez, s'il vous plait, la date et l'heure de prise de voiture (Exemple: 21.05.2017 11:00)",
            'description_de' => 'Geben Sie bitte das Abholdatum und -uhrzeit an (Beispiel: 21.05.2017 11:00)',
            'description_ru' => 'Укажите, пожалуйста, дату и время подачи автомобиля (Пример: 21.05.2017 11:00)',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 802,
            'is_prev' => 0,
            'next' => 511
        ]);

        //Добавить комментарий

        $this->insert('oos_comands', [
            'id' => '523',
            'title_en' => 'Add a comment',
            'title_es' => 'Añádir comentarios',
            'title_it' => 'Aggiungi un commento',
            'title_fr' => 'Ajouter un commentaire',
            'title_de' => 'Kommentar hinzufügen',
            'title_ru' => 'Добавить комментарий',
            'parent' => '515',
            'description_en' => 'Add a comment to the order',
            'description_es' => 'Introduzca un comentario al pedido',
            'description_it' => "Aggiungi un commento all'ordine",
            'description_fr' => 'Ajoutez un commentaire à commandeу',
            'description_de' => 'Fügen Sie einen Kommentar zur Bestellung',
            'description_ru' => 'Введите комментарий к заказу',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 802,
            'is_prev' => 0,
            'next' => 524
        ]);


        $this->insert('oos_comands', [
            'id' => '525',
            'title_en' => '',
            'title_es' => '',
            'title_it' => '',
            'title_fr' => '',
            'title_de' => '',
            'title_ru' => '',
            'parent' => '524',
            'description_en' => '{order}',
            'description_es' => '{order}',
            'description_it' => '{order}',
            'description_fr' => '{order}',
            'description_de' => '{order}',
            'description_ru' => '{order}',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 1,
            'sort' => 805,
            'is_prev' => 0,
            'next' => 515
        ]);

        $this->insert('oos_comands', [
            'id' => '526',
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => '524',
            'description_en' => '{order}',
            'description_es' => '{order}',
            'description_it' => '{order}',
            'description_fr' => '{order}',
            'description_de' => '{order}',
            'description_ru' => '{order}',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 803,
            'is_prev' => 0,
            'next' => 515
        ]);

        //Назначен

        $this->insert('oos_comands', [
            'id' => 601,
            'title_en' => '',
            'title_es' => '',
            'title_it' => '',
            'title_fr' => '',
            'title_de' => '',
            'title_ru' => '',
            'parent' => 601,
            'description_en' => '{check}',
            'description_es' => '{check}',
            'description_it' => '{check}',
            'description_fr' => '{check}',
            'description_de' => '{check}',
            'description_ru' => '{check}',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 1,
            'is_prev' => 0,
        ]);

        $this->insert('oos_comands', [
            'id' => 602,
            'title_en' => 'Cancel order',
            'title_es' => 'Cancelar el pedido',
            'title_it' => 'Annulla ordine',
            'title_fr' => 'Décommander',
            'title_de' => 'Bestellung abbrechen',
            'title_ru' => 'Отменить заказ',
            'parent' => 601,
            'description_en' => 'Your order has been cancelled',
            'description_es' => 'Su pedido ha sido cancelado',
            'description_it' => 'Il tuo ordine è stato annullato',
            'description_fr' => 'Votre commande est annulée',
            'description_de' => 'Ihr Auftrag ist abgebrochen',
            'description_ru' => 'Ваш заказ отменен',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' =>  808,
            'is_prev' => 0,
            'next' => 519
        ]);

        $this->insert('oos_comands', [
            'id' => '603',
            'title_en' => 'Main menu',
            'title_es' => 'Menú principal',
            'title_it' => 'Menu principale',
            'title_fr' => 'Menu principal',
            'title_de' => 'Hauptmenü',
            'title_ru' => 'Главное меню',
            'parent' => 601,
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 805,
            'is_main' => 1
        ]);

        //Ожидает
        $this->insert('oos_comands', [
            'id' => 607,
            'title_en' => '',
            'title_es' => '',
            'title_it' => '',
            'title_fr' => '',
            'title_de' => '',
            'title_ru' => '',
            'parent' => 606,
            'description_en' => '{wait}',
            'description_es' => '{wait}',
            'description_it' => '{wait}',
            'description_fr' => '{wait}',
            'description_de' => '{wait}',
            'description_ru' => '{wait}',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 1,
            'is_prev' => 0,
        ]);
        $this->insert('oos_comands', [
            'id' => 608,
            'title_en' => 'Cancel order',
            'title_es' => 'Cancelar el pedido',
            'title_it' => 'Annulla ordine',
            'title_fr' => 'Décommander',
            'title_de' => 'Bestellung abbrechen',
            'title_ru' => 'Отменить заказ',
            'parent' => 607,
            'description_en' => 'Your order has been cancelled',
            'description_es' => 'Su pedido ha sido cancelado',
            'description_it' => 'Il tuo ordine è stato annullato',
            'description_fr' => 'Votre commande est annulée',
            'description_de' => 'Ihr Auftrag ist abgebrochen',
            'description_ru' => 'Ваш заказ отменен',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 1,
            'is_prev' => 0,
            'next' => 519
        ]);

        //Уже оформлен
        $this->insert('oos_comands', [
            'id' => '630',
            'title_en' => '',
            'title_es' => '',
            'title_it' => '',
            'title_fr' => '',
            'title_de' => '',
            'title_ru' => '',
            'parent' => '630',
            'description_en' => '{order}',
            'description_es' => '{order}',
            'description_it' => '{order}',
            'description_fr' => '{order}',
            'description_de' => '{order}',
            'description_ru' => '{order}',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 806,
            'is_prev' => 0
        ]);

        $this->insert('oos_comands', [
            'id' => '632',
            'title_en' => 'Main menu',
            'title_es' => 'Menú principal',
            'title_it' => 'Menu principale',
            'title_fr' => 'Menu principal',
            'title_de' => 'Hauptmenü',
            'title_ru' => 'Главное меню',
            'parent' => '630',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 805,
            'is_main' => 1
        ]);

        //Отмена заказа

        $this->insert('oos_comands', [
            'id' => '633',
            'title_en' => 'Cancel order',
            'title_es' => 'Cancelar el pedido',
            'title_it' => 'Annulla ordine',
            'title_fr' => 'Décommander',
            'title_de' => 'Bestellung abbrechen',
            'title_ru' => 'Отменить заказ',
            'parent' => '630',
            'description_en' => 'Your order has been cancelled',
            'description_es' => 'Su pedido ha sido cancelado',
            'description_it' => 'Il tuo ordine è stato annullato',
            'description_fr' => 'Votre commande est annulée',
            'description_de' => 'Ihr Auftrag ist abgebrochen',
            'description_ru' => 'Ваш заказ отменен',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 802,
            'is_prev' => 0,
            'next' => 519
        ]);

        $this->insert('oos_comands', [
            'id' => '1000',
            'title_en' => '<b>The code you have entered is invalid. Please try again.</b>',
            'title_es' => '<b>Ha introducido el código incorrecto. Inténtelo de nuevo.</b>',
            'title_it' => '<b>Il codice immesso non è valido. Per favore, riprova.</b>',
            'title_fr' => '<b>Vous avez entrer un code invalide. Essayez encore une fois.</b>',
            'title_de' => '<b>Der eingegebene Code ist ungültig. Bitte versuche es erneut.</b>',
            'title_ru' => '<b>Вы ввели неверный код. Попробуйте еще раз</b>',
            'parent' => '',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 995,
        ]);


        //Завершен

        $this->insert('oos_comands', [
            'id' => 650,
            'title_en' => '',
            'title_es' => '',
            'title_it' => '',
            'title_fr' => '',
            'title_de' => '',
            'title_ru' => '',
            'parent' => 650,
            'description_en' => '{finish}',
            'description_es' => '{finish}',
            'description_it' => '{finish}',
            'description_fr' => '{finish}',
            'description_de' => '{finish}',
            'description_ru' => '{finish}',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 1,
            'is_prev' => 0,
        ]);

        $this->insert('oos_comands', [
            'id' => '651',
            'title_en' => 'Place an order',
            'title_es' => 'Hacer el pedido',
            'title_it' => 'Ordina',
            'title_fr' => 'Passer une commande',
            'title_de' => 'Bestellung machen',
            'title_ru' => 'Сделать заказ',
            'parent' => '650',
            'description_en' => 'Please specify pick-up address',
            'description_es' => 'Por favor, indique la dirección adonde el coche debe llegar',
            'description_it' => "Indica l'indirizzo del prelievo",
            'description_fr' => "Indiquez, s'il vous plait, l'adresse de pirse de voiture",
            'description_de' => 'Geben Sie bitte die Abholadresse an	',
            'description_ru' => 'Укажите, пожалуйста, адрес подачи автомобиля',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 807,
            'is_main' => 0,
            'next' => 2
        ]);

        $this->insert('oos_comands', [
            'id' => '652',
            'title_en' => 'Main menu',
            'title_es' => 'Menú principal',
            'title_it' => 'Menu principale',
            'title_fr' => 'Menu principal',
            'title_de' => 'Hauptmenü',
            'title_ru' => 'Главное меню',
            'parent' => '650',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 805,
            'is_main' => 1
        ]);


        //Отменен

        $this->insert('oos_comands', [
            'id' => 660,
            'title_en' => '',
            'title_es' => '',
            'title_it' => '',
            'title_fr' => '',
            'title_de' => '',
            'title_ru' => '',
            'parent' => 660,
            'description_en' => 'Your order has been cancelled',
            'description_es' => 'Su pedido ha sido cancelado',
            'description_it' => 'Il tuo ordine è stato annullato',
            'description_fr' => 'Votre commande est annulée',
            'description_de' => 'Ihr Auftrag ist abgebrochen',
            'description_ru' => 'Ваш заказ отменен',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 1,
            'is_prev' => 0,
        ]);

        $this->insert('oos_comands', [
            'id' => '661',
            'title_en' => 'Place an order',
            'title_es' => 'Hacer el pedido',
            'title_it' => 'Ordina',
            'title_fr' => 'Passer une commande',
            'title_de' => 'Bestellung machen',
            'title_ru' => 'Сделать заказ',
            'parent' => '660',
            'description_en' => 'Please specify pick-up address',
            'description_es' => 'Por favor, indique la dirección adonde el coche debe llegar',
            'description_it' => "Indica l'indirizzo del prelievo",
            'description_fr' => "Indiquez, s'il vous plait, l'adresse de pirse de voiture",
            'description_de' => 'Geben Sie bitte die Abholadresse an	',
            'description_ru' => 'Укажите, пожалуйста, адрес подачи автомобиля',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 807,
            'is_main' => 0,
            'next' => 2
        ]);

        $this->insert('oos_comands', [
            'id' => '662',
            'title_en' => 'Main menu',
            'title_es' => 'Menú principal',
            'title_it' => 'Menu principale',
            'title_fr' => 'Menu principal',
            'title_de' => 'Hauptmenü',
            'title_ru' => 'Главное меню',
            'parent' => '660',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 805,
            'is_main' => 1
        ]);
    }

    public function safeDown()
    {
        echo "m161129_093103_oos_comands_order_table cannot be reverted.\n";
        return false;
    }
}