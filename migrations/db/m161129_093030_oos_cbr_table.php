<?php
require_once('vendor/stdlib-migrations/Migrations/src/Migrations/Tools/MigrationTpl.php');
use Migrations\Tools\MigrationTpl;

class m161129_093030_oos_cbr_table extends MigrationTpl
{

// Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('oos_cbr',
            [
                'id' => 'bigserial NOT NULL',
                'num_code' => 'character(36) NOT NULL',
                'char_code' => 'character(36) NOT NULL',
                'nominal' => 'integer NOT NULL',
                'title_en' => 'character(256) NOT NULL',
                'title_es' => 'character(256) NOT NULL',
                'title_it' => 'character(256) NOT NULL',
                'title_fr' => 'character(256) NOT NULL',
                'title_de' => 'character(256) NOT NULL',
                'title_ru' => 'character(256) NOT NULL',
                'value' => 'decimal NOT NULL',
                'date_created' => 'timestamp NOT NULL DEFAULT NOW()',
                'date_download' => 'timestamp NOT NULL DEFAULT NOW()'
            ]
        );
        $this->addPrimaryKey('oos_cbr_primary_key', 'oos_cbr', 'id');
        $this->createIndex('oos_cbr_date_created_idx', 'oos_cbr', 'date_created');
    }

    public function safeDown()
    {
        echo "m161129_093030_oos_cbr_table cannot be reverted.\n";

        return false;
    }
}
