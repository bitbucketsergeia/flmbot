<?php
require_once('vendor/stdlib-migrations/Migrations/src/Migrations/Tools/MigrationTpl.php');
use Migrations\Tools\MigrationTpl;

class m161129_093009_oos_comands_table extends MigrationTpl
{

// Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('oos_comands',
            [
                'id' => 'bigserial NOT NULL',
                'title_en' => 'character varying(256) NULL',
                'title_es' => 'character varying(256) NULL',
                'title_it' => 'character varying(256) NULL',
                'title_fr' => 'character varying(256) NULL',
                'title_de' => 'character varying(256) NULL',
                'title_ru' => 'character varying(256) NULL',
                'parent' => 'bigint NULL',
                'description_en' => 'text NULL',
                'description_es' => 'text NULL',
                'description_it' => 'text NULL',
                'description_fr' => 'text NULL',
                'description_de' => 'text NULL',
                'description_ru' => 'text NULL',
                'request_contact' => "integer NOT NULL DEFAULT '0'",
                'request_location' => "integer NOT NULL DEFAULT '0'",
                'is_read' => "integer NOT NULL DEFAULT '0'",
                'sort' => "integer NOT NULL DEFAULT '1000'",
                'is_prev' => "integer NOT NULL DEFAULT '0'",
                'is_main' => "integer NOT NULL DEFAULT '0'",
                'next' => 'bigint NULL',
            ]
        );

        $this->addPrimaryKey('oos_comands_primary_key', 'oos_comands', 'id');
        $this->createIndex('oos_comands_parent_idx', 'oos_comands', 'parent');

        $this->createIndex('oos_comands_title_en_idx', 'oos_comands', 'title_en');
        $this->createIndex('oos_comands_title_es_idx', 'oos_comands', 'title_es');
        $this->createIndex('oos_comands_title_it_idx', 'oos_comands', 'title_it');
        $this->createIndex('oos_comands_title_fr_idx', 'oos_comands', 'title_fr');
        $this->createIndex('oos_comands_title_de_idx', 'oos_comands', 'title_de');
        $this->createIndex('oos_comands_title_ru_idx', 'oos_comands', 'title_ru');

        $this->insert('oos_comands', [
            'id' => '1',
            'title_en' => '',
            'title_es' => '',
            'title_it' => '',
            'title_fr' => '',
            'title_de' => '',
            'title_ru' => '',
            'parent' => '',
            'description_en' => '<b>Select desired menu item</b>',
            'description_es' => '<b>Seleccione la opción de menú que más le interese</b>',
            'description_it' => '<b>Seleziona la voce del menu desiderata</b>',
            'description_fr' => '<b>Choisissez un élément de menu, qui Vous intéresse</b>',
            'description_de' => '<b>Wählen Sie den gewünschten Menüpunkt aus</b>',
            'description_ru' => '<b>Выберите интересующий Вас пункт меню</b>',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 995,
        ]);


        $this->insert('oos_comands', [
            'id' => '2',
            'title_en' => 'Taxi services',
            'title_es' => 'Pedido de taxi',
            'title_it' => 'Servizio taxi',
            'title_fr' => 'Service de taxi',
            'title_de' => 'Taxiservice',
            'title_ru' => 'Заказ такси',
            'parent' => '1',
            'description_en' => 'Please specify pick-up address',
            'description_es' => 'Por favor, indique la dirección adonde el coche debe llegar',
            'description_it' => "Indica l'indirizzo del prelievo",
            'description_fr' => "Indiquez, s'il vous plait, l'adresse de pirse de voiture",
            'description_de' => 'Geben Sie bitte die Abholadresse an',
            'description_ru' => 'Укажите, пожалуйста, адрес подачи автомобиля',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 1000,
        ]);

        $this->insert('oos_comands', [
            'id' => '3',
            'title_en' => 'Путеводитель Myconcierge.ru',
            'title_es' => 'Путеводитель Myconcierge.ru',
            'title_it' => 'Путеводитель Myconcierge.ru',
            'title_fr' => 'Путеводитель Myconcierge.ru',
            'title_de' => 'Путеводитель Myconcierge.ru',
            'title_ru' => 'Путеводитель Myconcierge.ru',
            'parent' => '1',
            'description_en' => 'Данный раздел находится в разработке',
            'description_es' => 'Данный раздел находится в разработке',
            'description_it' => 'Данный раздел находится в разработке',
            'description_fr' => 'Данный раздел находится в разработке',
            'description_de' => 'Данный раздел находится в разработке',
            'description_ru' => 'Данный раздел находится в разработке',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 999,
        ]);

        $this->insert('oos_comands', [
            'id' => '998',
            'title_en' => 'Афиша Myconcierge.ru',
            'title_es' => 'Афиша Myconcierge.ru',
            'title_it' => 'Афиша Myconcierge.ru',
            'title_fr' => 'Афиша Myconcierge.ru',
            'title_de' => 'Афиша Myconcierge.ru',
            'title_ru' => 'Афиша Myconcierge.ru',
            'parent' => '1',
            'description_en' => 'Данный раздел находится в разработке',
            'description_es' => 'Данный раздел находится в разработке',
            'description_it' => 'Данный раздел находится в разработке',
            'description_fr' => 'Данный раздел находится в разработке',
            'description_de' => 'Данный раздел находится в разработке',
            'description_ru' => 'Данный раздел находится в разработке',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 998,
        ]);

        $this->insert('oos_comands', [
            'id' => '4',
            'title_en' => 'Translator',
            'title_es' => 'Traductor',
            'title_it' => 'Traduttore',
            'title_fr' => 'Traducteur',
            'title_de' => 'Übersetzer',
            'title_ru' => 'Переводчик',
            'parent' => '1',
            'description_en' => 'Enter text you would like to translate',
            'description_es' => 'Introduzca el texto que desea traducir',
            'description_it' => 'Inserisci il testo da tradurre',
            'description_fr' => 'Entrez le texte, le quel Vous voulez traduire',
            'description_de' => 'Geben Sie den Text zum Übersetzen ein',
            'description_ru' => 'Введите текст, который Вы хотите перевести',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 997,
        ]);

        $this->insert('oos_comands', [
            'id' => '5',
            'title_en' => 'Weather',
            'title_es' => 'Tiempo',
            'title_it' => 'Tempo metereologico',
            'title_fr' => 'Temps',
            'title_de' => 'Wetter',
            'title_ru' => 'Погода',
            'parent' => '1',
            'description_en' => 'Enter city name or send geolocation details',
            'description_es' => 'Introduzca el nombre de la ciudad o envíe la geolocalización',
            'description_it' => 'Inserisci il nome della città o invia i dati della geolocalizzazione',
            'description_fr' => 'Entrez la ville ou envoyez la géolocalisation',
            'description_de' => 'Geben Sie den Stadtnamen ein oder senden den Standort	',
            'description_ru' => 'Введите название города или отправьте геопозицию',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 996,
        ]);

        $this->insert('oos_comands', [
            'id' => '6',
            'title_en' => 'Currency rates',
            'title_es' => 'Tipos de cambio de moneda',
            'title_it' => 'Tassi di cambio',
            'title_fr' => 'Cours des devises',
            'title_de' => 'Wechselkurse',
            'title_ru' => 'Курс валют',
            'parent' => '1',
            'description_en' => '{cbr}',
            'description_es' => '{cbr}',
            'description_it' => '{cbr}',
            'description_fr' => '{cbr}',
            'description_de' => '{cbr}',
            'description_ru' => '{cbr}',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 995,
        ]);
        
        $this->insert('oos_comands', [
            'id' => '7',
            'title_en' => 'Settings',
            'title_es' => 'Ajustes',
            'title_it' => 'Impostazioni',
            'title_fr' => 'Paramètres',
            'title_de' => 'Eistellungen',
            'title_ru' => 'Настройки',
            'parent' => '1',
            'description_en' => '<b>Use the menu to change current settings</b>',
            'description_es' => '<b>Para cambiar la configuración actual, utilice el menú</b>',
            'description_it' => '<b>Utilizza il menu per modificare le impostazioni correnti</b>',
            'description_fr' => '<b>Pour changer les paramètres courants, utilisez le menu</b>',
            'description_de' => '<b>Ändern Sie die aktuellen Einstellungen über das Menü</b>',
            'description_ru' => '<b>Чтобы изменить текущие настройки, воспользуйтесь меню</b>',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 993,
        ]);


        $this->insert('oos_comands', [
            'id' => '8',
            'title_en' => '<b>You have entered a wrong command. Please try again</b>',
            'title_es' => '<b>El comando que ha introducido no es válido. Inténtelo de nuevo</b>',
            'title_it' => "<b>L'operazione richiesta non è valida. Si prega di riprovare</b>",
            'title_fr' => "<b>Vous avez entrer une commande invalide. Essayez-vous encore une fois, s'il vous plait</b>",
            'title_de' => '<b>Sie haben einen falschen Befehl eingegeben. Bitte, versuchen Sie es erneut</b>',
            'title_ru' => '<b>Вы ввели неверную команду. Попробуйте еще раз</b>',
            'parent' => '',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 993,
        ]);
    }

    public function safeDown()
    {
        echo "m161129_093009_oos_comands_table cannot be reverted.\n";

        return false;
    }
}
