<?php
require_once('vendor/stdlib-migrations/Migrations/src/Migrations/Tools/MigrationTpl.php');
use Migrations\Tools\MigrationTpl;

class m161129_093103_oos_comands_table extends MigrationTpl
{
    public function safeUp()
    {
        //Переводчик
        $this->insert('oos_comands', [
            'id' => '51',
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => '4',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 1000,
            'is_prev' => 1
        ]);

        $this->insert('oos_comands', [
            'id' => '52',
            'title_en' => '',
            'title_es' => '',
            'title_it' => '',
            'title_fr' => '',
            'title_de' => '',
            'title_ru' => '',
            'parent' => '4',
            'description_en' => 'Choose source text language',
            'description_es' => 'Seleccionar el idioma del texto de origen',
            'description_it' => 'Scegli la lingua del testo di partenza',
            'description_fr' => 'Coisissez la langue du texte source',
            'description_de' => 'Wählen Sie die Sprache des Ausgangstextes aus',
            'description_ru' => 'Выберите язык исходного текста',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 1,
            'sort' => 1000,
            'is_prev' => 0,
            'next' => 53
        ]);

        $langs = [
            'English', 'Español', 'Italiano', 'Français', 'Deutsch', 'Русский'
        ];

        $i1 = 53;
        $countPP = count($langs);

        for ($i = 0; $i < $countPP; $i++) {
            $this->insert('oos_comands', [
                'id' => $i1 + $i + 1,
                'title_en' => $langs[$i],
                'title_es' => $langs[$i],
                'title_it' => $langs[$i],
                'title_fr' => $langs[$i],
                'title_de' => $langs[$i],
                'title_ru' => $langs[$i],
                'parent' => $i1,
                'description_en' => 'Choose target text language',
                'description_es' => 'Introduzca el idioma de traducción',
                'description_it' => 'Scegli la lingua del testo di arrivo',
                'description_fr' => 'Choisissez la langue de traduction',
                'description_de' => 'Geben Sie die Zielsprache ein',
                'description_ru' => 'Введите язык перевода',
                'request_contact' => 0,
                'request_location' => 0,
                'is_read' => 1,
                'sort' => 1000 - $i,
                'is_prev' => 0,
                'next' => 60
            ]);
        }

        $i1 = 60;

        for ($i = 0; $i < $countPP; $i++) {
            $this->insert('oos_comands', [
                'id' => $i1 + $i + 1,
                'title_en' => $langs[$i],
                'title_es' => $langs[$i],
                'title_it' => $langs[$i],
                'title_fr' => $langs[$i],
                'title_de' => $langs[$i],
                'title_ru' => $langs[$i],
                'parent' => $i1,
                'description_en' => '',
                'description_es' => '{translate}',
                'description_it' => '{translate}',
                'description_fr' => '{translate}',
                'description_de' => '{translate}',
                'description_ru' => '{translate}',
                'request_contact' => 0,
                'request_location' => 0,
                'is_read' => 1,
                'sort' => 1000 - $i,
                'is_prev' => 0,
                'next' => 67
            ]);
        }

        $i1 = 67;

        $this->insert('oos_comands', [
            'id' => 68,
            'title_en' => 'Translate another text',
            'title_es' => 'Traducir el otro texto',
            'title_it' => 'Traduci un altro testo',
            'title_fr' => 'Traduire un autre texte	',
            'title_de' => 'Einen anderen Text übersetzen',
            'title_ru' => 'Перевести другой текст',
            'parent' => $i1,
            'description_en' => 'Enter text you would like to translate',
            'description_es' => 'Introduzca el texto que desea traducir',
            'description_it' => 'Inserisci il testo da tradurre',
            'description_fr' => 'Entrez le texte, le quel Vous voulez traduire',
            'description_de' => 'Geben Sie den Text zum Übersetzen ein',
            'description_ru' => 'Введите текст, который Вы хотите перевести',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 1000,
            'next' => 4
        ]);

        $this->insert('oos_comands', [
            'id' => 69,
            'title_en' => 'Main menu',
            'title_es' => 'Menú principal',
            'title_it' => 'Menu principale',
            'title_fr' => 'Menu principal',
            'title_de' => 'Hauptmenü',
            'title_ru' => 'Главное меню',
            'parent' => $i1,
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 999,
            'is_main' => 1
        ]);
    }

    public function safeDown()
    {
        echo "m161129_093103_oos_comands_table cannot be reverted.\n";

        return false;
    }
}
