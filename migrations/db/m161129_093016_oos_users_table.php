<?php
require_once('vendor/stdlib-migrations/Migrations/src/Migrations/Tools/MigrationTpl.php');
use Migrations\Tools\MigrationTpl;

class m161129_093016_oos_users_table extends MigrationTpl
{

// Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('oos_users',
            [
                'id' => 'bigserial NOT NULL',
                'params' => 'json NULL',
                'chat_id' => 'bigint NOT NULL',
                'city_id' => 'bigint NULL',
                'lang' => 'character varying(30) NULL',
                'phone'=> 'character varying(30) NULL',
                'date_created' => 'timestamp NOT NULL DEFAULT NOW()',
                'status' => 'character varying(30) NOT NULL', //ACTIVE, DELETE, CHECKED
            ]
        );

        $this->addPrimaryKey('oos_users_primary_key', 'oos_users', 'id');
        $this->createIndex('oos_users_chat_id_idx', 'oos_users', 'chat_id');
        $this->createIndex('oos_users_phone_idx', 'oos_users', 'phone');
    }

    public function safeDown()
    {
        echo "m161129_093016_oos_users_table cannot be reverted.\n";

        return false;
    }
}