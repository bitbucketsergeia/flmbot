<?php
require_once('vendor/stdlib-migrations/Migrations/src/Migrations/Tools/MigrationTpl.php');
use Migrations\Tools\MigrationTpl;

class m161129_093103_oos_comands_settings_table extends MigrationTpl
{
    public function safeUp()
    {
        //Настройки
        $this->insert('oos_comands', [
            'id' => '41',
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => '7',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 811,
            'is_prev' => 1
        ]);

        $this->insert('oos_comands', [
            'id' => '42',
            'title_en' => 'Change language',
            'title_es' => 'Cambiar el idioma',
            'title_it' => 'Cambia lingua',
            'title_fr' => 'Changer la langue',
            'title_de' => 'Sprache ändern',
            'title_ru' => 'Изменить язык',
            'parent' => '7',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 801,
            'is_prev' => 0,
        ]);

        $this->insert('oos_comands', [
            'id' => '43',
            'title_en' => 'Change city',
            'title_es' => 'Cambia città',
            'title_it' => 'Cambiar la ciudad',
            'title_fr' => 'Changer la ville',
            'title_de' => 'Stadt ändern',
            'title_ru' => 'Изменить город',
            'parent' => '7',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 800,
            'is_prev' => 0
        ]);

        $this->insert('oos_comands', [
            'id' => '44',
            'title_en' => 'Reset to defaults',
            'title_es' => 'Restablecer ajustes',
            'title_it' => 'Ripristina valori predefiniti',
            'title_fr' => 'Réinitialiser des paramètres',
            'title_de' => 'Einstellungen zurücksetzen',
            'title_ru' => 'Сбросить настройки',
            'parent' => '7',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 799,
            'is_main' => 0
        ]);

        $this->insert('oos_comands', [
            'id' => '45',
            'title_en' => 'Request a taxi',
            'title_es' => 'Pedir taxi',
            'title_it' => 'Ordina un taxi',
            'title_fr' => 'Commander un taxi',
            'title_de' => 'Taxi bestellen',
            'title_ru' => 'Заказать такси',
            'parent' => '7',
            'description_en' => 'Choose city to order a taxi',
            'description_es' => 'Para pedir un taxi, seleccione la ciudad',
            'description_it' => 'Scegli la città per ordinare un taxi',
            'description_fr' => 'Pour commander le taxi, choisissez la ville',
            'description_de' => 'Um Taxi zu bestellen, wählen Sie bitte die Stadt aus',
            'description_ru' => 'Чтобы заказать такси, выберите город',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 798,
            'is_main' => 0,
            'next' => 2
        ]);

        $this->insert('oos_comands', [
            'id' => '47',
            'title_en' => 'Developers',
            'title_es' => 'Desarrolladores',
            'title_it' => 'Sviluppatori',
            'title_fr' => 'Développeurs',
            'title_de' => 'Entwickler',
            'title_ru' => 'Разработчики',
            'parent' => '7',
            'description_en' => 'Development team chat bot: @webguruBot',
            'description_es' => 'Bot de charla de los Desarrolladores: @webguruBot',
            'description_it' => 'Chatbot degli sviluppatori: @webguruBot',
            'description_fr' => 'Chat-bot des développeurs: @webguruBot',
            'description_de' => 'Chatbot der Entwickler: @webguruBot',
            'description_ru' => 'Чат-бот разработчиков: @webguruBot',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 798,
            'is_main' => 0
        ]);

        $this->insert('oos_comands', [
            'id' => '49',
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => '47',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 811,
            'is_prev' => 1
        ]);

        $this->insert('oos_comands', [
            'id' => '46',
            'title_en' => 'Main menu',
            'title_es' => 'Menú principal',
            'title_it' => 'Menu principale',
            'title_fr' => 'Menu principal',
            'title_de' => 'Hauptmenü',
            'title_ru' => 'Главное меню',
            'parent' => '7',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 797,
            'is_main' => 1
        ]);

    }

    public function safeDown()
    {
        echo "m161129_093103_oos_comands_table cannot be reverted.\n";

        return false;
    }
}
