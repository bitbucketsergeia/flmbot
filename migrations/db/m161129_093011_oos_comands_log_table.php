<?php
require_once('vendor/stdlib-migrations/Migrations/src/Migrations/Tools/MigrationTpl.php');
use Migrations\Tools\MigrationTpl;

class m161129_093011_oos_comands_log_table extends MigrationTpl
{

// Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('oos_comands_log',
            [
                'id' => 'bigserial NOT NULL',
                'params' => 'json',
                'user_id' => 'bigint NOT NULL',
                'comand_id' => 'bigint NOT NULL',
                'date_created' => 'timestamp NOT NULL DEFAULT NOW()'
            ]
        );

        $this->addPrimaryKey('oos_comands_log_primary_key', 'oos_comands_log', 'id');
        $this->createIndex('oos_comands_log_date_created_idx', 'oos_comands_log', 'date_created');
        $this->createIndex('oos_comands_log_user_id_idx', 'oos_comands_log', 'user_id');
        $this->createIndex('oos_comands_log_comand_id_idx', 'oos_comands_log', 'comand_id');
    }

    public function safeDown()
    {
        echo "m161129_093011_oos_comands_log_table cannot be reverted.\n";

        return false;
    }
}
