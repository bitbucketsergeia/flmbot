<?php
require_once('vendor/stdlib-migrations/Migrations/src/Migrations/Tools/MigrationTpl.php');
use Migrations\Tools\MigrationTpl;

class m161129_093103_oos_comands_weather_table extends MigrationTpl
{
    public function safeUp()
    {
        //Погода
        $this->insert('oos_comands', [
            'id' => '9',
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => '5',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 999,
            'is_prev' => 1
        ]);

        $this->insert('oos_comands', [
            'id' => '10',
            'title_en' => '',
            'title_es' => '',
            'title_it' => '',
            'title_fr' => '',
            'title_de' => '',
            'title_ru' => '',
            'parent' => '5',
            'description_en' => '{weather}',
            'description_es' => '{weather}',
            'description_it' => '{weather}',
            'description_fr' => '{weather}',
            'description_de' => '{weather}',
            'description_ru' => '{weather}',
            'request_contact' => 0,
            'request_location' => 1,
            'is_read' => 1,
            'sort' => 1000,
            'is_prev' => 0
        ]);

        $this->insert('oos_comands', [
            'id' => '11',
            'title_en' => 'Send geolocation details',
            'title_es' => 'Enviar geolocalización',
            'title_it' => 'Invia i dati della geolocalizzazione',
            'title_fr' => 'Envoyer une géolocalisation',
            'title_de' => 'Standort senden',
            'title_ru' => 'Отправить геопозицию',
            'parent' => '5',
            'description_en' => '{weather}',
            'description_es' => '{weather}',
            'description_it' => '{weather}',
            'description_fr' => '{weather}',
            'description_de' => '{weather}',
            'description_ru' => '{weather}',
            'request_contact' => 0,
            'request_location' => 1,
            'is_read' => 1,
            'sort' => 1000,
            'is_prev' => 0
        ]);

        $this->insert('oos_comands', [
            'id' => '12',
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => '11',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 1000,
            'is_prev' => 1
        ]);

        $this->insert('oos_comands', [
            'id' => '13',
            'title_en' => 'Main menu',
            'title_es' => 'Menú principal',
            'title_it' => 'Menu principale',
            'title_fr' => 'Menu principal',
            'title_de' => 'Hauptmenü',
            'title_ru' => 'Главное меню',
            'parent' => '11',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 999,
            'is_main' => 1
        ]);


        $this->insert('oos_comands', [
            'id' => '23',
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => '10',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 1000,
            'is_prev' => 1
        ]);

        $this->insert('oos_comands', [
            'id' => '24',
            'title_en' => 'Main menu',
            'title_es' => 'Menú principal',
            'title_it' => 'Menu principale',
            'title_fr' => 'Menu principal',
            'title_de' => 'Hauptmenü',
            'title_ru' => 'Главное меню',
            'parent' => '10',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 999,
            'is_main' => 1
        ]);

    }

    public function safeDown()
    {
        echo "m161129_093103_oos_comands_table cannot be reverted.\n";

        return false;
    }
}
