<?php
require_once('vendor/stdlib-migrations/Migrations/src/Migrations/Tools/MigrationTpl.php');
use Migrations\Tools\MigrationTpl;

class m161129_093103_oos_comands_screens_table extends MigrationTpl
{
    public function safeUp()
    {

        //Главный языки

        $langs = [
            'English', 'Español', 'Italiano', 'Français', 'Deutsch', 'Русский'
        ];

        $i1 = 12000;
        $countPP = count($langs);

        for ($i = 0; $i < $countPP; $i++) {
            $this->insert('oos_comands', [
                'id' => $i1 + $i+1,
                'title_en' => $langs[$i],
                'title_es' => $langs[$i],
                'title_it' => $langs[$i],
                'title_fr' => $langs[$i],
                'title_de' => $langs[$i],
                'title_ru' => $langs[$i],
                'parent' => $i1,
                'description_en' => 'Choose language',
                'description_es' => 'Seleccione el idioma',
                'description_it' => 'Scegli la lingua',
                'description_fr' => 'Choisissez la langue',
                'description_de' => 'Wählen Sie die Sprache aus',
                'description_ru' => 'Выберите язык:',
                'request_contact' => 0,
                'request_location' => 0,
                'is_read' => 1,
                'sort' => 100000 - $i,
                'is_prev' => 0,
                'next' => 0
            ]);
        }


        //Экран языки

        $langs = [
            'English', 'Español', 'Italiano', 'Français', 'Deutsch', 'Русский'
        ];

        $i1 = 100;
        $countPP = count($langs);

        for ($i = 0; $i < $countPP; $i++) {
            $this->insert('oos_comands', [
                'id' => $i1 + $i + 1,
                'title_en' => $langs[$i],
                'title_es' => $langs[$i],
                'title_it' => $langs[$i],
                'title_fr' => $langs[$i],
                'title_de' => $langs[$i],
                'title_ru' => $langs[$i],
                'parent' => $i1,
                'description_en' => 'Choose language',
                'description_es' => 'Seleccione el idioma',
                'description_it' => 'Scegli la lingua',
                'description_fr' => 'Choisissez la langue',
                'description_de' => 'Wählen Sie die Sprache aus',
                'description_ru' => 'Выберите язык:',
                'request_contact' => 0,
                'request_location' => 0,
                'is_read' => 1,
                'sort' => 1000 - $i,
                'is_prev' => 0,
                'next' => 0
            ]);
        }

        $this->insert('oos_comands', [
            'id' => 11000,
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => $i1,
            'description_en' => '<b>Use the menu to change current settings</b>',
            'description_es' => '<b>Para cambiar la configuración actual, utilice el menú</b>',
            'description_it' => '<b>Utilizza il menu per modificare le impostazioni correnti</b>',
            'description_fr' => '<b>Pour changer les paramètres courants, utilisez le menu</b>',
            'description_de' => '<b>Ändern Sie die aktuellen Einstellungen über das Menü</b>',
            'description_ru' => '<b>Чтобы изменить текущие настройки, воспользуйтесь меню</b>',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 2,
            'is_prev' => 0,
            'is_main' => 0,
            'next'=> 7
        ]);

        $cities = $this->getDbConnection()->createCommand('SELECT * FROM public.oos_cities WHERE region = 0 ORDER BY id ASC')->queryAll();

        $i1 = 150;
        $i = 0;

        foreach($cities as $city) {
            $this->insert('oos_comands', [
                'id' => $i1 + $i + 1,
                'title_en' => $city['title_en'],
                'title_es' => $city['title_es'],
                'title_it' => $city['title_it'],
                'title_fr' => $city['title_fr'],
                'title_de' => $city['title_de'],
                'title_ru' => $city['title_ru'],
                'parent' => $i1,
                'description_en' => 'In what region would you like to use the service?',
                'description_es' => '¿En qué región desea utilizar el servicio?',
                'description_it' => 'In quale regione vorresti utilizzare il servizio?',
                'description_fr' => 'Dans la quelle région voulez-Vous utiliser le service?',
                'description_de' => 'In welcher Region wollen Sie den Dienst ausnutzen?',
                'description_ru' => 'В каком регионе Вы хотите пользоваться сервисом?',
                'request_contact' => 0,
                'request_location' => 0,
                'is_read' => 1,
                'sort' => 1000 - $i,
                'is_prev' => 0,
                'next' => 0
            ]);
            $i++;
        }

        $this->insert('oos_comands', [
            'id' => 11001,
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => $i1,
            'description_en' => '<b>Use the menu to change current settings</b>',
            'description_es' => '<b>Para cambiar la configuración actual, utilice el menú</b>',
            'description_it' => '<b>Utilizza il menu per modificare le impostazioni correnti</b>',
            'description_fr' => '<b>Pour changer les paramètres courants, utilisez le menu</b>',
            'description_de' => '<b>Ändern Sie die aktuellen Einstellungen über das Menü</b>',
            'description_ru' => '<b>Чтобы изменить текущие настройки, воспользуйтесь меню</b>',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 2,
            'is_prev' => 0,
            'is_main' => 0,
            'next' => 7
        ]);

        //В заказе

        $i1 = 12500;
        $i = 0;

        foreach($cities as $city) {
            $this->insert('oos_comands', [
                'id' => $i1 + $i + 1,
                'title_en' => $city['title_en'],
                'title_es' => $city['title_es'],
                'title_it' => $city['title_it'],
                'title_fr' => $city['title_fr'],
                'title_de' => $city['title_de'],
                'title_ru' => $city['title_ru'],
                'parent' => $i1,
                'description_en' => 'In what region would you like to use the service?',
                'description_es' => '¿En qué región desea utilizar el servicio?',
                'description_it' => 'In quale regione vorresti utilizzare il servizio?',
                'description_fr' => 'Dans la quelle région voulez-Vous utiliser le service?',
                'description_de' => 'In welcher Region wollen Sie den Dienst ausnutzen?',
                'description_ru' => 'В каком регионе Вы хотите пользоваться сервисом?',
                'request_contact' => 0,
                'request_location' => 0,
                'is_read' => 1,
                'sort' => 100000 - $i,
                'is_prev' => 0,
                'next' => 0
            ]);
            $i++;
        }

        $this->insert('oos_comands', [
            'id' => 12520,
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => $i1,
            'description_en' => '<b>Select desired menu item</b>',
            'description_es' => '<b>Seleccione la opción de menú que más le interese</b>',
            'description_it' => '<b>Seleziona la voce del menu desiderata</b>',
            'description_fr' => '<b>Choisissez un élément de menu, qui Vous intéresse</b>',
            'description_de' => '<b>Wählen Sie den gewünschten Menüpunkt aus</b>',
            'description_ru' => '<b>Выберите интересующий Вас пункт меню</b>',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 2,
            'is_prev' => 0,
            'is_main' => 0,
            'next' => 1
        ]);

        //Экран города
        $cities = $this->getDbConnection()->createCommand('SELECT * FROM public.oos_cities WHERE region = 5 ORDER BY id ASC')->queryAll();

        $i1 = 200;
        $i = 0;

        foreach($cities as $city) {
            $this->insert('oos_comands', [
                'id' => $i1 + $i + 1,
                'title_en' => $city['title_en'],
                'title_es' => $city['title_es'],
                'title_it' => $city['title_it'],
                'title_fr' => $city['title_fr'],
                'title_de' => $city['title_de'],
                'title_ru' => $city['title_ru'],
                'parent' => $i1,
                'description_en' => 'Choose city',
                'description_es' => 'Seleccione la ciudad',
                'description_it' => 'Scegli la città',
                'description_fr' => 'Coisissez la ville',
                'description_de' => 'Wählen Sie die Stadt aus',
                'description_ru' => 'Выберите город',
                'request_contact' => 0,
                'request_location' => 0,
                'is_read' => 1,
                'sort' => 1000 - $i,
                'is_prev' => 0,
                'next' => 0
            ]);
            $i++;
        }

        $this->insert('oos_comands', [
            'id' => 11002,
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => $i1,
            'description_en' => 'In what region would you like to use the service?',
            'description_es' => '¿En qué región desea utilizar el servicio?',
            'description_it' => 'In quale regione vorresti utilizzare il servizio?',
            'description_fr' => 'Dans la quelle région voulez-Vous utiliser le service?',
            'description_de' => 'In welcher Region wollen Sie den Dienst ausnutzen?',
            'description_ru' => 'В каком регионе Вы хотите пользоваться сервисом?',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 2,
            'is_prev' => 0,
            'is_main' => 0,
            'next' => 150
        ]);


    }

    public function safeDown()
    {
        echo "m161129_093103_oos_comands_screens_table cannot be reverted.\n";

        return false;
    }
}
