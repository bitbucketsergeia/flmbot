<?php
require_once('vendor/stdlib-migrations/Migrations/src/Migrations/Tools/MigrationTpl.php');
use Migrations\Tools\MigrationTpl;

class m161129_093103_oos_comands_rent_table extends MigrationTpl
{
    public function safeUp()
    {
        //Аренда
        $this->insert('oos_comands', [
            'id' => '15',
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => '3',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 1000,
            'is_prev' => 1
        ]);

        //Аренда
        $this->insert('oos_comands', [
            'id' => '999',
            'title_en' => 'Back',
            'title_es' => 'Volver',
            'title_it' => 'Indietro',
            'title_fr' => 'Précédent',
            'title_de' => 'Zurück',
            'title_ru' => 'Назад',
            'parent' => '998',
            'description_en' => '',
            'description_es' => '',
            'description_it' => '',
            'description_fr' => '',
            'description_de' => '',
            'description_ru' => '',
            'request_contact' => 0,
            'request_location' => 0,
            'is_read' => 0,
            'sort' => 1000,
            'is_prev' => 1
        ]);
    }

    public function safeDown()
    {
        echo "m161129_093103_oos_comands_table cannot be reverted.\n";

        return false;
    }
}
