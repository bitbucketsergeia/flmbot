<?php
require_once('vendor/stdlib-migrations/Migrations/src/Migrations/Tools/MigrationTpl.php');
use Migrations\Tools\MigrationTpl;

class m161129_093013_oos_lang_table extends MigrationTpl
{

// Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('oos_lang',
            [
                'id' => 'bigserial NOT NULL',
                'lang' => 'character varying(30) NULL',
                'sort' => 'integer NOT NULL',
                'code' => 'character varying(30) NULL',
            ]
        );

        $this->addPrimaryKey('oos_lang_primary_key', 'oos_lang', 'id');

        $this->insert('oos_lang', [
            'id' => 1,
            'lang' => 'English',
            'sort' => 100,
            'code'=> 'en'
        ]);
        $this->insert('oos_lang', [
            'id' => 2,
            'lang' => 'Español',
            'sort' => 99,
            'code'=> 'es'
        ]);
        $this->insert('oos_lang', [
            'id' => 3,
            'lang' => 'Italiano',
            'sort' => 98,
            'code'=> 'it'
        ]);
        $this->insert('oos_lang', [
            'id' => 4,
            'lang' => 'Français',
            'sort' => 97,
            'code'=> 'fr'
        ]);
        $this->insert('oos_lang', [
            'id' => 5,
            'lang' => 'Deutsch',
            'sort' => 96,
            'code'=> 'de'
        ]);
        $this->insert('oos_lang', [
            'id' => 6,
            'lang' => 'Русский',
            'sort' => 95,
            'code'=> 'ru'
        ]);
    }

    public function safeDown()
    {
        echo "m161129_093013_oos_lang_table cannot be reverted.\n";

        return false;
    }
}