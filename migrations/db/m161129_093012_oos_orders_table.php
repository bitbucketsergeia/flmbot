<?php
require_once('vendor/stdlib-migrations/Migrations/src/Migrations/Tools/MigrationTpl.php');
use Migrations\Tools\MigrationTpl;

class m161129_093012_oos_orders_table extends MigrationTpl
{

// Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('oos_orders',
            [
                'id' => 'bigserial NOT NULL',
                'params' => 'json',
                'user_id' => 'bigint NOT NULL',
                'status' => 'character varying(30) NOT NULL', //SAVE, WAIT, FINISH, CANCEL,
                'external_id' => 'character varying(30)',
                'date_created' => 'timestamp NOT NULL DEFAULT NOW()'
            ]
        );

        $this->addPrimaryKey('oos_orders_primary_key', 'oos_orders', 'id');
        $this->createIndex('oos_orders_date_created_idx', 'oos_orders', 'date_created');
        $this->createIndex('oos_orders_user_id_idx', 'oos_orders', 'user_id');
    }

    public function safeDown()
    {
        echo "m161129_093012_oos_orders_table cannot be reverted.\n";

        return false;
    }
}
