<?php
require_once('vendor/stdlib-migrations/Migrations/src/Migrations/Tools/MigrationTpl.php');
use Migrations\Tools\MigrationTpl;

class m161129_103512_oos_log extends MigrationTpl
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute('CREATE SEQUENCE "oos_log_id_seq"');

        $this->createTable('oos_log', [
            'id'      => 'bigint DEFAULT nextval(\'oos_log_id_seq\') NOT NULL',
            'date'    => 'timestamp NULL DEFAULT now()',
            'ip'      => 'character varying(15) NULL',
            'type'      => 'character varying(15) NULL',
            'action'  => 'text NULL',
            'chat_id'      => 'character varying(32) NULL',
            'params'  => 'json NULL',
            'status'  => 'integer NULL',
            'details' => 'json NULL',
        ]);
        $this->grantTablePermissions('oos_log', 'oos_log_id_seq');

        $this->createIndex('oos_log_pkey', 'oos_log', 'id');

        $this->execute("
            CREATE OR REPLACE FUNCTION oos_log_trigger()
            RETURNS TRIGGER AS
            \$BODY$
            DECLARE
            table_master varchar(255) := 'oos_log';
            table_part varchar(255) := '';
            BEGIN
            -- Даём имя партиции --------------------------------------------------
                table_part := table_master
                                || '_' || date_part( 'year', NEW.date )::text
                                || '_' || date_part( 'month', NEW.date )::text;
            -- Проверяем партицию на существование --------------------------------
            PERFORM 1 FROM pg_class WHERE relname = table_part LIMIT 1;
            -- Если её ещё нет, то создаём --------------------------------------------
            IF NOT FOUND
            THEN
            -- Создаём партицию, наследуя мастер-таблицу --------------------------
            EXECUTE 'CREATE TABLE ' || table_part || ' (
                    id bigint NOT NULL DEFAULT nextval(''' || table_master || '_id_seq''::regclass),
                    CONSTRAINT ' || table_part || '_id_pk PRIMARY KEY (id)
            ) INHERITS ( ' || table_master || ' ) WITH ( OIDS=FALSE );';
            EXECUTE 'GRANT ALL ON TABLE ' || table_part || ' TO \"" . $this->_dbUsers['db_owner'] . "\";';
            EXECUTE 'GRANT ALL ON TABLE ' || table_part || ' TO \"" . $this->_dbUsers['server_user'] . "\";';
            EXECUTE 'GRANT ALL ON TABLE ' || table_part || ' TO \"" . $this->_dbUsers['rw_group'] . "\";';
            EXECUTE 'GRANT SELECT ON TABLE ' || table_part || ' TO \"" . $this->_dbUsers['ro_group'] . "\";';

            END IF;
            -- Вставляем данные в партицию --------------------------------------------
            EXECUTE '
            INSERT INTO ' || table_part || ' SELECT ( (' || QUOTE_LITERAL(NEW) || ')::' || TG_RELNAME || ' ).*;';
            RETURN NULL;
            END;
            \$BODY$
            LANGUAGE plpgsql SECURITY DEFINER
            COST 100;
        ");

        $this->execute("CREATE TRIGGER oos_log_trigger BEFORE INSERT ON oos_log FOR EACH ROW EXECUTE PROCEDURE oos_log_trigger();");
    }

    public function safeDown()
    {
        echo "m161129_103512_oos_log cannot be reverted.\n";

        return true;
    }

}
