<?php
require_once('vendor/stdlib-migrations/Migrations/src/Migrations/Tools/MigrationTpl.php');
use Migrations\Tools\MigrationTpl;

class m161129_093014_oos_cities_table extends MigrationTpl
{

// Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('oos_cities',
            [
                'id' => 'bigserial NOT NULL',
                'title_en' => 'character varying(30) NULL',
                'title_es' => 'character varying(30) NULL',
                'title_it' => 'character varying(30) NULL',
                'title_fr' => 'character varying(30) NULL',
                'title_de' => 'character varying(30) NULL',
                'title_ru' => 'character varying(30) NULL',
                'sort' => 'integer NOT NULL',
                'po' => 'character varying(30) NOT NULL',
                'region' => 'integer NOT NULL',
                'timezone' => 'integer NULL',
            ]
        );

        $this->addPrimaryKey('oos_cities_primary_key', 'oos_cities', 'id');

        $this->insert('oos_cities', [
            'id' => 1,
            'title_en' => 'Moscow',
            'title_es' => 'Moscú',
            'title_it' => 'Mosca',
            'title_fr' => 'Moscou',
            'title_de' => 'Moskau',
            'title_ru' => 'Москва',
            'sort' => '99',
            'po' => 'taxik',
            'region' => '0',
            'timezone' => '3'
        ]);

        $this->insert('oos_cities', [
            'id' => 2,
            'title_en' => 'Saint Petersburg',
            'title_es' => 'San Petersburgo',
            'title_it' => 'San Pietroburgo',
            'title_fr' => 'Saint-Pétersbourg',
            'title_de' => 'Sankt Petersburg',
            'title_ru' => 'Санкт-Петербург',
            'sort' => '98',
            'po' => 'liga',
            'region' => '0',
            'timezone' => '3'
        ]);

        $this->insert('oos_cities', [
            'id' => 3,
            'title_en' => 'Sochi',
            'title_es' => 'Sochi',
            'title_it' => 'Sochi',
            'title_fr' => 'Sotchi',
            'title_de' => 'Sotschi',
            'title_ru' => 'Сочи',
            'sort' => '97',
            'po' => 'xdn',
            'region' => '0',
            'timezone' => '3'
        ]);

        $this->insert('oos_cities', [
            'id' => 4,
            'title_en' => 'Omsk',
            'title_es' => 'Omsk',
            'title_it' => 'Omsk',
            'title_fr' => 'Omsk',
            'title_de' => 'Omsk',
            'title_ru' => 'Омск',
            'sort' => '96',
            'po' => 'hive',
            'region' => '0',
            'timezone' => '6'
        ]);


        $this->insert('oos_cities', [
            'id' => 5,
            'title_en' => 'Crimea',
            'title_es' => 'Crimea',
            'title_it' => 'Crimea',
            'title_fr' => 'Crimée',
            'title_de' => 'Krim',
            'title_ru' => 'Крым',
            'sort' => '95',
            'po' => 'hive',
            'region' => '0',
        ]);


        $this->insert('oos_cities', [
            'id' => 6,
            'title_en' => 'Sevastopol',
            'title_es' => 'Sebastopol',
            'title_it' => 'Sevastopol',
            'title_fr' => 'Sébastopol',
            'title_de' => 'Sewastopol',
            'title_ru' => 'Севастополь',
            'sort' => '94',
            'po' => 'hive',
            'region' => '5',
            'timezone' => '3'
        ]);


        $this->insert('oos_cities', [
            'id' => 7,
            'title_en' => 'Alushta',
            'title_es' => 'Alushta',
            'title_it' => 'Alušta',
            'title_fr' => 'Alouchta',
            'title_de' => 'Aluschta',
            'title_ru' => 'Алушта',
            'sort' => '93',
            'po' => 'hive',
            'region' => '5',
            'timezone' => '3'
        ]);


        $this->insert('oos_cities', [
            'id' => 8,
            'title_en' => 'Simferopol',
            'title_es' => 'Simferópol',
            'title_it' => 'Simferopol',
            'title_fr' => 'Simferopol',
            'title_de' => 'Simferopol',
            'title_ru' => 'Симферополь',
            'sort' => '92',
            'po' => 'hive',
            'region' => '5',
            'timezone' => '3'
        ]);

        $this->insert('oos_cities', [
            'id' => 9,
            'title_en' => 'Krasnogvardeyskoye',
            'title_es' => 'Krasnogvardéiskoye',
            'title_it' => 'Krasnogvardeyskoye',
            'title_fr' => 'Krasnogvardeiskoye',
            'title_de' => 'Krasnogwardeiskoje',
            'title_ru' => 'Красногвардейское',
            'sort' => '91',
            'po' => 'hive',
            'region' => '5',
            'timezone' => '3'
        ]);


        $this->insert('oos_cities', [
            'id' => 10,
            'title_en' => 'Yalta',
            'title_es' => 'Yalta',
            'title_it' => 'Jalta',
            'title_fr' => 'Yalta',
            'title_de' => 'Jalta',
            'title_ru' => 'Ялта',
            'sort' => '90',
            'po' => 'hive',
            'region' => '5',
            'timezone' => '3'
        ]);


        $this->insert('oos_cities', [
            'id' => 11,
            'title_en' => 'Sudak',
            'title_es' => 'Sudak',
            'title_it' => 'Sudak',
            'title_fr' => 'Soudak',
            'title_de' => 'Sudak',
            'title_ru' => 'Судак',
            'sort' => '89',
            'po' => 'hive',
            'region' => '5',
            'timezone' => '3'
        ]);

        $this->insert('oos_cities', [
            'id' => 12,
            'title_en' => 'Saky',
            'title_es' => 'Saki',
            'title_it' => 'Saky',
            'title_fr' => 'Saki',
            'title_de' => 'Saky',
            'title_ru' => 'Саки',
            'sort' => '89',
            'po' => 'hive',
            'region' => '5',
            'timezone' => '3'
        ]);
    }

    public function safeDown()
    {
        echo "m161129_093014_oos_cities_table cannot be reverted.\n";

        return false;
    }
}