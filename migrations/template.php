<?php

/* @var $className string the new migration class name */

echo "<?php\n";
?>
require_once ('vendor/stdlib-migrations/Migrations/src/Migrations/Tools/MigrationTpl.php');
use Migrations\Tools\MigrationTpl;

class <?= $className ?> extends MigrationTpl
{

// Use safeUp/safeDown to run migration code within a transaction
public function safeUp()
{
}

public function safeDown()
{
echo "<?= $className ?> cannot be reverted.\n";
return false;
}

}
