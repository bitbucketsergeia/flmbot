<?php

namespace Services\Validators;

use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;

class FileExists extends Validator
{
    /**
     * @param Validation $validator
     * @param string $attribute
     * @return bool
     */
    public function validate(Validation $validator, $attribute)
    {
        $value = $validator->getValue($attribute);

        if (!is_readable($value) || !file_exists($value)) {
            $message = $this->getOption("message");

            if (!$message) {
                $message = "File is not exists";
            }

            $validator->appendMessage(
                new Message($message, $attribute, "FileExists")
            );

            return false;
        }
        return true;
    }
}