<?php

namespace Services;

use \Phalcon\Di\FactoryDefault;
use Services\Exceptions\NoFileException;

class ErrorLogger
{
    /** @var \Phalcon\Logger\Adapter\Syslog $sysLogger */
    protected $sysLogger;
    /** @var \Phalcon\Logger\Adapter\File $fileLogger */
    protected $fileLogger;

    public function __construct(\Phalcon\Logger\Adapter\Syslog $sysLogger = null, \Phalcon\Logger\Adapter\File $fileLogger = null)
    {
        $this->sysLogger = $sysLogger;
        $this->fileLogger = $fileLogger;
    }

    /**
     * @param \Throwable $e
     * @return $this
     */
    public function logException(\Throwable $e)
    {
        $id = base_convert(rand(1, 10) . '' . microtime(false), 10, 36);

        $info = [];
        if (isset($_SERVER['REQUEST_URI'])) {
            $info[] = 'Method: ' . $_SERVER['REQUEST_URI'];
        }
        if (isset($_POST['source'])) {
            $info[] = 'Source: ' . $_POST['source'];
        }

        $info = implode(', ', $info);
        if ($this->sysLogger) {
            if ($e instanceof NoFileException) {
                $this->sysLogger->warning('#' . $id . ' Info: ' . $info . ' Message: ' . $e->getMessage());
            } else {
                $this->sysLogger->error('#' . $id . ' Info: ' . $info . ' Message: ' . $e->getMessage());
            }
        }

        if ($this->fileLogger) {
            $this->fileLogger->critical("[Uncaught exception #$id]\n" . "Info: $info\n" . $e->getMessage() . "\n" . $e->getTraceAsString());
        }

        return $this;
    }

    /**
     * @param $type
     * @param $message
     * @return $this
     */
    public function log($type, $message)
    {
        if ($this->sysLogger) {
            $this->sysLogger->log($type, $message);
        }

        return $this;
    }
}