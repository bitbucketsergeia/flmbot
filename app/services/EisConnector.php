<?php

namespace Services;

use Util\Oos\Ftp\CoreFtp;
use Util\FileManager\FileManager;

class EisConnector
{
    /**
     * CoreFtp Class
     *
     * @var CoreFtp
     */
    private $ftp;

    /**
     * File Manager Class
     *
     * @var FileManager
     */
    private $fileManager;

    /**
     * Получение данных из FTP
     *
     * @param $connectionParams
     * @param $requestParams
     * @param $tmpFolder
     * @return \DirectoryIterator|null
     * @throws \Exception
     * @throws \FtpException
     */
    public function getFromFtp($connectionParams, $requestParams, $tmpFolder)
    {
        if ($this->connectToFtp($connectionParams)) {
            $this->fileManager->makeSavePath($tmpFolder);
            $this->copyFilesFromFtp($requestParams['path'], $requestParams, $tmpFolder);
            $this->closeFtp();
            return new \DirectoryIterator($tmpFolder);
        }
        return null;
    }

    /**
     * Копирование данных с ФТП и распаковка архивов
     *
     * @param $rootDir
     * @param $requestParams
     * @param $tmpFolder
     * @throws \Exception
     * @throws \FtpException
     */
    private function copyFilesFromFtp($rootDir, $requestParams, $tmpFolder)
    {
        $parts = explode('*', $rootDir);

        if (count($parts) === 2) {
            $realRoot = $parts[0];
            $newRoots = $this->ftp->ls($realRoot);

            foreach ($newRoots as $newRoot) {
                if (!$this->isDir($newRoot)) {
                    continue;
                }

                $this->copyFilesFromFtp($realRoot . $newRoot['name'] . '/', $requestParams, $tmpFolder);
            }
            return;
        }

        $items = $this->ftp->ls($rootDir);

        foreach ($items as $item) {
            $targetSubFolder = $item['name'] . '/' . $requestParams['type'];

            if (!$this->isDir($item) || !$this->ftp->isDir($rootDir . $targetSubFolder)) {
                continue;
            }

            $xmls = $this->ftp->ls($rootDir . $targetSubFolder);

            if (!$xmls) {
                continue;
            }

            foreach ($xmls as $xml) {

                if (!$this->isTargetFile(
                    '.*_' . $requestParams['date']->format('Ymd') . '_.*',
                    $xml,
                    $requestParams['all'] == '1'
                )
                ) {
                    continue;
                }

                $filePath = $rootDir . $targetSubFolder . '/' . $xml['name'];
                $fileName = basename($filePath);
                $currentFileFolder = $tmpFolder . '/' . $fileName;
                $this->fileManager->makeSavePath($currentFileFolder);
                $localPath = $currentFileFolder . '/' . $fileName;

                if (!file_exists($localPath)) {
                    $fp = fopen($localPath, 'w');

                    if (!$fp) {
                        throw new \FtpException('Не удается создать файл для записи');
                    }

                    $this->ftp->fget($fp, $filePath, CoreFtp::BINARY);
                    fclose($fp);
                    $this->fileManager->unzip($localPath, $currentFileFolder);
                    $this->fileManager->removeFile($localPath);
                }
            }
        }
    }

    /**
     * Check Dir
     *
     * @param $item
     * @return bool
     */
    private function isDir($item)
    {
        return $item['type'] === CoreFtp::TYPE_DIR;
    }

    /**
     * Check FileName
     *
     * @param $mask
     * @param $fileInfo
     * @return bool
     */
    private function isTargetFile($mask, $fileInfo, $withoutMask = false)
    {
        $isTargetFile = $fileInfo['type'] === CoreFtp::TYPE_FILE
            && ((preg_match("@{$mask}@", $fileInfo['name']) && !$withoutMask) || $withoutMask);

        return $isTargetFile;
    }

    /**
     * Connection to FTP
     *
     * @param $options
     * @return bool
     */
    private function connectToFtp($options)
    {
        try {
            $this->ftp->connect($options['host']);
            $this->ftp->login($options['login'], $options['password']);
            $this->ftp->pasv(true);
        } catch (\FtpException $e) {
            return false;
        }
        return true;
    }

    /**
     * Close Ftp connection
     */
    private function closeFtp()
    {
        try {
            $this->ftp->close();
        } catch (\FtpException $e) {
        }
    }
}