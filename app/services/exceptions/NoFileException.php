<?php

namespace Services\Exceptions;

class NoFileException extends \Exception
{
  protected $message = "Файл не найден или поврежден";
}