<?php

namespace Services;

use Models\OosData;
use MongoDB\BSON\UTCDateTime as MongoUTCDateTime;

class Storage
{
    /**
     * Cохранение данных в MongoDb
     *
     * @param $params
     * @return bool|OosData
     */
    public function save($params)
    {
        try {
            OosData::setStaticSource($params['fz'], $params['type']);
            $oosData = new OosData();
            $oosData->setParams($params['params']);
            $oosData->setDateDownload(new MongoUTCDateTime($params['download_date']));
            $oosData->setDateCreated(new MongoUTCDateTime($params['created_date']));
            $oosData->setGuid($params['guid']);
            $oosData->save();
        } catch (\Exception $e) {
            return false;
        }
        return $oosData;
    }

    /**
     * Get Data From MongoDb
     *
     * @param array $searchParams
     * @param array $order
     * @param int $limit
     * @return array
     */
    public function getOosData($searchParams = [], $order = [], $limit = 0)
    {
        $fz = $searchParams['fz'];
        $type = $searchParams['type'];
        unset($searchParams['fz']);
        unset($searchParams['type']);

        $params = [
            $searchParams,
            "order" => $order,
        ];

        if ($limit > 0) {
            $params['limit'] = $limit;
        }
        
        OosData::setStaticSource($fz, $type);
        $oosData = OosData::find($params);
        return $oosData;
    }
}