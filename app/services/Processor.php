<?php

namespace Services;

use Util\Oos\Xml\XmlToArrayConverter;
use MongoDB\BSON\UTCDatetime as MongoUTCDateTime;

class Processor
{
    /**
     * XmlToArrayConverter Class
     *
     * @var XmlToArrayConverter
     */
    private $xmlToArray;

    public function sendToBitrix($arra)
    {

        $postData = array(
            'TITLE' => 'Заявка из бота',
            'NAME' => $arra['name'],
            'PHONE_WORK' => $arra['phone'],
            'EMAIL_WORK' => $arra['email'],
            'COMMENTS' => $arra['message']
        );

        // добавляем в массив параметры авторизации
        if (defined('CRM_AUTH')) {
            $postData['AUTH'] = CRM_AUTH;
        } else {
            $postData['LOGIN'] = CRM_LOGIN;
            $postData['PASSWORD'] = CRM_PASSWORD;
        }

        // открываем сокет соединения к облачной CRM
        $fp = fsockopen("ssl://" . CRM_HOST, CRM_PORT, $errno, $errstr, 30);
        if ($fp) {
            // производим URL-кодирование строки
            $strPostData = '';
            foreach ($postData as $key => $value)
                $strPostData .= ($strPostData == '' ? '' : '&') . $key . '=' . urlencode($value);

            // подготавливаем заголовки
            $str = "POST " . CRM_PATH . " HTTP/1.0\r\n";
            $str .= "Host: " . CRM_HOST . "\r\n";
            $str .= "Content-Type: application/x-www-form-urlencoded\r\n";
            $str .= "Content-Length: " . strlen($strPostData) . "\r\n";
            $str .= "Connection: close\r\n\r\n";

            $str .= $strPostData;

            fwrite($fp, $str);

            $result = '';
            while (!feof($fp)) {
                $result .= fgets($fp, 128);
            }
            fclose($fp);
            $response = explode("\r\n\r\n", $result);

            //echo '&lt;pre>' . print_r($response[1], 1) . '&lt;/pre>';
        } else {
            //echo 'Не удалось подключиться к CRM ' . $errstr . ' (' . $errno . ')';
        }
    }

    /**
     * Подготовка параметров для сохранения
     *
     * @param $fz
     * @param $jsonData
     * @param \DirectoryIterator $fileData
     * @return array
     */
    public function createSaveParams($fz, $jsonData, \DirectoryIterator $fileData)
    {
        $params = explode('_', $fileData->getBasename());
        $type = !empty($params) ? $params[0] : '';
        $dateDownload = (new \DateTime())->setTimestamp($fileData->getMTime());
        $dateCreated = null;
        $guid = '';

        if (isset($jsonData['body']['item'][$type . 'Data']['createDateTime'])) {
            $dateCreated = new \DateTime($jsonData['body']['item'][$type . 'Data']['createDateTime']);
        } elseif (isset($jsonData['body']['item'][$type . 'Data']['creationDateTime'])) {
            $dateCreated = new \DateTime($jsonData['body']['item'][$type . 'Data']['creationDateTime']);
        }

        if (isset($jsonData['body']['item'][$type . 'Data']['guid'])) {
            $guid = $jsonData['body']['item'][$type . 'Data']['guid'];
        }

        return [
            'params' => $jsonData,
            'fz' => $fz,
            'created_date' => $dateCreated,
            'download_date' => $dateDownload,
            'type' => $type,
            'guid' => $guid
        ];
    }

    /**
     * Подготовка данных для ответа
     *
     * @param $oosData
     * @return array
     */
    public function getDataForResponse($oosData)
    {
        $outputData = [];

        if (!empty($oosData)) {
            foreach ($oosData as $data) {
                $outputData[] = [
                    'params' => $data->getParams(),
                    'created_date' => $data->getDateCreated()->toDateTime()->format(\DateTime::ATOM),
                    'download_date' => $data->getDateDownload()->toDateTime()->format(\DateTime::ATOM),
                    'guid' => $data->getGuid()
                ];
            }
        }

        return $outputData;
    }

    /**
     * Подготовка параметров для поиска данных
     *
     * @param $requestParams
     * @return array
     */
    public function getParamsForSearch($requestParams)
    {
        $params = [
            'fz' => $requestParams['fz'],
            'type' => $requestParams['type']
        ];

        if (!empty($requestParams['dt'])) {
            try {
                $params['date_created'] = $this->getCripteriaForCurrentDate(new \DateTime($requestParams['dt']));
            } catch (\Exception $e) {
                $params['date_created'] = $this->getCripteriaForCurrentDate(new \DateTime());
            }
        } elseif (!empty($requestParams['dt_from']) && !empty($requestParams['dt_to'])) {
            try {
                $params['date_created'] = [
                    '$gt' => new MongoUTCDateTime(new \DateTime($requestParams['dt_from'])),
                    '$lt' => new MongoUTCDateTime(new \DateTime($requestParams['dt_to']))
                ];
            } catch (\Exception $e) {
                $params['date_created'] = $this->getCripteriaForCurrentDate(new \DateTime());
            }
        }

        if (!empty($requestParams['guid'])) {
            $params['guid'] = $requestParams['guid'];
        }

        return $params;
    }

    /**
     * @param \DateTime $date
     * @return array
     */
    private function getCripteriaForCurrentDate(\DateTime $date)
    {
        $strDate = $date->format("Y-m-d");
        $date1 = new \DateTime($strDate . " 00:00:00");
        $date2 = new \DateTime($strDate . " 23:59:59");
        return [
            '$gt' => new MongoUTCDateTime($date1),
            '$lt' => new MongoUTCDateTime($date2)
        ];
    }
}