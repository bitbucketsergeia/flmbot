<?php

namespace Services;

use Models\OosFiles;

class EisLogger
{
    /**
     * Поиск файла в логах
     *
     * @param $filename
     * @return bool
     */
    public function isset($filename)
    {
        return OosFiles::count("filename = '{$filename}'") > 0;
    }

    /**
     * Сохранение логов
     *
     * @param \DirectoryIterator $file
     * @return bool|OosFiles
     */
    public function save(\DirectoryIterator $file)
    {
        try {
            $fileInfo = pathinfo($file->__toString());
            $oosFile = new OosFiles();
            $oosFile->setFilename($fileInfo['basename']);
            $oosFile->setExtension($fileInfo['extension']);
            $dateDownload = (new \DateTime())->setTimestamp($file->getMTime());
            $oosFile->setDateDownload($dateDownload->format('Y-m-d H:i:s'));
            $oosFile->add();
        } catch (\Exception $e) {
            return false;
        }

        return $oosFile;
    }
}