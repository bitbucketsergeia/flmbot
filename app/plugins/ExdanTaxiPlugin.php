<?php

namespace Plugins;

use Phalcon\Http\Client\Request;
use \Phalcon\Di;

class ExdanTaxiPlugin extends TaxiPlugin
{
    const PATH = 'http://client.a.simtax-system.exdan.ru/';
    const API_KEY = 'XfDXlqAYIqDyYAM7p5vdhs4M575qGT9K';
    const TARIF_ECONOM = 1;
    const TARIF_COMFORT = 2;
    const TARIF_BIZ = 3;
    const TARIF_MINI = 4;
    const COMISSION = 25;

    private $provider;
    private $accessKey;

    public function __construct(Di $di, $accessKey = '')
    {
        parent::__construct($di);
        $this->provider = Request::getProvider();
        $this->provider->setBaseUri($this->getPath());
        $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    private function getPath($accessKey = '')
    {
        return self::PATH . '?api_key=' . self::API_KEY . (!empty($this->accessKey) ? '&access_token=' . $this->accessKey : '');
    }

    public function setAccessKey($accessKey)
    {
        $this->accessKey = $accessKey;
        $this->provider->setBaseUri($this->getPath());
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function auth(array $params)
    {
        try {
            $pr = [
                'phone' => $params['phone']
            ];

            $this->logger->debug(print_r($pr, true));
            $response = $this->provider->get('login', $pr);
            $this->logger->debug(print_r($response->body, true));
            return json_decode($response->body, true);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function checkAuth(array $params)
    {
        try {
            $params = [
                'phone' => $params['phone'],
                'code' => $params['code'],
                'os' => '0',
                'device' => (new \DateTime())->getTimestamp()
            ];

            $this->logger->debug('check auth params:' . print_r($params, true));
            $response = $this->provider->post('login', $params);
            $this->logger->debug($response->body);
            return json_decode($response->body, true);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function create(array $params)
    {
        try {

            $pl = [
                'tariff' => $params['tariff'],
                'services' => [],
                "geo" => [
                    $params['in']['lat'] . ', ' . $params['in']['lon'],
                    $params['out']['lat'] . ', ' . $params['out']['lon']
                ],
                "pay" => 0,
                "comment" => $params['comment']
            ];

            if (!$params['is_current']) {
                $pl['date'] = (new \DateTime($params['bookingTime']))->format("d.m.Y H:i");
            }

            $this->logger->debug('create params:' . print_r($pl, true));
            $response = $this->provider->post('order', $pl);
            $this->logger->debug($response->body);
            return json_decode($response->body, true);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function cancel(array $params)
    {
        try {
            $this->logger->debug('cancel params:' . print_r($params, true));
            $response = $this->provider->put('order/' . $params['orderId'] . '/cancel', [
                'id' => $params['orderId']
            ]);
            $this->logger->debug($response->body);
            return json_decode($response->body, true);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function info(array $params)
    {
        try {
            $this->logger->debug('info params:' . print_r($params, true));
            $response = $this->provider->get('order/' . $params['orderId'], [
            ]);

            $this->logger->debug('info: '. print_r($response->body, true));
            return json_decode($response->body, true);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    public function geo(array $params)
    {
        try {
            $this->logger->debug('info params:' . print_r($params, true));
            $response = $this->provider->get('/geo/get-by-name', [
                'query' => $params['query']
            ]);
            $this->logger->debug($response->body);
            return json_decode($response->body, true);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $param
     * @return array|string
     */
    public function update(array $param)
    {
        /*try {
            $this->logger->debug('update params:' . print_r($param, true));
            $params = [];

            $params['orderId'] = $param['orderId'];

            if (!empty($param['bookingTime'])) {
                $params['bookingTime'] = (new \DateTime($param['bookingTime']))->format("Y-m-d H:i:s");
            }

            if (!empty($param['comment'])) {
                $params['comment'] = (new \DateTime($param['comment']))->format("Y-m-d H:i:s");
            }

            $response = $this->provider->post('order/update', $params);

            return $response->body;
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }*/
        return [];
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function price(array $params)
    {
        try {
            $pl = [
                'tariff' => $params['tariff'],
                'services' => [],
                "geo" => [
                    $params['in']['lat'] . ', ' . $params['in']['lon'],
                    $params['out']['lat'] . ', ' . $params['out']['lon']
                ]
            ];

            $this->logger->debug('price params:' . print_r($pl, true));
            $response = $this->provider->get('order/calculate', $pl);
            $this->logger->debug($response->body);
            return json_decode($response->body, true);

        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function tariffs(array $params)
    {
        try {
            $this->logger->debug('tarifs params:' . print_r($params, true));
            $response = $this->provider->get(
                'tariff',
                [

                ]
            );

            return $response->body;
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param $region
     * @param $id
     * @return int
     */
    public function getTarif($region, $id)
    {
        $tariff = 1;

        switch ($id) {
            case 507:
                $tariff = 1;
                break;
            case 508:
                $tariff = 3;
                break;
            case 509:
                $tariff = 2;
                break;
        }

        return $tariff;
    }

    /**
     * @return int
     */
    public function getClass()
    {
        return 2;
    }
}