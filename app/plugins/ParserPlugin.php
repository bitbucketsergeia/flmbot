<?php

namespace Plugins;

use \Phalcon\Di;
use CBR\CurrencyDaily;
use Models\OosCbr;
use Cmfcmf\OpenWeatherMap;
use Cmfcmf\OpenWeatherMap\Exception as OWMException;
use Yandex\Translate\Translator;
use Yandex\Translate\Exception;

class ParserPlugin
{
    private $di;

    private $logger;

    public function __construct(Di $di)
    {
        $this->di = $di;
        $this->logger = $di->get('fileLogger');
    }

    /**
     * Cbr
     *
     * @return array
     */
    public function parseCbr()
    {
        try {
            $dt = new \DateTime();

            $elements = OosCbr::find(['date_created' => $dt->format('Y-m-d')]);

            if (count($elements) > 0) {
                return $elements;
            }

            $handler = new CurrencyDaily();
            $result = $handler
                ->setDate($dt->format("d/m/Y"))
                ->request()
                ->getResult();

            $this->logger->debug('parsing CBR:' . print_r($result, true));

            if (!empty($result)) {

                $date = $handler->getResultDate('Y-m-d');

                $elements = OosCbr::find(['date_created' => $date]);

                if (count($elements) > 0) {
                    return $elements;
                }

                foreach($result as $res) {
                    $cbr = new OosCbr();
                    $cbr->num_code = $res['NumCode'];
                    $cbr->char_code = $res['CharCode'];
                    $cbr->nominal = $res['Nominal'];

                    $cbr->title_ru = $res['Nominal'] . ' ' . $res['Name'];

                    $cbr->title_de = $this->getTranslate([
                        'text' => $res['Nominal'] . ' ' . $res['Name'],
                        'napr' => 'ru-de'
                    ]);
                    $cbr->title_en = $this->getTranslate([
                        'text' => $res['Nominal'] . ' ' . $res['Name'],
                        'napr' => 'ru-en'
                    ]);

                    $cbr->title_es = $this->getTranslate([
                        'text' => $res['Nominal'] . ' ' . $res['Name'],
                        'napr' => 'ru-es'
                    ]);

                    $cbr->title_fr = $this->getTranslate([
                        'text' => $res['Nominal'] . ' ' . $res['Name'],
                        'napr' => 'ru-fr'
                    ]);

                    $cbr->title_it = $this->getTranslate([
                        'text' => $res['Nominal'] . ' ' . $res['Name'],
                        'napr' => 'ru-it'
                    ]);

                    $cbr->value = $res['Value'];
                    $cbr->date_created = $date . ' 00:00:00';
                    $cbr->date_download = $dt->format("Y-m-d H:i:s");
                    $cbr->add();
                    unset($cbr);
                }

                $elements = OosCbr::find(['date_created' => $date]);

                if (count($elements) > 0) {
                    return $elements;
                }
            }
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
            return [];
        }

        return [];
    }

    /**
     * OpenweatherMap
     *
     * @param array $params
     * @return array|OpenWeatherMap\WeatherForecast
     */
    public function parseWeather(array $params)
    {
        if (empty($params['search'])) {
            return [];
        }

        $weather = [];
        $lang = $params['lang'] ?? 'en';
        $units = 'metric';
        $apiKey = '0679b5c72d846d621c5b7f5910d45367';
        $owm = new OpenWeatherMap($apiKey);

        try {
            $weather = $owm->getWeatherForecast($params['search'], $units, $lang, $apiKey, 5);
            $this->logger->debug('parsing OpenWeatherMap:' . print_r($weather, true));
        } catch (OWMException $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
            return $weather;
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
            return $weather;
        }

        return $weather;
    }

    /**
     * Yandex geo
     *
     * @param array $params
     * @return array
     */
    public function getGeoCoords(array $params)
    {
        $this->logger->debug('geo:' . print_r($params, true) . '');

        if (empty($params['search']) && empty($params['geo'])) {
            return [];
        }

        try {
            $api = new \Yandex\Geo\Api();

            if (!empty($params['search'])) {
                $api->setQuery($params['search']);
                $api
                    ->setLimit(1)// кол-во результатов
                    ->setLang(\Yandex\Geo\Api::LANG_RU)// локаль ответа
                    ->load();

                $response = $api->getResponse();
                $collection = $response->getList();

                if (!empty($collection)) {

                    $this->logger->debug('geo:' . print_r($collection, true) . '');

                    $item = $collection[0];

                    $geo = [
                        'lat' => $item->getLatitude(),
                        'lon' => $item->getLongitude(),
                        'address' => $item->getData()['Address']
                    ];

                    $this->logger->debug('get geo coords:' . print_r($geo, true));
                    return $geo;
                }
            } else {
                $api->setPoint($params['geo']['lon'], $params['geo']['lat']);
                $api
                    ->setLimit(1)// кол-во результатов
                    ->setLang(\Yandex\Geo\Api::LANG_RU)// локаль ответа
                    ->load();

                $response = $api->getResponse();
                $collection = $response->getList();

                if (!empty($collection)) {
                    $item = $collection[0];
                    $geo = [
                        'lat' => $item->getLatitude(),
                        'lon' => $item->getLongitude(),
                        'address' => $item->getData()['Address']
                    ];

                    $this->logger->debug('get geo coords:' . print_r($geo, true));
                    return $geo;
                }
            }

        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
            return [];
        }

        return [];
    }

    /**
     * Yandex translate
     *
     * @param array $params
     * @return array|string
     */
    public function getTranslate(array $params)
    {
        if (empty($params['text']) || empty($params['napr'])) {
            return [];
        }

        try {
            $this->logger->debug('yandex translate params:' . print_r($params, true));
            $translator = new Translator('trnsl.1.1.20170328T211911Z.16faf5996fb113eb.a78c2f7cdc416f7ef834d96b3f2bb13fde03da59');
            $translation = $translator->translate($params['text'], $params['napr']);
            $list = $translation->getResult();
            $this->logger->debug('yandex translate:' . print_r($list, true));
            return !empty($list) ? $list[0] : '';
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
            return '';
        }
        return '';
    }
}