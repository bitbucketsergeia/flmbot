<?php

namespace Plugins;

use Phalcon\Http\Client\Request;
use \Phalcon\Di;
use Plugins\NaiveHmacSignerPlugin;

class HiveTaxiPlugin extends TaxiPlugin
{
    const URL = 'https://club-omsk.hivelogin.ru:444';
    const API_KEY = '5e33e97b530fdeca19483bfaca79614f';
    const TARIF_ECONOM = 1;
    const TARIF_COMFORT = 2;
    const TARIF_BIZ = 3;
    const TARIF_MINI = 4;
    const COMISSION = 25;

    private $provider;
    private $id;
    private $accessKey;

    public function __construct(Di $di, $accessKey = '', $id = '')
    {
        parent::__construct($di);
        $this->provider = Request::getProvider();
        $this->provider->setBaseUri($this->getPath($accessKey));
        $this->provider->header->set('Content-Type', 'application/json; charset=utf-8');
        $this->provider->header->set('Hive-Profile', self::API_KEY);
        $this->id = $id;
        $this->accessKey = $accessKey;
    }

    //https://github.com/HIVETAXI/client-mobile-api/wiki/HMAC-%D0%B0%D0%B2%D1%82%D0%BE%D1%80%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F#%D0%9F%D1%80%D0%B8%D0%BC%D0%B5%D1%80-%D0%BD%D0%B0-php
    /**
     * @param $method
     * @param $path
     */
    private function setAuthHeaders($method, $path)
    {
        $signer = new NaiveHmacSignerPlugin($this->id, $this->accessKey);
        $this->logger->debug($this->id . '   ' . $this->accessKey);
        $signature = $signer->newSignature($method, $path);
        $this->logger->debug(print_r($signature, true));
        $this->provider->header->set('Authentication', $signature['Authentication']);
        $this->provider->header->set('Date', $signature['Date']);
    }

    public function setKeys($accessKey = '', $id = '')
    {
        $this->accessKey = $accessKey;
        $this->id = $id;
    }

    /**
     * @param $lat
     * @param $long
     */
    private function setGeoHeaders($lat, $long)
    {
        $this->provider->header->set('X-Hive-GPS-Position', $lat . ' ' . $long);
    }

    /**
     * @return string
     */
    private function getPath($accessKey)
    {
        return self::URL;
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function auth(array $params)
    {
        try {
            $pl = [
                'phone' => $params['phone'],
                'confirmationType' => 'sms'
            ];

            $this->logger->debug('auth params:' . print_r($pl, true));
            $response = $this->provider->post('api/client/mobile/1.0/registration/submit',
                json_encode($pl)
            );
            $this->logger->debug($response->body);
            return json_decode($response->body, true);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function reSend(array $params)
    {
        try {
            $pl = [
                'id' => $params['id'],
                'confirmationType' => 'sms'
            ];

            $this->logger->debug('resend params:' . print_r($pl, true));
            $response = $this->provider->get('api/client/mobile/1.0/registration/resubmit',
                $pl
            );
            $this->logger->debug($response->body);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function checkAuth(array $params)
    {
        try {
            $pl = [
                'id' => $params['id'],
                'code' => $params['code']
            ];

            $this->logger->debug('check auth params:' . print_r($pl, true));
            $response = $this->provider->get('api/client/mobile/1.0/registration/confirm',
                $pl
            );
            $this->logger->debug($response->body);
            return json_decode($response->body, true);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function create(array $params)
    {
        try {

            $pl = [
                'time' => (new \DateTime($params['bookingTime']))->format("Y-m-d") . 'T' . (new \DateTime($params['bookingTime']))->format("H:i:s"),
                'phone' => $params['phone'],
                'tariff' => $params['tariff']['name'],
                'options' => $params['tariff']['options'],
                'comment' => $params['comment'],
                "route" => [
                    [
                        "address" => [
                            "name" => $params['in']['address'],
                            'components' => [],
                            "position" => [
                                "lat" => $params['in']['lat'],
                                "lon" => $params['in']['lon']
                            ]
                        ]
                    ],
                    [
                        "address" => [
                            "name" => $params['out']['address'],
                            'components' => [],
                            "position" => [
                                "lat" => $params['out']['lat'],
                                "lon" => $params['out']['lon']
                            ]
                        ]
                    ]
                ]
            ];

            $this->logger->debug('order params:' . print_r($pl, true));
            $this->setAuthHeaders('POST', '/api/client/mobile/3.0/orders');
            $response = $this->provider->post('api/client/mobile/3.0/orders', json_encode($pl));
            $this->logger->debug(print_r($response->header, true));
            $this->logger->debug(print_r($response->body, true));
            return json_decode($response->body, true);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function cancel(array $params)
    {
        try {
            $this->logger->debug('cancel params:' . print_r($params, true));

            $this->setAuthHeaders('DELETE', '/api/client/mobile/1.0/orders/' . $params['orderId']);

            $response = $this->provider->delete('api/client/mobile/1.0/orders/' . $params['orderId'], [

            ]);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function info(array $params)
    {
        try {
            $this->logger->debug('info params:' . print_r($params, true));
            $this->setAuthHeaders('GET', '/api/client/mobile/2.0/orders/' . $params['orderId']);
            $response = $this->provider->get('api/client/mobile/2.0/orders/' . $params['orderId'], [

            ]);
            $this->logger->debug('info: '. print_r($response->body, true));
            return json_decode($response->body, true);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $param
     * @return array|string
     */
    public function update(array $param)
    {
        /*try {
            $this->logger->debug('update params:' . print_r($param, true));
            $params = [];

            $params['orderId'] = $param['orderId'];

            if (!empty($param['bookingTime'])) {
                $params['bookingTime'] = (new \DateTime($param['bookingTime']))->format("Y-m-d H:i:s");
            }

            if (!empty($param['comment'])) {
                $params['comment'] = (new \DateTime($param['comment']))->format("Y-m-d H:i:s");
            }

            $response = $this->provider->post('order/update', $params);

            return $response->body;
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }*/
        return [];
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function price(array $params)
    {
        try {
            $pl = [
                'tariff' => $params['tariff']['name'],
                'options' => $params['tariff']['options'],
                "route" => [
                    [
                        "lat" => $params['in']['lat'],
                        "lon" => $params['in']['lon']
                    ],
                    [
                        "lat" => $params['out']['lat'],
                        "lon" => $params['out']['lon']
                    ]
                ],
                'time' => (new \DateTime($params['bookingTime']))->format("Y-m-d") . 'T' . (new \DateTime($params['bookingTime']))->format("H:i:s")
            ];

            $this->logger->debug('price params:' . print_r($pl, true));
            $response = $this->provider->post('api/client/mobile/1.0/estimate', json_encode($pl));
            $this->logger->debug(print_r(json_decode($response->body, true), true));
            return json_decode($response->body, true);

        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function tariffs(array $params)
    {
        try {
            $this->logger->debug('tarifs params:' . print_r($params, true));

            $this->setGeoHeaders($params['in']['lat'], $params['in']['lon']);

            $response = $this->provider->get(
                'api/client/mobile/2.0/tariffs',
                [

                ]
            );

            return json_decode($response->body, true);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }


    /**
     * @param array $params
     * @return array|string
     */
    public function geo(array $params)
    {
        try {
            $this->logger->debug('tarifs params:' . print_r($params, true));

            //$this->setGeoHeaders($params['in']['lat'], $params['in']['lon']);

            $response = $this->provider->get(
                'api/client/mobile/2.0/address/nearest',
                [

                ]
            );
            $this->logger->debug($response->body);
            $els = json_decode($response->body, true);

            return !empty($els) ? $els[0] : [];
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function search(array $params)
    {
        try {
            $this->logger->debug('tarifs params:' . print_r($params, true));

            //$this->setGeoHeaders($params['in']['lat'], $params['in']['lon']);

            $response = $this->provider->get(
                '/api/client/mobile/1.0/address/geocoding',
                [
                    'query' => $params['query']
                ]
            );
            $this->logger->debug($response->body);
            $els = json_decode($response->body, true);

            return !empty($els) ? $els[0] : [];
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param $region
     * @param $id
     * @return array
     */
    public function getTarif($region, $id)
    {
        $tariffs = $this->tariffs([]);

        $this->logger->debug(print_r($tariffs, true));

        $tariffStr = 'Крым-Про100 телеграмм';
        $tariff = [];

        if ($tariffs) {
            foreach($tariffs['tariffs'] as $t) {
                if ($t['name'] == $tariffStr) {
                    $tariff['name'] = $t['name'];
                    $tariff['options'] = [];
                }
            }
        }

        return $tariff;
    }


}