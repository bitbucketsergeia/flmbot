<?php

namespace Plugins;

use \Phalcon\Di;

class TaxiPlugin
{
    protected $di;

    protected $logger;

    public function __construct(Di $di, $accessKey = '', $id = '')
    {
        $this->di = $di;
        $this->logger = $di->get('fileLogger');
    }
}