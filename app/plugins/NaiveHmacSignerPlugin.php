<?php

namespace Plugins;

class NaiveHmacSignerPlugin {

    private $identity;

    private $secret;

    public function __construct($id, $key) {
        $this->identity = $id;
        $this->secret = base64_decode($key);
    }

    private function hmac($algorithm, $secret, $data) {
        return hex2bin(hash_hmac($algorithm, $data, $secret));
    }

    public function newSignature($requestMethod, $requestPath) {

        $date = date(DATE_RFC1123);
        $nonce = time();

        $data = $requestMethod . $requestPath . $date . $nonce;

        $digest = base64_encode($this->hmac("sha256", $this->secret, $data));

        return array(
            "Authentication" => "hmac " . $this->identity . ":" . $nonce . ":" . $digest,
            "Date" => $date
        );
    }
}