<?php

namespace Plugins;

class CliRequestValidatorPlugin
{
    private $errors = [];
    private $console;

    /**
     * @param $event
     * @param \Phalcon\Cli\Console $console
     */
    public function beforeHandleTask($event, \Phalcon\Cli\Console $console)
    {
        $this->console = $console;

        try {
            $dispatcher = $this->console->getDI()->getShared('dispatcher');
            $task = $dispatcher->getTaskName();
            $params = $dispatcher->getParams();
            $serviceConfig = $this->console->getDI()->getConfig()->get('oosexchanger');
            $cronParams = $serviceConfig->get('cli');
            $clearParams = $this->clearParams($params, array_keys((array)$cronParams['defaultParams']));

            if ($task == 'update_data_from_oos') {
               
            }

            $dispatcher->setParams($clearParams);
        } catch (\Exception $e) {
            if (!empty($this->errors)) {
                foreach ($this->errors as $key => $message) {
                    echo $message . PHP_EOL;
                }
                die();
            }

        }
    }

    /**
     * @param $messages
     * @throws \Exception
     */
    private function parseMessages($messages)
    {
        if (count($messages) == 0) {
            return;
        }

        foreach ($messages as $msg) {
            $this->errors[] = $msg->getMessage();
        }

        throw new \Exception('Errors found');
    }

    /**
     * Конвертация входных параметров
     * @param array $params
     * @param array $allowedParams
     * @return array
     */
    private function clearParams(array $params, array $allowedParams)
    {
        $clearParams = [];

        if (!empty($params)) {
            foreach ($params as $keyParam => $keyValue) {
                $arr = [];

                if (preg_match('|--(.+)=(.+)|sei', $keyValue, $arr)
                    && count($arr) >= 3
                    && in_array($arr[1], $allowedParams)
                ) {
                    if ($arr[1] == 'date') {
                        try {
                            $clearParams[$arr[1]] = new \DateTime($arr[2]);
                        } catch (\Exception $e) {
                            $clearParams[$arr[1]] = new \DateTime();
                        }
                    } else {
                        $clearParams[$arr[1]] = $arr[2];
                    }
                }
            }
        }

        return $clearParams;
    }

    /**
     * Check updateDataFromOosTask Params
     * @param $clearParams
     * @throws \Exception
     */
    private function updateDataFromOosTaskValidate($clearParams)
    {
        $messages = $this->console->getDI()->get('updateDataFromOosTaskValidator')->validate($clearParams);
        $this->parseMessages($messages);
    }
}