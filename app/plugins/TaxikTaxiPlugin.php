<?php

namespace Plugins;

use Phalcon\Http\Client\Request;
use \Phalcon\Di;

class TaxikTaxiPlugin extends TaxiPlugin
{
    const PATH = 'https://taxistock.ru/taxik/api/v2/';
    const API_KEY = '302f2bd86139a5263dba0f5d4701911f';
    const TARIF_ECONOM = 1;
    const TARIF_COMFORT = 2;
    const TARIF_BIZ = 3;
    const TARIF_MINI = 4;
    const COMISSION = 25;

    private $provider;

    public function __construct(Di $di, $accessKey = '')
    {
        parent::__construct($di);
        $this->provider = Request::getProvider();
        $this->provider->setBaseUri($this->getPath());
    }

    /**
     * @return string
     */
    private function getPath()
    {
        return self::PATH . '?apikey=' . self::API_KEY;
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function create(array $params)
    {
        try {
            $reqP = [
                'bookingTime' => (new \DateTime($params['bookingTime']))->format("Y-m-d H:i:s"),
                'tariff' => $params['tariff'],
                "client" => ["phone" => $params['phone']],
                "routePoints" => [
                    [
                        "address" => $params['in']['address'],
                        "point" => [
                            "latitude" => $params['in']['lat'],
                            "longitude" => $params['in']['lon']
                        ]
                    ],
                    [
                        "address" => $params['out']['address'],
                        "point" => [
                            "latitude" => $params['out']['lat'],
                            "longitude" => $params['out']['lon']
                        ]
                    ]
                ],
                "commission" => self::COMISSION,
                "driversView" => true,
                "payment" => [
                    "fixedPrice" => $params['price'],
                    "cashless" => false
                ],
                "comment" => $params['comment']
            ];

            $this->logger->debug('order create params:' . print_r($reqP, true));
            $response = $this->provider->post('order/create', json_encode($reqP));
            return json_decode($response->body, true);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function cancel(array $params)
    {
        try {
            $this->logger->debug('cancel params:' . print_r($params, true));
            $response = $this->provider->post('order/cancel', json_encode([
                'orderId' => $params['orderId']
            ]));

            return json_decode($response->body, true);
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function info(array $params)
    {
        try {
            $this->logger->debug('info params:' . print_r($params, true));
            $response = $this->provider->post('order/info', [
                'orderId' => $params['orderId']
            ]);

            $this->logger->debug('info: '. print_r($response->body, true));
            return $response->body;
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $param
     * @return array|string
     */
    public function update(array $param)
    {
        try {
            $this->logger->debug('update params:' . print_r($param, true));
            $params = [];

            $params['orderId'] = $param['orderId'];

            if (!empty($param['bookingTime'])) {
                $params['bookingTime'] = (new \DateTime($param['bookingTime']))->format("Y-m-d H:i:s");
            }

            if (!empty($param['comment'])) {
                $params['comment'] = (new \DateTime($param['comment']))->format("Y-m-d H:i:s");
            }

            $response = $this->provider->post('order/update', $params);

            return $response->body;
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function price(array $params)
    {
        try {
            $reqP = [
                'bookingTime' => (new \DateTime($params['bookingTime']))->format("Y-m-d H:i:s"),
                'tariff' => $params['tariff'],
                "routePoints" => [
                    [
                        "point" => [
                            "latitude" => $params['in']['lat'],
                            "longitude" => $params['in']['lon']
                        ]
                    ],
                    [
                        "point" => [
                            "latitude" => $params['out']['lat'],
                            "longitude" => $params['out']['lon']
                        ]
                    ]
                ],
            ];

            $this->logger->debug('price params:' . print_r($reqP, true));
            $response = $this->provider->post('order/price', json_encode($reqP));
            return json_decode($response->body, true);

        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
        return [];
    }

    /**
     * @param $region
     * @param $id
     * @return int
     */
    public function getTarif($region, $id)
    {
        $tariff = 1;

        switch ($id) {
            case 507:
                $tariff = 1;
                break;
            case 508:
                $tariff = 2;
                break;
            case 509:
                $tariff = 3;
                break;
        }

        return $tariff;
    }
}