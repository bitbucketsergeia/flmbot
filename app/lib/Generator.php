<?php

/**
 * Created by PhpStorm.
 * User: kartashev
 * Date: 21.09.16
 * Time: 13:21
 */
namespace Lib;

use Phalcon\Security\Random;

class Generator
{
    public static function generateUniqueName($filename)
    {
        $hashedName = md5($filename) . "-";
        $rnd = new Random();
        return strtolower(substr_replace($rnd->uuid(), $hashedName, 9, 0));
    }
}