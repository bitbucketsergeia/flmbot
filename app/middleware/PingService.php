<?php

namespace Middleware;

use \Phalcon\Mvc\Micro\MiddlewareInterface;
use \Phalcon\Mvc\Micro;
use Phalcon\Db\Adapter\Pdo\Postgresql;

class PingService extends ServiceBase implements MiddlewareInterface
{

    public function call(Micro $app)
    {
        $catchedException = false;
        try {
            /** @var Postgresql $connection */
            $connection = $app->di->get('db');
            $connection->execute('SELECT * FROM oos_log LIMIT 1');
        } catch (\Exception $e) {
            $catchedException = true;
        }
        if ($catchedException) {
            $app->response->setContent('db_error')->setContentType('text/text');
        } else {
            $app->response->setContent('pong')->setContentType('text/text');
        }
    }
}