<?php

namespace Middleware;

use Models\OosCities;
use Models\OosLang;
use Models\OosOrders;
use Models\OosUsers;
use Phalcon\Logger;
use \Phalcon\Mvc\Micro\MiddlewareInterface;
use \Phalcon\Mvc\Micro;
use Models\OosComands;
use Models\OosComandsLog;
use Plugins\ParserPlugin;
use Plugins\TaxikTaxiPlugin;

class TaxikService extends ServiceBase implements MiddlewareInterface
{
    // $this->logger->debug('Request params: !!!!!');
    private $logger;

    //lang
    private $lang;

    //regions
    private $regions = [];

    //langs
    private $langs = [];

    //region
    private $currentRegion;

    /**
     * @var ParserPlugin
     */
    private $parserPlugin;

    /**
     * @var
     */
    private $api;

    public function call(Micro $app)
    {
        $this->logger = $app->di->get('fileLogger');
        $this->parserPlugin = new ParserPlugin($app->di);

        if (!empty($app->invalid)) {
            return false;
        }

        //@flmtaxiBot

        $config = $app->getDI()->getConfig()->get('oosexchanger');
        $langHref = new OosLang();
        $citiesHref = new OosCities();

        try {
            $this->langs = $langHref->find([]);
            $this->regions = $citiesHref->find([]);
            $bot = new \TelegramBot\Api\Client($config['bot_token']);
            $botCurrent = new \TelegramBot\Api\Botan($config['ya_metrika']);
            $keyboards = new \TelegramBot\Api\Types\ReplyKeyboardMarkup([], null, null);
            $logger = $this->logger;
            $requestParams = json_decode(file_get_contents('php://input'), true);

            if (!empty($requestParams) && isset($requestParams['orderInfo']['orderId'])) {

                $order = $this->getOrder($requestParams['orderInfo']['orderId']);

                if (!empty($order)) {
                    $chat = OosUsers::findFirst(
                        [
                            "id = '" . $order->user_id . "' AND status<>'" . OosUsers::STATUS_DELETED . "'"
                        ]
                    );

                    if (!empty($chat)) {
                        $this->lang = $chat->lang;

                        if ($requestParams['action'] == 'update.status') {
                            if (!empty($requestParams['orderInfo'])) {
                                if ($requestParams['orderInfo']['status'] == 'complete') {
                                    $this->paintViewById($order, $requestParams, $chat, $bot, 650, null, true);
                                    $order->setStatus(OosOrders::STATUS_FINISH);
                                    $order->save();
                                } elseif ($requestParams['orderInfo']['status'] == 'cancelled') {
                                    $this->paintViewById($order, $requestParams, $chat, $bot, 660, null, true);
                                    $order->setStatus(OosOrders::STATUS_CANCEL);
                                    $order->save();
                                } elseif ($requestParams['orderInfo']['status'] == 'waiting') {
                                    $this->paintViewById($order, $requestParams, $chat, $bot, 601, null, true);
                                }
                            }
                        } elseif ($requestParams['action'] == 'update.driver') {
                            $this->paintViewById($order, $requestParams, $chat, $bot, 601, null, true);
                        }
                    }
                }
            }

        } catch (\TelegramBot\Api\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }

        die(json_encode(['status' => 2]));
    }

    /**
     * @param $order
     * @param $params
     * @param $chat
     * @param $bot
     * @param $id
     * @param null $comandC
     * @param bool $isScreen
     */
    private function paintViewById($order, $params, $chat, $bot, $id, $comandC = null, $isScreen = false)
    {
        $comandC = $comandC ?? OosComands::getComandById($id);
        $next = $isScreen ? $comandC->parent : ($comandC->next ?? $comandC->id);
        $comands = OosComands::getComandsByParent($next);
        $keyboardsArr = [];

        if ($comands) {
            foreach($comands as $comand) {
                if ($comand->request_contact == 1) {
                    $keyboardsArr[] = [['text' => $comand->{'title_' . $this->lang}, 'request_contact' => true]];
                } elseif ($comand->request_location == 1) {
                    $keyboardsArr[] = [['text' => $comand->{'title_' . $this->lang}, 'request_location' => true]];
                } else {
                    $keyboardsArr[] = [['text' => stripcslashes($comand->{'title_' . $this->lang})]];
                }
            }
        }

        if (!empty($comandC->{'description_' . $this->lang})) {
            $description = $comandC->{'description_' . $this->lang};

            switch ($description) {
                case '{check}':
                    $description = $this->viewStatus($order, $chat, $params);

                    if (empty($description)) {
                        die(json_encode(['status' => 3]));
                    }
                    break;
                case '{finish}':
                    $description = $this->viewFinish($order, $chat, $params);

                    if (empty($description)) {
                        die(json_encode(['status' => 3]));
                    }
                    break;
            }

            $keyboards = new \TelegramBot\Api\Types\ReplyKeyboardMarkup($keyboardsArr, null, true);

            if (is_array($description)) {
                $cArray = count($description);

                for($i = 0; $i < $cArray; $i++) {
                    $bot->sendMessage($chat->chat_id, $description[$i], 'HTML', null, null, $keyboards);
                }

            } else {
                $countChars = mb_strlen($description, "UTF-8");

                if ($countChars > 4096) {
                    $parts = $this->mb_str_split($description, 4096);
                    $c = count($parts);

                    if ($c > 0) {
                        for($i = 0; $i < $c; $i++) {
                            $bot->sendMessage($chat->chat_id, mb_substr($parts[$i], 0, 4095, 'UTF-8'), 'HTML', null, null, $keyboards);
                        }
                    }

                } else {
                    $bot->sendMessage($chat->chat_id, mb_substr($description, 0, 4095, 'UTF-8'), 'HTML', null, null, $keyboards);
                }
            }
        }
        $this->saveComandToLog($chat->chat_id, $comandC, $comandC->is_read == 1 ? ['message' => ''] : []);
    }

    /**
     * @param $order
     * @param $chat
     * @param $params
     * @return string
     */
    private function viewStatus($order, $chat, $params)
    {
        $description = '';

        if (!empty($params['orderInfo']['driver'])) {
            $description = $this->getPriceData($params);
        }

        return $description;
    }

    /**
     * @param $order
     * @param $chat
     * @param $params
     * @return string
     */
    private function viewFinish($order, $chat, $params)
    {
        $description = $this->getFinishData($order, $params);
        return $description;
    }

    /**
     * @return string
     */
    private function getFinishData($order, $params)
    {
        $description = '';
        $params = $order->getParams();

        switch ($this->lang) {
            case 'ru':
                $description = 'Заказ закрыт на сумму ' . $params['price'] . ' ₽. Спасибо за то, что вы воспользовались сервисом Myconcierge.ru';
                break;
            case 'en':
                $description = 'Заказ закрыт на сумму ' . $params['price'] . ' ₽. Спасибо за то, что вы воспользовались сервисом Myconcierge.ru';
                break;
            case 'es':
                $description = 'Заказ закрыт на сумму ' . $params['price'] . ' ₽. Спасибо за то, что вы воспользовались сервисом Myconcierge.ru';
                break;
            case 'it':
                $description = 'Заказ закрыт на сумму ' . $params['price'] . ' ₽. Спасибо за то, что вы воспользовались сервисом Myconcierge.ru';
                break;
            case 'de':
                $description = 'Заказ закрыт на сумму ' . $params['price'] . ' ₽. Спасибо за то, что вы воспользовались сервисом Myconcierge.ru';
                break;
            case 'fr':
                $description = 'Заказ закрыт на сумму ' . $params['price'] . ' ₽. Спасибо за то, что вы воспользовались сервисом Myconcierge.ru';
                break;
        }

        return $description;
    }

    /**
     * @return string
     */
    private function getPriceData($params)
    {
        $description = '';

        $car = $params['orderInfo']['driver']['car']['color'] . ' ' . $params['orderInfo']['driver']['car']['name'];

        if ($this->lang != 'ru') {
            $paramsEr = [
                'text' => $params['orderInfo']['driver']['car']['color'] . ' ' . $params['orderInfo']['driver']['car']['name'],
                'napr' => 'ru-' . $this->lang
            ];
            $car = $this->parserPlugin->getTranslate($paramsEr);
        }

        switch ($this->lang) {
            case 'ru':
                switch ($params['orderInfo']['status']) {
                    case 'assigned':
                        $description .= '<i>Вам назначен ' . $car . ' ' . $params['orderInfo']['driver']['car']['number'] . '</i>' . PHP_EOL;
                        $description .= '<i>Телефон водителя: ' . $params['orderInfo']['driver']['phone'] . '</i>' . PHP_EOL;
                        $description .= '<b>Машина в пути. Я сообщу, как подъедет</b>' . PHP_EOL;
                        break;
                    case 'waiting':
                        $description .= '<i>Вас ожидает ' . $car . ' ' . $params['orderInfo']['driver']['car']['number'] . '</i>' . PHP_EOL;
                        $description .= '<i>Телефон водителя: ' . $params['orderInfo']['driver']['phone'] . '</i>' . PHP_EOL;
                        $description .= '<b>Пожалуйста, выходите</b>' . PHP_EOL;
                        break;
                }
                break;
            case 'en':
                switch ($params['orderInfo']['status']) {
                    case 'assigned':
                        $description .= '<i>A ' . $car . ' ' . $params['orderInfo']['driver']['car']['number'] . 'has been assigned to you.</i>' . PHP_EOL;
                        $description .= "<i>Driver's phone number: " . $params['orderInfo']['driver']['phone'] . '</i>' . PHP_EOL;
                        $description .= '<b>Your car is on its way. I will notify you as soon as it arrives.</b>' . PHP_EOL;
                        break;
                    case 'waiting':
                        $description .= '<i>А ' . $car . ' ' . $params['orderInfo']['driver']['car']['number'] . 'is waiting for you.</i>' . PHP_EOL;
                        $description .= "<i>Driver's phone number: " . $params['orderInfo']['driver']['phone'] . '</i>' . PHP_EOL;
                        $description .= '<b>Please come out</b>' . PHP_EOL;
                        break;
                }
                break;
            case 'es':

                switch ($params['orderInfo']['status']) {
                    case 'assigned':
                        $description .= '<i>Se le ha asignado un coche ' . $car . ' ' . $params['orderInfo']['driver']['car']['number'] . '</i>' . PHP_EOL;
                        $description .= '<i>El teléfono del chofer: ' . $params['orderInfo']['driver']['phone'] . '</i>' . PHP_EOL;
                        $description .= '<b>El coche ya está en camino. Le avisaré cuando llegue</b>' . PHP_EOL;
                        break;
                    case 'waiting':
                        $description .= '<i>El coche ' . $car . ' ' . $params['orderInfo']['driver']['car']['number'] . 'ya llegó y espera por Usted</i>' . PHP_EOL;
                        $description .= '<i>El teléfono del chofer: ' . $params['orderInfo']['driver']['phone'] . '</i>' . PHP_EOL;
                        $description .= '<b>Por favor, ya puede salir</b>' . PHP_EOL;
                        break;
                }
                break;
            case 'it':
                switch ($params['orderInfo']['status']) {
                    case 'assigned':
                        $description .= '<i>La macchina assegnata è una ' . $car . ' ' . $params['orderInfo']['driver']['car']['number'] . '</i>' . PHP_EOL;
                        $description .= '<i>Numero di telefono del conducente: ' . $params['orderInfo']['driver']['phone'] . '</i>' . PHP_EOL;
                        $description .= '<b>La macchina è in viaggio. Ti informerò non appena arriverà.</b>' . PHP_EOL;
                        break;
                    case 'waiting':
                        $description .= '<i>Ti aspetta una ' . $car . ' ' . $params['orderInfo']['driver']['car']['number'] . '</i>' . PHP_EOL;
                        $description .= '<i>Numero di telefono del conducente: ' . $params['orderInfo']['driver']['phone'] . '</i>' . PHP_EOL;
                        $description .= '<b>Per favore, vieni fuori</b>' . PHP_EOL;
                        break;
                }
                break;
            case 'de':

                switch ($params['orderInfo']['status']) {
                    case 'assigned':
                        $description .= '<i>Für Sie wurde ein ' . $car . ' ' . $params['orderInfo']['driver']['car']['number'] . ' bestimmt.</i>' . PHP_EOL;
                        $description .= '<i>Telefonnummer des Fahrers: ' . $params['orderInfo']['driver']['phone'] . '</i>' . PHP_EOL;
                        $description .= '<b>Ihr Auto ist unterwegs. Sobald das Auto ankommt, werden Sie benachrichtigt.</b>' . PHP_EOL;
                        break;
                    case 'waiting':
                        $description .= '<i>Der ' . $car . ' ' . $params['orderInfo']['driver']['car']['number'] . ' wartet auf Sie.</i>' . PHP_EOL;
                        $description .= '<i>Telefonnummer des Fahrers: ' . $params['orderInfo']['driver']['phone'] . '</i>' . PHP_EOL;
                        $description .= '<b>Bitte komm heraus</b>' . PHP_EOL;
                        break;
                }
                break;
            case 'fr':

                switch ($params['orderInfo']['status']) {
                    case 'assigned':
                        $description .= '<i>Le ' . $car . ' ' . $params['orderInfo']['driver']['car']['number'] . ' est commandé pour Vous.</i>' . PHP_EOL;
                        $description .= '<i>Téléphone de chauffeur: ' . $params['orderInfo']['driver']['phone'] . '</i>' . PHP_EOL;
                        $description .= '<b>La vouture est en route. Je Vous informe, quand elle va arriver</b>' . PHP_EOL;
                        break;
                    case 'waiting':
                        $description .= '<i>Le ' . $car . ' ' . $params['orderInfo']['driver']['car']['number'] . 'noir Vous attend.</i>' . PHP_EOL;
                        $description .= '<i>Téléphone de chauffeur: ' . $params['orderInfo']['driver']['phone'] . '</i>' . PHP_EOL;
                        $description .= "<b>Voulez-Vous sortir, s'il vous plait</b>" . PHP_EOL;
                        break;
                }
                break;
        }

        return $description;
    }

    /**
     * @param $string
     * @param int $string_length
     * @return array
     */
    private function mb_str_split($string, $string_length = 1)
    {
        if (mb_strlen($string) > $string_length || !$string_length) {
            do {
                $c = mb_strlen($string);
                $parts[] = mb_substr($string, 0, $string_length);
                $string = mb_substr($string, $string_length);
            } while (!empty($string));
        } else {
            $parts = array($string);
        }
        return $parts;
    }

    /**
     * @param $orderId
     * @return \Phalcon\Mvc\Model
     */
    private function getOrder($orderId)
    {
        $order = OosOrders::findFirst(
            [
                "external_id = '$orderId' AND status <> '" . OosOrders::STATUS_FINISH . "' AND status <> '" . OosOrders::STATUS_CANCEL . "'",
                "order" => "date_created DESC",
            ]
        );

        return $order;
    }

    /**
     * Логи
     *
     * @param $id
     * @param $comand
     * @param array $params
     */
    private function saveComandToLog($id, $comand, $params = [])
    {
        $oosComandsLog = new OosComandsLog();
        $oosComandsLog->comand_id = $comand->id;
        $oosComandsLog->user_id = $id;
        $oosComandsLog->date_created = (new \DateTime())->format("Y-m-d H:i:s");
        $oosComandsLog->setParams($params);
        $oosComandsLog->add();
    }
}