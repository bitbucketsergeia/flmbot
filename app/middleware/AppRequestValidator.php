<?php

namespace Middleware;

use Phalcon\Mvc\Micro\MiddlewareInterface;
use Phalcon\Mvc\Micro;

class AppRequestValidator implements MiddlewareInterface
{
    protected $errors = [];

    protected $app;

    public function call(Micro $app)
    {
        $this->app = $app;

        try {
            $route = str_replace('/', '', $app->getRouter()->getMatchedRoute()->getCompiledPattern());

            $messages = [];
            if ($route != 'ping') {
                //$messages = $app->di->get('requestValidator')->validate($app->request->get());
            }

            $this->parseMessages($messages);

            if ($route == 'ping') {
                // nothing to do
            } elseif ($route == 'get_ftp_data') {
                //$this->checkGetParamsInRequest($route);
            }

        } catch (\Exception $e) {
            if (empty($this->errors)) {
                $this->errors['request'] = $e->getMessage();
            }
            $app->response->setJsonContent(['status' => STATUS_ERR, 'error' => $this->errors]);
            $app->invalid = true;
            return false;
        }
        return true;
    }

    protected function parseMessages($messages)
    {
        if (count($messages) == 0) {
            return;
        }

        foreach ($messages as $msg) {
            $this->errors[] = $msg->getMessage();
        }

        throw new \Exception('Errors found');
    }

    protected function checkGetParamsInRequest($route)
    {
        $messages = $this->app->di->get('getParamsValidator')->validate($this->app->request->getQuery());
        $this->parseMessages($messages);
    }
}