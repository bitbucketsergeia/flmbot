<?php

namespace Middleware;

use Phalcon\Mvc\Micro\MiddlewareInterface;
use Phalcon\Mvc\Micro;
use \TelegramBot\Api\BotApi;

class RequestLogger implements MiddlewareInterface
{
    public function call(Micro $app)
    {
        if ($app->getRouter()->getMatchedRoute()->getCompiledPattern() == '/ping') {
            return;
        }
        try {
            $response = $app->response->getContent();

            /** @var Log $logger */
            $logger = $app->di->get('dbLogger');
            $logger->setIp($app->request->getClientAddress());
            $logger->setDate(date('c'));
            $json = file_get_contents('php://input');

            if(!empty($json)) {
                $params = BotApi::jsonValidate(file_get_contents('php://input'), true);
                $logger->setAction($params['message']['text']);
                $logger->setChatId($params['message']['chat']['id']);
                $logger->setParams($params);
            }

            $logger->setStatus($response['status']);
            $logger->setDetails($response);
            $logger->setType($app->request->get('id'));

            if (!$logger->create()) {
                throw new \Exception('Insert data error');
            }
        } catch (\Exception $e) {
            $fLogger = $app->di->get('fileLogger');
            $fLogger->critical($e->getTraceAsString() . "[db log was not write]");
        }
    }
}