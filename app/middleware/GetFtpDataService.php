<?php

namespace Middleware;

use Models\OosCities;
use Models\OosLang;
use Models\OosOrders;
use Models\OosUsers;
use Phalcon\Logger;
use \Phalcon\Mvc\Micro\MiddlewareInterface;
use \Phalcon\Mvc\Micro;
use Models\OosComands;
use Models\OosComandsLog;
use Plugins\ParserPlugin;
use Plugins\TaxikTaxiPlugin;
use Plugins\HiveTaxiPlugin;
use Plugins\ExdanTaxiPlugin;

class GetFtpDataService extends ServiceBase implements MiddlewareInterface
{
    const MAIN_VIEW = 1;
    const ORDER_VIEW = 2;
    const LANG_VIEW = 101;
    const LANG_VIEW_MAIN = 12001;
    const REGION_VIEW = 151;
    const REGION_VIEW_MAIN = 12501;
    const DEFAULT_LANG = 'ru';
    const REGION_VIEW_KRIM = 201;
    const REGION_VIEW_KRIM_MAIN = 12801;
    const TIMEOUT_ORDER = 18;

    // $this->logger->debug('Request params: !!!!!');
    private $logger;

    /**
     * @var ParserPlugin
     */
    private $parserPlugin;

    //lang
    private $lang;

    //regions
    private $regions = [];

    //langs
    private $langs = [];

    //region
    private $currentRegion;

    /**
     * @var
     */
    private $api;

    /**
     * @var
     */
    private $eradv;

    public function call(Micro $app)
    {
        $this->logger = $app->di->get('fileLogger');

        if (!empty($app->invalid)) {
            return false;
        }

        //@flmtaxiBot

        $this->parserPlugin = new ParserPlugin($app->di);
        $config = $app->getDI()->getConfig()->get('oosexchanger');
        $langHref = new OosLang();
        $citiesHref = new OosCities();
        $this->eradv = new ExdanTaxiPlugin($app->getDI());

        try {
            $this->langs = $langHref->find([]);
            $this->regions = $citiesHref->find([]);

            $bot = new \TelegramBot\Api\Client($config['bot_token']);
            $botCurrent = new \TelegramBot\Api\Botan($config['ya_metrika']);
            $keyboards = new \TelegramBot\Api\Types\ReplyKeyboardMarkup([], null, null);
            $logger = $this->logger;

            $bot->on(function(\TelegramBot\Api\Types\Update $update) use ($logger, $app, $bot, $keyboards, $botCurrent) {

                $message = $update->getMessage();

                $chat = OosUsers::findFirst(
                    [
                        "chat_id = '" . $message->getChat()->getId() . "' AND status<>'" . OosUsers::STATUS_DELETED . "'"
                    ]
                );

                if (!empty($chat)) {
                    $this->lang = $chat->lang ?? self::DEFAULT_LANG;

                    foreach($this->regions as $region) {
                        if ($chat->city_id == $region->id) {
                            $this->currentRegion = $region;

                            if ($this->currentRegion->po == OosCities::TAXIK_SYSTEM) {
                                $this->api = new TaxikTaxiPlugin($app->getDI());
                            }

                            if ($this->currentRegion->po == OosCities::HIVE_SYSTEM) {
                                $this->api = new HiveTaxiPlugin($app->getDI());
                            }

                            if ($this->currentRegion->po == OosCities::XDN_SYSTEM) {
                                $this->api = new ExdanTaxiPlugin($app->getDI());
                            }
                        }
                    }

                } else {
                    $this->lang = self::DEFAULT_LANG;
                }

                $log = OosComandsLog::getComandByUserId($message->getChat()->getId());
                $prev = $log ? OosComands::getComandById($log->comand_id) : null;

                //Костыль для кнопок назад
                if ($prev->id == self::LANG_VIEW) {

                    $comandC = OosComands::getComandByNameAndParent($message->getText(), 100, $this->lang);

                    if (empty($comandC) || $comandC->id != 11000) {
                        $comandC = OosComands::getComandByNameAndParent($message->getText(), $prev ? ($prev->next > 0 ? $prev->next : $prev->id) : 0, $this->lang);
                    }

                } elseif ($prev->id == self::REGION_VIEW_MAIN) {

                    $comandC = OosComands::getComandByNameAndParent($message->getText(), 12500, $this->lang);

                    if (empty($comandC) || $comandC->id != 12520) {
                        $comandC = OosComands::getComandByNameAndParent($message->getText(), $prev ? ($prev->next > 0 ? $prev->next : $prev->id) : 0, $this->lang);
                    }

                } elseif ($prev->id == self::REGION_VIEW) {

                    $comandC = OosComands::getComandByNameAndParent($message->getText(), 150, $this->lang);

                    if (empty($comandC) || $comandC->id != 11001) {
                        $comandC = OosComands::getComandByNameAndParent($message->getText(), $prev ? ($prev->next > 0 ? $prev->next : $prev->id) : 0, $this->lang);
                    }


                } elseif ($prev->id == self::REGION_VIEW_KRIM) {

                    $comandC = OosComands::getComandByNameAndParent($message->getText(), 200, $this->lang);

                    if (empty($comandC) || $comandC->id != 11002) {
                        $comandC = OosComands::getComandByNameAndParent($message->getText(), $prev ? ($prev->next > 0 ? $prev->next : $prev->id) : 0, $this->lang);
                    }

                } else {
                    $comandC = OosComands::getComandByNameAndParent($message->getText(), $prev ? ($prev->next > 0 ? $prev->next : $prev->id) : 0, $this->lang);
                }

                /////////////////////Конец костыля

                if (($comandC && $comandC->id > 0) || $message->getText() == '/start') {

                    if ($message->getText() == '/start') {
                        if (!$chat) {
                            $this->addUser($message);
                            $this->paintViewById($chat, $bot, $message, self::LANG_VIEW_MAIN, null, true);
                            $botCurrent->track($message, 'registration');
                        } else {
                            $this->paintViewById($chat, $bot, $message, self::MAIN_VIEW);
                            $botCurrent->track($message, 'comands');
                        }
                    } else {
                        if ($comandC->is_main == 1) {
                            $this->paintViewById($chat, $bot, $message, self::MAIN_VIEW);
                        } elseif ($comandC->is_prev == 1) {
                            $comN = OosComands::getComandById($comandC->parent);
                            $this->paintViewById($chat, $bot, $message, $comN->parent);
                        } elseif ($comandC->id == 42) {
                            //Изменить язык
                            $this->paintViewById($chat, $bot, $message, self::LANG_VIEW, null, true);
                        } elseif ($comandC->id == 2 || $comandC->id == 520 || $comandC->id == 45) {
                            //Сделать заказ
                            if (empty($chat->city_id)) {
                                $this->paintViewById($chat, $bot, $message, self::REGION_VIEW_MAIN, null, true);
                            } elseif (empty($chat->phone) || ($chat->status == OosUsers::STATUS_ACTIVE)) {
                                $this->paintViewById($chat, $bot, $message, 400, null, true);
                            } else {
                                $order = $this->getLast($chat->id);

                                if (empty($order)) {
                                    $this->createOrder($chat->id);
                                    $this->paintViewById($chat, $bot, $message, null, $comandC);
                                } else {
                                    //Сразу на оформление
                                    if ($order->status == OosOrders::STATUS_WAIT) {
                                        $this->paintViewById($chat, $bot, $message, 630, null, true);
                                    } else {
                                        $this->paintViewById($chat, $bot, $message, null, $comandC);
                                    }
                                }
                            }
                        } elseif ($comandC->id == 43) {
                            //Изменить регион
                            $this->paintViewById($chat, $bot, $message, self::REGION_VIEW, null, true);
                            //Время
                        } elseif ($comandC->id == 513) {
                            $this->saveOrderTime($chat->id, $message->getText(), true);
                            $this->paintViewById($chat, $bot, $message, null, $comandC);
                            //Тариф
                        } elseif ($comandC->id == 507 || $comandC->id == 508 || $comandC->id == 509 || $comandC->id == 4001) {

                            $this->saveTarif($chat->id, $comandC->id, $message->getText());
                            $this->paintViewById($chat, $bot, $message, null, $comandC);

                        } elseif ($comandC->id == 516) {
                            //Оформить заказ
                            $stat = $this->waitOrder($chat->phone, $chat->id, $chat);

                            if ($stat) {
                                $this->paintViewById($chat, $bot, $message, null, $comandC);
                            } else {
                                $this->paintViewById($chat, $bot, $message, self::ORDER_VIEW);
                            }
                        } elseif ($comandC->id == 519 || $comandC->id == 633 || $comandC->id == 602 || $comandC->id == 10000) {
                            //Отменить заказ
                            $stat = $this->cancelOrder($chat->id, $chat);

                            if ($stat) {
                                $this->paintViewById($chat, $bot, $message, null, $comandC);
                            } else {
                                $this->paintViewById($chat, $bot, $message, self::ORDER_VIEW);
                            }
                        } elseif ($comandC->id == 44) {
                            if (!empty($chat)) {
                                $this->cancelOrder($chat->id, $chat);
                                $chat->status = OosUsers::STATUS_DELETED;
                                $chat->save();
                                $this->addUser($message);
                            }
                            $this->paintViewById($chat, $bot, $message, self::LANG_VIEW_MAIN, null, true);
                        } elseif ($comandC->id == 406) {
                            $this->resendCode($chat, $message->getText());
                            $this->paintViewById($chat, $bot, $message, 405, null, true);
                            return true;

                        } else {
                            $this->paintViewById($chat, $bot, $message, null, $comandC);
                        }

                        $botCurrent->track($message, 'comands');
                    }
                } else {
                    //Обработка текстовых сообщений
                    $comandC = OosComands::getComandByNameAndParent('', $prev ? ($prev->next > 0 ? $prev->next : $prev->id) : 0, $this->lang);

                    if ($comandC) {
                        //Координаты места отправления
                        if ($comandC->id == 500 || $comandC->id == 501) {

                            $coords = $this->getCoords($message);

                            if (!empty($coords)) {
                                $this->saveLocation($chat->id, $coords, []);
                            }
                        }

                        //Проверочный код
                        if ($comandC->id == 405) {

                            if ($this->currentRegion->po == OosCities::HIVE_SYSTEM) {
                                $params = $chat->getParams();

                                $responce = $this->api->checkAuth([
                                    'id' => $params['tmps_id'],
                                    'code' => $message->getText()
                                ]);

                                if (!empty($responce['id']) && !empty($responce['key'])) {
                                    $params = $chat->getParams();
                                    $params['out_id'] = $responce['id'];
                                    $params['out_key'] = $responce['key'];
                                    $chat->setParams($params);
                                    $chat->status = OosUsers::STATUS_CHECKED;
                                    $chat->save();
                                } else {
                                    $this->paintErrorView($bot, $message, 1000);
                                    return true;
                                }
                            } elseif ($this->currentRegion->po == OosCities::TAXIK_SYSTEM) {

                                $params = $chat->getParams();

                                $responce = $this->eradv->checkAuth([
                                    'phone' => $chat->phone,
                                    'code' => $message->getText()
                                ]);

                                if (!empty($responce['data']['access_token'])) {
                                    $params = $chat->getParams();
                                    $params['out_key'] = $responce['data']['access_token'];
                                    $chat->setParams($params);
                                    $chat->status = OosUsers::STATUS_CHECKED;
                                    $chat->save();
                                } else {
                                    $this->paintErrorView($bot, $message, 1000);
                                    return true;
                                }
                            } elseif ($this->currentRegion->po == OosCities::XDN_SYSTEM) {

                                $params = $chat->getParams();

                                $responce = $this->api->checkAuth([
                                    'phone' => $chat->phone,
                                    'code' => $message->getText()
                                ]);

                                if (!empty($responce['data']['access_token'])) {
                                    $params = $chat->getParams();
                                    $params['out_key'] = $responce['data']['access_token'];
                                    $chat->setParams($params);
                                    $chat->status = OosUsers::STATUS_CHECKED;
                                    $chat->save();
                                } else {
                                    $this->paintErrorView($bot, $message, 1000);
                                    return true;
                                }
                            }

                            $this->paintViewById($chat, $bot, $message, self::ORDER_VIEW);
                            return true;
                        }
                        //авторизация
                        if ($comandC->id == 400 || $comandC->id == 402) {

                            $contact = $message->getContact();

                            if (!empty($contact)) {
                                $phone = $contact->getPhoneNumber();
                            } else {
                                $phone = $message->getText();
                            }

                            if (!empty($phone)) {

                                if ($this->currentRegion->po == OosCities::TAXIK_SYSTEM) {

                                    $responce = $this->eradv->auth([
                                        'phone' => OosUsers::formatPhone($phone)
                                    ]);

                                    if (!empty($responce['code']) && $responce['code'] == 200) {
                                        $chat->phone = OosUsers::formatPhone($phone);
                                        $chat->save();
                                        $this->paintViewById($chat, $bot, $message, 405, null, true);
                                        return true;
                                    } else {
                                        $this->paintViewById($chat, $bot, $message, 400, null, true);
                                        return true;
                                    }

                                } elseif ($this->currentRegion->po == OosCities::HIVE_SYSTEM) {

                                    $responce = $this->api->auth([
                                        'phone' => OosUsers::formatPhone($phone)
                                    ]);

                                    if (!empty($responce['id'])) {
                                        $chat->phone = OosUsers::formatPhone($phone);
                                        $params = $chat->getParams();
                                        $params['tmps_id'] = $responce['id'];
                                        $chat->setParams($params);
                                        $chat->save();

                                        $this->paintViewById($chat, $bot, $message, 405, null, true);
                                        return true;
                                    } else {
                                        $this->paintViewById($chat, $bot, $message, 400, null, true);
                                        return true;
                                    }
                                } elseif ($this->currentRegion->po == OosCities::XDN_SYSTEM) {

                                    $responce = $this->api->auth([
                                        'phone' => OosUsers::formatPhone($phone)
                                    ]);

                                    if (!empty($responce['code']) && $responce['code'] == 200) {
                                        $chat->phone = OosUsers::formatPhone($phone);
                                        $chat->save();
                                        $this->paintViewById($chat, $bot, $message, 405, null, true);
                                        return true;
                                    } else {
                                        $this->paintViewById($chat, $bot, $message, 400, null, true);
                                        return true;
                                    }
                                }
                            }

                            $this->paintViewById($chat, $bot, $message, self::ORDER_VIEW);
                            return true;
                        }

                        //Координаты места назначения
                        if ($comandC->id == 504 || $comandC->id == 4000) {

                            $coords = $this->getCoords($message);

                            if (!empty($coords)) {
                                $this->saveLocation($chat->id, [], $coords);
                            }
                        }

                        //Время
                        if ($comandC->id == 512) {
                            $this->saveOrderTime($chat->id, $message->getText());
                        }

                        //Комментарий
                        if ($comandC->id == 525) {
                            $this->saveComment($chat->id, $message->getText());
                        }

                        $this->paintViewById($chat, $bot, $message, null, $comandC);
                    } else {

                        $nonError = true;
                        //Языки
                        foreach($this->langs as $lang) {
                            if ($lang->lang == $message->getText()) {

                                if (!empty($chat)) {
                                    $chat->lang = $lang->code;
                                    $chat->save();
                                    $this->lang = $chat->lang ?? self::DEFAULT_LANG;
                                }

                                $this->paintViewById($chat, $bot, $message, self::MAIN_VIEW);
                                $nonError = false;
                                break;
                            }
                        }

                        //Регионы
                        foreach($this->regions as $region) {
                            if ($region->{'title_' . $this->lang} == $message->getText()) {

                                if ($region->id != 5) {
                                    $this->cancelOrder($chat->id, $chat);
                                    $this->createOrder($chat->id);

                                    if (!empty($chat)) {
                                        $chat->phone = '';
                                        $chat->status = OosUsers::STATUS_ACTIVE;
                                        $chat->city_id = $region->id;
                                        $chat->save();
                                    }

                                    if (empty($chat->phone) || ($chat->status == OosUsers::STATUS_ACTIVE)) {
                                        $this->paintViewById($chat, $bot, $message, 400, null, true);
                                    } else {
                                        $this->paintViewById($chat, $bot, $message, self::ORDER_VIEW);
                                    }

                                } else {
                                    //Крым
                                    $this->paintViewById($chat, $bot, $message, self::REGION_VIEW_KRIM, null, true);
                                }

                                $nonError = false;
                                break;
                            }
                        }

                        if ($nonError) {
                            $this->paintErrorView($bot, $message, 8);
                        }
                    }

                    $botCurrent->track($message, 'text_comands');
                }
            }, function($message) use ($bot, $keyboards) {
                return true;
            });

            $bot->run();
        } catch (\TelegramBot\Api\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }
    }

    private function resendCode($chat, $text)
    {
        if ($this->currentRegion->po == OosCities::HIVE_SYSTEM) {
            $params = $chat->getParams();
            $responce = $this->api->reSend(['id' => $params['tmps_id']]);
        } elseif ($this->currentRegion->po == OosCities::TAXIK_SYSTEM) {
            $responce = $this->eradv->auth([
                'phone' => $chat->phone
            ]);
        }
    }

    /**
     * @param $message
     * @return array
     */
    private function getCoords($message)
    {
        $location = $message->getLocation();
        $textLocation = $message->getText();
        $coords = [];

        if (!empty($textLocation)) {

            if ($this->lang != 'ru') {
                $params = [
                    'text' => $textLocation,
                    'napr' => $this->lang . '-ru'
                ];

                $textLocation = $this->parserPlugin->getTranslate($params);
            }
            $coords = $this->parserPlugin->getGeoCoords([
                'search' => $this->currentRegion->title_ru . ', ' . $textLocation
            ]);
        } elseif (!empty($location)) {
            $coords = $this->parserPlugin->getGeoCoords([
                'geo' => [
                    'lat' => $location->getLatitude(),
                    'lon' => $location->getLongitude()
                ]
            ]);
        }

        return $coords;
    }

    /**
     * @param $message
     */
    private function addUser($message)
    {
        $usersHref = new OosUsers();
        $usersHref->status = OosUsers::STATUS_ACTIVE;
        $usersHref->chat_id = $message->getChat()->getId();
        $usersHref->date_created = (new \DateTime())->format("Y-m-d H:i:s");
        $usersHref->params = json_encode([
            'firstName' => $message->getChat()->getFirstName(),
            'lastName' => $message->getChat()->getLastName(),
            'userName' => $message->getChat()->getUsername()
        ]);
        $usersHref->save();
    }

    /**
     * @param $order
     */
    private function addOrder($order)
    {
        $ordersHref = new OosOrders();
        $ordersHref->user_id = $order['user_id'];
        $ordersHref->status = $order['status'];
        $ordersHref->date_created = (new \DateTime())->format("Y-m-d H:i:s");
        $ordersHref->add();
    }

    /**
     * Вывод ошибок
     * @param $bot
     * @param $message
     */
    private function paintErrorView($bot, $message, $num)
    {
        $comand = OosComands::getComandById($num);
        $bot->sendMessage($message->getChat()->getId(), $comand->{'title_' . $this->lang}, 'HTML', null, null);
    }

    /**
     * Отрисовка Экрана
     *
     * @param $chat
     * @param $bot
     * @param $message
     * @param $id
     * @param null $comandC
     * @param bool $isScreen
     */
    private function paintViewById($chat, $bot, $message, $id, $comandC = null, $isScreen = false)
    {
        $tariffs = [
            5 => [4001, 510],
            0 => [507, 508, 509, 510]
        ];

        $this->logger->debug(print_r($message, true));
        $comandC = $comandC ?? OosComands::getComandById($id);
        $next = $isScreen ? $comandC->parent : ($comandC->next ?? $comandC->id);
        $this->logger->debug(print_r($next, true));
        $comands = OosComands::getComandsByParent($next);
        $keyboardsArr = [];

        if ($comands) {
            foreach($comands as $comand) {

                //Для тарифов
                if ($next == 506 && !empty($this->currentRegion) && !in_array($comand->id, $tariffs[$this->currentRegion->region])) {
                    continue;
                }

                if ($comand->request_contact == 1) {
                    $keyboardsArr[] = [['text' => $comand->{'title_' . $this->lang}, 'request_contact' => true]];
                } elseif ($comand->request_location == 1) {
                    $keyboardsArr[] = [['text' => $comand->{'title_' . $this->lang}, 'request_location' => true]];
                } else {
                    $keyboardsArr[] = [['text' => stripcslashes($comand->{'title_' . $this->lang})]];
                }
            }
        }

        if (!empty($comandC->{'description_' . $this->lang})) {
            $description = $comandC->{'description_' . $this->lang};

            switch ($description) {
                case '{weather}':
                    $location = $message->getLocation();
                    $description = $this->createWeatherScreen($message->getText(), $location);
                    break;
                case '{cbr}':
                    $description = $this->createCbrScreen();
                    break;
                case '{translate}':
                    $description = $this->createTranslateScreen($message->getText(), $message->getChat()->getId());
                    break;
                case '{order}':
                    $description = $this->createOrderScreen($message->getText(), $chat->id, $chat);

                    if (empty($description)) {
                        $this->paintViewById($chat, $bot, $message, self::ORDER_VIEW);
                        return;
                    }
                    break;
                default:
                    break;
            }

            $keyboards = new \TelegramBot\Api\Types\ReplyKeyboardMarkup($keyboardsArr, null, true);

            if (is_array($description)) {
                $cArray = count($description);

                for($i = 0; $i < $cArray; $i++) {
                    $bot->sendMessage($message->getChat()->getId(), $description[$i], 'HTML', null, null, $keyboards);
                }

            } else {
                $countChars = mb_strlen($description, "UTF-8");

                if ($countChars > 4096) {
                    $parts = $this->mb_str_split($description, 4096);
                    $c = count($parts);

                    if ($c > 0) {
                        for($i = 0; $i < $c; $i++) {
                            $bot->sendMessage($message->getChat()->getId(), mb_substr($parts[$i], 0, 4095, 'UTF-8'), 'HTML', null, null, $keyboards);
                        }
                    }

                } else {
                    $bot->sendMessage($message->getChat()->getId(), mb_substr($description, 0, 4095, 'UTF-8'), 'HTML', null, null, $keyboards);
                }
            }
        }

        $this->saveComandToLog($message->getChat()->getId(), $comandC, $comandC->is_read == 1 ? ['message' => $message->getText()] : []);
    }

    /**
     * @param $string
     * @param int $string_length
     * @return array
     */
    private function mb_str_split($string, $string_length = 1)
    {
        if (mb_strlen($string) > $string_length || !$string_length) {
            do {
                $c = mb_strlen($string);
                $parts[] = mb_substr($string, 0, $string_length);
                $string = mb_substr($string, $string_length);
            } while (!empty($string));
        } else {
            $parts = array($string);
        }
        return $parts;
    }

    /**
     * Логи
     *
     * @param $id
     * @param $comand
     * @param array $params
     */
    private function saveComandToLog($id, $comand, $params = [])
    {
        $oosComandsLog = new OosComandsLog();
        $oosComandsLog->comand_id = $comand->id;
        $oosComandsLog->user_id = $id;
        $oosComandsLog->date_created = (new \DateTime())->format("Y-m-d H:i:s");
        $oosComandsLog->setParams($params);
        $oosComandsLog->add();
    }

    /**
     * Погода
     *
     * @param $textLocation
     * @param \TelegramBot\Api\Types\Location|null $location
     * @return array
     */
    private function createWeatherScreen($textLocation, \TelegramBot\Api\Types\Location $location = null)
    {
        $datas = [];
        $coords = [];

        if (!empty($textLocation)) {
            $coords = $this->parserPlugin->getGeoCoords([
                'search' => $textLocation
            ]);
        } elseif (!empty($location)) {
            $coords = [
                'lat' => $location->getLatitude(),
                'lon' => $location->getLongitude()
            ];
        }

        if (!empty($coords)) {
            $dataArray = $this->parserPlugin->parseWeather(['search' => $coords, 'lang' => $this->lang]);

            $dates = [];
            $city = '';

            for($dataArray->rewind(); $dataArray->valid(); $dataArray->next()) {
                try {
                    $forecast = $dataArray->current();

                    if (isset($dates[$forecast->time->day->format("Y-m-d")])) {
                        $dates[$forecast->time->day->format("Y-m-d")][] = $forecast;
                    } else {
                        $dates[$forecast->time->day->format("Y-m-d")] = [];
                        $dates[$forecast->time->day->format("Y-m-d")][] = $forecast;
                    }
                    $city = $forecast->city->name;
                } catch (\Exception $e) {
                    $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
                }
            }

            if ($dates) {
                foreach($dates as $k => $dt) {
                    $datas[] = $this->getWeather($k, $dt);
                }
            }
        }
        return $datas;
    }

    /**
     * @param $k
     * @param $dt
     * @return string
     */
    private function getWeather($k, $dt)
    {
        $data = '';
        if (!empty($dt)) {
            switch ($this->lang) {
                case 'it':
                    $data = '<b>Previsioni meteo per il giorno ' . (new \DateTime($k))->format('d.m.Y') . '</b>' . PHP_EOL;
                    foreach($dt as $k1 => $d1) {
                        $temp = str_replace('&deg;', '', $d1->temperature->now->getFormatted());
                        $data .= '<b>' . $d1->time->from->format('H:i') . ' - ' . $d1->time->to->format('H:i') . '</b>:' . PHP_EOL;
                        $data .= '<i>' . $d1->weather . '</i>' . PHP_EOL;
                        $data .= "<i>Temperatura dell'aria: " . $temp . '</i>' . PHP_EOL;
                        $data .= '<i>Umidità: ' . $d1->humidity->getFormatted() . '</i>' . PHP_EOL;
                        $data .= '<i>Vento: ' . $d1->wind->speed->getFormatted() . '</i>';
                        $data .= PHP_EOL . PHP_EOL . PHP_EOL;
                    }
                    break;
                case 'de':
                    $data = '<b>Wettervorhersage für ' . (new \DateTime($k))->format('d.m.Y') . '</b>' . PHP_EOL;
                    foreach($dt as $k1 => $d1) {
                        $temp = str_replace('&deg;', '', $d1->temperature->now->getFormatted());
                        $data .= '<b>' . $d1->time->from->format('H:i') . ' - ' . $d1->time->to->format('H:i') . '</b>:' . PHP_EOL;
                        $data .= '<i>' . $d1->weather . '</i>' . PHP_EOL;
                        $data .= '<i>Lufttemperatur: ' . $temp . '</i>' . PHP_EOL;
                        $data .= '<i>Die Luftfeuchtigkeit: ' . $d1->humidity->getFormatted() . '</i>' . PHP_EOL;
                        $data .= '<i>Der Wind: ' . $d1->wind->speed->getFormatted() . '</i>';
                        $data .= PHP_EOL . PHP_EOL . PHP_EOL;
                    }
                    break;
                case 'ru':
                    $data = '<b>Прогноз погоды на ' . (new \DateTime($k))->format('d.m.Y') . '</b>' . PHP_EOL;
                    foreach($dt as $k1 => $d1) {
                        $temp = str_replace('&deg;', '', $d1->temperature->now->getFormatted());
                        $data .= '<b>' . $d1->time->from->format('H:i') . ' - ' . $d1->time->to->format('H:i') . '</b>:' . PHP_EOL;
                        $data .= '<i>' . $d1->weather . '</i>' . PHP_EOL;
                        $data .= '<i>Температура воздуха: ' . $temp . '</i>' . PHP_EOL;
                        $data .= '<i>Влажность: ' . $d1->humidity->getFormatted() . '</i>' . PHP_EOL;
                        $data .= '<i>Ветер: ' . $d1->wind->speed->getFormatted() . '</i>';
                        $data .= PHP_EOL . PHP_EOL . PHP_EOL;
                    }
                    break;
                case 'es':
                    $data = '<b>Pronóstico del tiempo para el ' . (new \DateTime($k))->format('d/m/Y') . '</b>' . PHP_EOL;
                    foreach($dt as $k1 => $d1) {
                        $temp = str_replace('&deg;', '', $d1->temperature->now->getFormatted());
                        $data .= '<b>' . $d1->time->from->format('H:i') . ' - ' . $d1->time->to->format('H:i') . '</b>:' . PHP_EOL;
                        $data .= '<i>' . $d1->weather . '</i>' . PHP_EOL;
                        $data .= '<i>Temperatura del aire: ' . $temp . '</i>' . PHP_EOL;
                        $data .= '<i>La humedad: ' . $d1->humidity->getFormatted() . '</i>' . PHP_EOL;
                        $data .= '<i>El viento: ' . $d1->wind->speed->getFormatted() . '</i>';
                        $data .= PHP_EOL . PHP_EOL . PHP_EOL;
                    }
                    break;
                case 'en':
                    $data = '<b>Weather forecast for ' . (new \DateTime($k))->format('d.m.Y') . '</b>' . PHP_EOL;
                    foreach($dt as $k1 => $d1) {
                        $temp = str_replace('&deg;', '', $d1->temperature->now->getFormatted());
                        $data .= '<b>' . $d1->time->from->format('H:i') . ' - ' . $d1->time->to->format('H:i') . '</b>:' . PHP_EOL;
                        $data .= '<i>' . $d1->weather . '</i>' . PHP_EOL;
                        $data .= '<i>Air temperature: ' . $temp . '</i>' . PHP_EOL;
                        $data .= '<i>Humidity: ' . $d1->humidity->getFormatted() . '</i>' . PHP_EOL;
                        $data .= '<i>Wind: ' . $d1->wind->speed->getFormatted() . '</i>';
                        $data .= PHP_EOL . PHP_EOL . PHP_EOL;
                    }
                    break;
                case 'fr':
                    $data = '<b>Météo pour le ' . (new \DateTime($k))->format('d.m.Y') . '</b>' . PHP_EOL;
                    foreach($dt as $k1 => $d1) {
                        $temp = str_replace('&deg;', '', $d1->temperature->now->getFormatted());
                        $data .= '<b>' . $d1->time->from->format('H:i') . ' - ' . $d1->time->to->format('H:i') . '</b>:' . PHP_EOL;
                        $data .= '<i>' . $d1->weather . '</i>' . PHP_EOL;
                        $data .= "<i>Température de l'air: " . $temp . '</i>' . PHP_EOL;
                        $data .= "<i>L'humidité: " . $d1->humidity->getFormatted() . '</i>' . PHP_EOL;
                        $data .= '<i>Le vent: ' . $d1->wind->speed->getFormatted() . '</i>';
                        $data .= PHP_EOL . PHP_EOL . PHP_EOL;
                    }
                    break;
            }
        }
        return $data;
    }

    /**
     * Курсы валют
     *
     * @return string
     */
    private function createCbrScreen()
    {
        $data = [];
        $cbrList = $this->parserPlugin->parseCbr();

        $data[] = $this->getCbrData();

        if (count($cbrList) > 0) {
            foreach($cbrList as $v) {
                $data[] = $v->{'title_' . $this->lang} . "(<b>" . trim($v->char_code) . "</b>) - " . $v->value . " <b>₽</b>\n";
            }
        }

        return implode("", $data);
    }


    /**
     * @return string
     */
    private function getCbrData()
    {
        $description = '';

        switch ($this->lang) {
            case 'ru':
                $description .= '<b>Курсы валют ЦБ РФ на ' . ((new \DateTime())->format("d.m.Y")) . '</b>' . PHP_EOL . PHP_EOL;
                break;
            case 'en':
                $description .= '<b>Exchange rates of the Central Bank of the RF as of ' . ((new \DateTime())->format("d.m.Y")) . '</b>' . PHP_EOL . PHP_EOL;
                break;
            case 'es':
                $description .= '<b>Tipos de cambio del Banco Central de Rusia al  ' . ((new \DateTime())->format("d/m/Y")) . '</b>' . PHP_EOL . PHP_EOL;
                break;
            case 'it':
                $description .= '<b>Tassi di cambio della Banca Centrale Russa per il giorno ' . ((new \DateTime())->format("d.m.Y")) . '</b>' . PHP_EOL . PHP_EOL;
                break;
            case 'de':
                $description .= '<b>Wechselkurse der Zentralbank der Russischen Föderation am ' . ((new \DateTime())->format("d.m.Y")) . '</b>' . PHP_EOL . PHP_EOL;
                break;
            case 'fr':
                $description .= '<b>Courses des devises de La Banque Centrale de Fédération de Russie pour le ' . ((new \DateTime())->format("d.m.Y")) . '</b>' . PHP_EOL . PHP_EOL;
                break;
        }


        return $description;
    }

    /**
     * Переводчик
     *
     * @return string
     */
    private function createTranslateScreen($textM, $id)
    {
        $logs = OosComandsLog::getComandsByUserId($id, 2);
        $napr = '';
        $text = '';
        $translation = '';

        if (count($logs) == 2) {
            $params2 = $logs[0]->getParams();
            $params1 = $logs[1]->getParams();

            $l1 = '';
            $l2 = '';

            foreach($this->langs as $lang) {

                if ($lang->lang == $params2['message']) {
                    $l1 = $lang->code;
                }

                if ($lang->lang == $textM) {
                    $l2 = $lang->code;
                }
            }

            $params = [
                'text' => $params1['message'] ?? '',
                'napr' => $l1 . '-' . $l2
            ];

            $translation = $this->parserPlugin->getTranslate($params);
        }

        return $translation;
    }

    /**
     * Заказ такси
     *
     * @return string
     */
    private function createOrderScreen($text, $userId, $chat)
    {
        $order = $this->getLast($userId);
        $description = '';

        if (!empty($order)) {
            $price = 0;
            $params = $order->getParams();

            if ($this->currentRegion->po == OosCities::TAXIK_SYSTEM) {

                if ($params['tarif'] == 507 || $params['tarif'] == 508) {
                    $tariff = $this->api->getTarif($this->currentRegion, $params['tarif']);
                    $inp = $this->api->price(
                        [
                            'bookingTime' => $params['time'],
                            'tariff' => $tariff,
                            'in' => $params['start'],
                            'out' => $params['end']
                        ]
                    );

                    if (!empty($inp) && isset($inp['approximatePrice'])) {
                        $price = $inp['approximatePrice'];
                        $params['price'] = $price;
                    }
                } elseif ($params['tarif'] == 509) {
                    $uP = $chat->getParams();
                    $this->eradv->setAccessKey($uP['out_key']);
                    $tariff = $this->eradv->getTarif($this->currentRegion, $params['tarif']);
                    $class = $this->eradv->getClass();
                    $inp = $this->eradv->price(
                        [
                            'bookingTime' => $params['time'],
                            'tariff' => $tariff,
                            'in' => $params['start'],
                            'out' => $params['end'],
                            'class' => $class
                        ]
                    );

                    if (!empty($inp) && isset($inp['data']['summary'])) {
                        $price = $inp['data']['summary'];
                        $params['price'] = $price;
                    }
                }

            } elseif ($this->currentRegion->po == OosCities::HIVE_SYSTEM) {

                $tariff = $this->api->getTarif($this->currentRegion, $params['tarif']);
                $inp = $this->api->price(
                    [
                        'bookingTime' => $params['time'],
                        'tariff' => $tariff,
                        'in' => $params['start'],
                        'out' => $params['end']
                    ]
                );

                if (!empty($inp) && isset($inp['cost']['amount'])) {
                    $price = $inp['cost']['amount'];
                    $params['price'] = $price;
                }

            } elseif ($this->currentRegion->po == OosCities::XDN_SYSTEM) {
                $uP = $chat->getParams();
                $this->api->setAccessKey($uP['out_key']);
                $tariff = $this->api->getTarif($this->currentRegion, $params['tarif']);
                $class = $this->api->getClass();
                $inp = $this->api->price(
                    [
                        'bookingTime' => $params['time'],
                        'tariff' => $tariff,
                        'in' => $params['start'],
                        'out' => $params['end'],
                        'class' => $class
                    ]
                );

                if (!empty($inp) && isset($inp['data']['summary'])) {
                    $price = $inp['data']['summary'];
                    $params['price'] = $price;
                }
            }

            if ($price) {
                $description = $this->getPriceData($params, $price);
                $order->setParams($params);
                $order->save();
            }
        }

        return $description;
    }

    /**
     * @param $params
     * @param $price
     * @return string
     */
    private function getPriceData($params, $price)
    {
        $description = '';

        switch ($this->lang) {
            case 'ru':
                $description .= '<i>Подача: ' . $params['start']['address'] . '</i>' . PHP_EOL;
                $description .= '<i>Назначение: ' . $params['end']['address'] . '</i>' . PHP_EOL;
                $description .= '<i>Тариф: ' . $params['tarif_name'] . '</i>' . PHP_EOL;
                $description .= '<i>Время подачи: ' . (new \DateTime($params['time']))->format("H:i:s d.m.Y") . '</i>' . PHP_EOL;
                $description .= '<i>Стоимость: ' . $price . ' ₽</i>' . PHP_EOL;
                break;
            case 'en':
                $description .= '<i>Pick-up: ' . $params['start']['address'] . '</i>' . PHP_EOL;
                $description .= '<i>Destination: ' . $params['end']['address'] . '</i>' . PHP_EOL;
                $description .= '<i>Tariff: ' . $params['tarif_name'] . '</i>' . PHP_EOL;
                $description .= '<i>Pick-up time: ' . (new \DateTime($params['time']))->format("H:i:s d.m.Y") . '</i>' . PHP_EOL;
                $description .= '<i>Cost: ' . $price . ' ₽</i>' . PHP_EOL;
                break;
            case 'es':
                $description .= '<i>Llegada: ' . $params['start']['address'] . '</i>' . PHP_EOL;
                $description .= '<i>Destino: ' . $params['end']['address'] . '</i>' . PHP_EOL;
                $description .= '<i>Tarifa: ' . $params['tarif_name'] . '</i>' . PHP_EOL;
                $description .= '<i>Hora de llegada: ' . (new \DateTime($params['time']))->format("H:i:s d.m.Y") . '</i>' . PHP_EOL;
                $description .= '<i>Costo: ' . $price . ' ₽</i>' . PHP_EOL;
                break;
            case 'it':
                $description .= '<i>Prelievo: ' . $params['start']['address'] . '</i>' . PHP_EOL;
                $description .= '<i>Destinazione: ' . $params['end']['address'] . '</i>' . PHP_EOL;
                $description .= '<i>Tariffa: ' . $params['tarif_name'] . '</i>' . PHP_EOL;
                $description .= '<i>Orario del prelievo: ' . (new \DateTime($params['time']))->format("H:i:s d.m.Y") . '</i>' . PHP_EOL;
                $description .= '<i>Costo: ' . $price . ' ₽</i>' . PHP_EOL;
                break;
            case 'de':
                $description .= '<i>Abholort: ' . $params['start']['address'] . '</i>' . PHP_EOL;
                $description .= '<i>Zielort: ' . $params['end']['address'] . '</i>' . PHP_EOL;
                $description .= '<i>Tarif: ' . $params['tarif_name'] . '</i>' . PHP_EOL;
                $description .= '<i>Abholzeit: ' . (new \DateTime($params['time']))->format("H:i:s d.m.Y") . '</i>' . PHP_EOL;
                $description .= '<i>Preis: ' . $price . ' ₽</i>' . PHP_EOL;
                break;
            case 'fr':
                $description .= '<i>Prise: ' . $params['start']['address'] . '</i>' . PHP_EOL;
                $description .= '<i>Destination: ' . $params['end']['address'] . '</i>' . PHP_EOL;
                $description .= '<i>Tarif: ' . $params['tarif_name'] . '</i>' . PHP_EOL;
                $description .= "<i>L'heure de prise:" . (new \DateTime($params['time']))->format("H:i:s d.m.Y") . '</i>' . PHP_EOL;
                $description .= '<i>Prix: ' . $price . ' ₽</i>' . PHP_EOL;
                break;
        }


        return $description;
    }

    /**
     * Задание времени
     */
    private function saveOrderTime($userId, $time, $isCurrent = false)
    {
        $order = $this->getLast($userId);

        if (!empty($order)) {
            $params = $order->getParams();

            try {
                $timeNow = (new \DateTime())->getTimestamp();
                $timeC = (new \DateTime($time))->getTimestamp();

                if ($timeC > $timeNow) {
                    $params['time'] = (new \DateTime())->modify(($this->currentRegion->timezone > 0 ? '+' : '-') . $this->currentRegion->timezone . ' hours ' . self::TIMEOUT_ORDER . ' minutes')->format('Y-m-d H:i:s');
                } else {
                    $params['time'] = (new \DateTime($time))->format('Y-m-d H:i:s');
                }

            } catch (\Exception $e) {
                $params['time'] = (new \DateTime())->modify(($this->currentRegion->timezone > 0 ? '+' : '-') . $this->currentRegion->timezone . ' hours ' . self::TIMEOUT_ORDER . ' minutes')->format('Y-m-d H:i:s');
            }

            if ($isCurrent) {
                $params['is_current'] = true;
            }
            $order->setParams($params);
            $order->save();
        }
    }

    /**
     * Задание тарифа
     */
    private function saveTarif($userId, $tarif, $name)
    {
        $order = $this->getLast($userId);

        if (!empty($order)) {
            $params = $order->getParams();
            $params['tarif'] = $tarif;
            $params['tarif_name'] = $name;
            $order->setParams($params);
            $order->save();
        }
    }

    /**
     * Создание заказа
     */
    private function createOrder($userId)
    {
        $this->addOrder([
            'user_id' => $userId,
            'status' => OosOrders::STATUS_SAVE
        ]);
    }

    /**
     * Отмена заказа
     */
    private function cancelOrder($userId, $chat)
    {
        $order = $this->getLast($userId);

        if (!empty($order)) {
            $params = $order->getParams();

            if (!empty($order->external_id)) {
                if ($this->currentRegion->po == OosCities::TAXIK_SYSTEM) {

                    if ($params['tarif'] == 507 || $params['tarif'] == 508) {
                        $inp = $this->api->cancel(
                            [
                                'orderId' => $order->external_id
                            ]
                        );

                        if (!empty($inp) && isset($inp['success']) && $inp['success']) {
                            $order->setStatus(OosOrders::STATUS_CANCEL);
                            $order->save();
                            return true;
                        }
                    } elseif ($params['tarif'] == 509) {
                        $uP = $chat->getParams();
                        $this->eradv->setAccessKey($uP['out_key']);
                        $inp = $this->eradv->cancel(
                            [
                                'orderId' => $order->external_id
                            ]
                        );

                        if (!empty($inp) && !empty($inp['data']['id'])) {
                            $order->setStatus(OosOrders::STATUS_CANCEL);
                            $order->save();
                            return true;
                        }
                    }


                } elseif ($this->currentRegion->po == OosCities::HIVE_SYSTEM) {
                    $uP = $chat->getParams();
                    $this->api->setKeys($uP['out_key'], $uP['out_id']);
                    $this->api->cancel(
                        [
                            'orderId' => $order->external_id
                        ]
                    );

                    $order->setStatus(OosOrders::STATUS_CANCEL);
                    $order->save();
                    return true;
                } elseif ($this->currentRegion->po == OosCities::XDN_SYSTEM) {

                    $uP = $chat->getParams();
                    $this->api->setAccessKey($uP['out_key']);
                    $inp = $this->api->cancel(
                        [
                            'orderId' => $order->external_id
                        ]
                    );

                    if (!empty($inp) && !empty($inp['data']['id'])) {
                        $order->setStatus(OosOrders::STATUS_CANCEL);
                        $order->save();
                        return true;
                    }

                }
            } else {
                $order->setStatus(OosOrders::STATUS_CANCEL);
                $order->save();
                return true;
            }
        }

        return false;
    }

    /**
     * Оформление заказа
     */
    private function waitOrder($phone, $userId, $chat)
    {
        $order = $this->getLast($userId);

        if (!empty($order)) {
            if ($this->currentRegion->po == OosCities::TAXIK_SYSTEM) {
                $params = $order->getParams();

                if ($params['tarif'] == 507 || $params['tarif'] == 508) {

                    $tariff = $this->api->getTarif($this->currentRegion, $params['tarif']);
                    $inp = $this->api->create(
                        [
                            'bookingTime' => $params['time'],
                            'tariff' => $tariff,
                            'in' => $params['start'],
                            'out' => $params['end'],
                            'comment' => $params['comment'],
                            'phone' => $phone,
                            'price' => $params['price']
                        ]
                    );

                    if (!empty($inp) && isset($inp['orderId'])) {
                        $params = $order->getParams();
                        $order->external_id = $inp['orderId'];
                        $order->setParams($params);
                        $order->setStatus(OosOrders::STATUS_WAIT);
                        $order->save();
                        return true;
                    }
                } elseif ($params['tarif'] == 509) {

                    $uP = $chat->getParams();
                    $this->eradv->setAccessKey($uP['out_key']);
                    $tariff = $this->eradv->getTarif($this->currentRegion, $params['tarif']);
                    $class = $this->eradv->getClass();
                    $inp = $this->eradv->create(
                        [
                            'bookingTime' => $params['time'],
                            'tariff' => $tariff,
                            'in' => $params['start'],
                            'out' => $params['end'],
                            'class' => $class,
                            'comment' => $params['comment'],
                            'is_current' => $params['is_current']
                        ]
                    );

                    if (!empty($inp) && isset($inp['data']['id'])) {
                        $params = $order->getParams();
                        $order->external_id = $inp['data']['id'];
                        $order->setParams($params);
                        $order->setStatus(OosOrders::STATUS_WAIT);
                        $order->save();
                        return true;
                    }
                }

            } elseif ($this->currentRegion->po == OosCities::HIVE_SYSTEM) {
                $uP = $chat->getParams();
                $params = $order->getParams();
                $tariff = $this->api->getTarif($this->currentRegion, $params['tarif']);
                $this->api->setKeys($uP['out_key'], $uP['out_id']);

                $inp = $this->api->create(
                    [
                        'bookingTime' => $params['time'],
                        'tariff' => $tariff,
                        'in' => $params['start'],
                        'out' => $params['end'],
                        'comment' => $params['comment'],
                        'phone' => $phone
                    ]
                );

                if (!empty($inp) && isset($inp['id'])) {
                    $params = $order->getParams();
                    $order->external_id = $inp['id'];
                    $order->setParams($params);
                    $order->setStatus(OosOrders::STATUS_WAIT);
                    $order->save();
                    return true;
                }
            } elseif ($this->currentRegion->po == OosCities::XDN_SYSTEM) {
                $params = $order->getParams();
                $uP = $chat->getParams();
                $this->api->setAccessKey($uP['out_key']);
                $tariff = $this->api->getTarif($this->currentRegion, $params['tarif']);
                $class = $this->api->getClass();
                $inp = $this->api->create(
                    [
                        'bookingTime' => $params['time'],
                        'tariff' => $tariff,
                        'in' => $params['start'],
                        'out' => $params['end'],
                        'class' => $class,
                        'comment' => $params['comment'],
                        'is_current' => $params['is_current']
                    ]
                );

                if (!empty($inp) && isset($inp['data']['id'])) {
                    $params = $order->getParams();
                    $order->external_id = $inp['data']['id'];
                    $order->setParams($params);
                    $order->setStatus(OosOrders::STATUS_WAIT);
                    $order->save();
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Сохранение маршрута
     *
     * @param $userId
     * @param array $start
     * @param array $end
     */
    private function saveLocation($userId, $start = [], $end = [])
    {
        $order = $this->getLast($userId);

        if (!empty($order)) {
            $params = $order->getParams();

            if (!empty($start)) {
                $params['start'] = $start;
            }

            if (!empty($end)) {
                $params['end'] = $end;
            }

            $order->setParams($params);
            $order->save();
        }
    }

    /**
     * Сохранение коммента
     */
    private function saveComment($userId, $comment)
    {
        $order = $this->getLast($userId);

        if (!empty($order)) {
            $params = $order->getParams();
            $params['comment'] = $comment;
            $order->setParams($params);
            $order->save();
        }
    }


    /**
     * @param $userId
     * @return \Phalcon\Mvc\Model
     */
    private function getLast($userId)
    {
        $order = OosOrders::findFirst(
            [
                "user_id = $userId AND status <> '" . OosOrders::STATUS_FINISH . "' AND status <> '" . OosOrders::STATUS_CANCEL . "'",
                "order" => "date_created DESC",
            ]
        );

        return $order;
    }
}