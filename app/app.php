<?php
/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */

use Phalcon\Events\Manager as EventsManager;
use Middleware\AppRequestValidator;
use Middleware\GetFtpDataService;
use Middleware\PingService;
use Middleware\RequestLogger;
use Middleware\TaxikService;

/**
 * Валидируем запрос. Не пропускаем невалидные запросы далее
 */
$app->before(new AppRequestValidator());

/**
 * Роуты
 */
$app->get('/', function() use ($app) {
    die('Service worked');
});

$app->post('/', function() use ($app) {
    $id = $app->request->get('id');

    if ($id == 'client') {
        $service = new GetFtpDataService();
        $service->call($app);
    } elseif ($id == 'driver') {

    } else {
        $app->response->setJsonContent(['status' => STATUS_ERR, 'error' => 'Service not found']);
    }
});

$app->get('/ping', function() use ($app) {
    $service = new PingService();
    $service->call($app);
});

$app->post('/taxik', function() use ($app) {
    $service = new TaxikService();
    $service->call($app);
});

$app->get('/eradv', function() use ($app) {
    die(json_encode(['status' => 2]));
});

$app->get('/hive', function() use ($app) {
    die(json_encode(['status' => 2]));
});

/**
 * После обработки роута логируем результат
 */
$app->after(new RequestLogger());

/**
 * Завершаем работу сервиса отправкой ответа
 */
$app->finish(function() use ($app) {
    $app->response->send();
});

/**
 * Not found handler
 */
$app->notFound(function() use ($app) {
    $app->di->get('errorLogger')->log(LOG_WARNING, 'Not found: ' . $_SERVER['REQUEST_URI']);
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    $app->response->setJsonContent(['status' => STATUS_ERR, 'error' => 'Route not found']);
});

/**
 * Ловушка исключений. Ловит все не пойманные исключения и ошибки, логирует их и возвращает нейтральный результат
 */
$app->error(function(\Throwable $e) use ($app) {
    $app->di->get('errorLogger')->logException($e);
    $app->response->setJsonContent(['status' => STATUS_ERR, 'error' => 'Uncaught error']);
    $app->response->send();
    return false;
});