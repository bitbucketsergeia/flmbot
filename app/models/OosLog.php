<?php

namespace Models;

class OosLog extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     * @Primary
     * @Identity
     * @Column(type="string", nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $date;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $ip;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $chat_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $action;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $type;
    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $params;

    /**
     *
     * @var integer
     * @Column(type="integer", nullable=true)
     */
    public $status;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $details;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'oos_log';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Log[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Log
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = pg_escape_string($action);
    }

    /**
     * @param string $type
     */
    public function setType($action)
    {
        $this->type = pg_escape_string($action);
    }


    /**
     * @param string $params
     */
    public function setParams($params)
    {
        if (is_array($params)) {
            $params = json_encode($params);
        }
        $this->params = $params;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = (int)$status;
    }

    /**
     * @param string $details
     */
    public function setDetails($details)
    {
        if (is_array($details)) {
            $details = json_encode($details);
        }
        $this->details = $details;
    }

    /**
     * @param string $status
     */
    public function setChatId($status)
    {
        $this->chat_id = $status;
    }

    // ----- SETTERS ------

}
