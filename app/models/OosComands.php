<?php

namespace Models;

use Phalcon\Security\Random;

class OosComands extends \Phalcon\Mvc\Model
{

    public $id;
    public $title_en;
    public $title_es;
    public $title_it;
    public $title_fr;
    public $title_de;
    public $title_ru;
    public $parent;
    public $request_contact;
    public $request_location;
    public $is_read;
    public $sort;
    public $description_en;
    public $description_es;
    public $description_it;
    public $description_fr;
    public $description_de;
    public $description_ru;
    public $is_prev;
    public $is_main;
    public $next;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'oos_comands';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Files[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Files
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function getComandByNameAndParent($name, $parent, $lang)
    {
        $parameters = [
            "conditions" => "title_" . $lang . " = ?1 AND parent = ?2",
            "bind" => [
                1 => $name,
                2 => $parent
            ]
        ];

        return parent::findFirst($parameters);
    }

    public static function getComandsByParent($parent)
    {
        $parameters = [
            "parent = $parent",
            "order" => "sort DESC"
        ];

        return parent::find($parameters);
    }

    public static function getComandById($id)
    {
        $parameters = [
            "id = $id",
            "order" => "sort DESC"
        ];
        return parent::findFirst($parameters);
    }

    /**
     * wrapper for create method - generate and set id
     */
    public function add()
    {
        $this->create();
        $this->refresh();
    }

    public function getInfo()
    {
        $result = $this->toArray();
        unset($result['id']);
        return $result;
    }
}