<?php

namespace Models;

use Phalcon\Mvc\MongoCollection as MongoCollection;

class OosData extends MongoCollection
{
    const TABLE_PREFIX = 'oos_data_';

    /**
     * Название коллекции
     * @var string
     */
    private static $source = '';

    /**
     * Дата получения
     *
     * @var string
     */
    public $date_download;

    /**
     * Дата формирования покета
     *
     * @var string
     */
    public $date_created;

    /**
     * Параметры xml файла
     *
     * @var array
     */
    public $params;

    /**
     * Guide
     *
     * @var string
     */
    public $guid;

    /**
     * Установка коллекции
     *
     * @param $type
     * @param $fz
     */
    public static function setStaticSource($fz, $type)
    {
        static::$source = self::TABLE_PREFIX . $fz . '_' . $type;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return static::$source;
    }

    /**
     * @param string $date
     */
    public function setDateCreated($date)
    {
        $this->date_created = $date;
    }

    /**
     * @param string $date
     */
    public function setDateDownload($date)
    {
        $this->date_download = $date;
    }

    /**
     * @param array $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

    /**
     * @param string $guid
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @return mixed
     */
    public function getDateDownload()
    {
        return $this->date_download;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @return mixed
     */
    public function getGuid()
    {
        return $this->guid;
    }
}