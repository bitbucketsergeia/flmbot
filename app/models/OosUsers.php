<?php

namespace Models;

use Phalcon\Security\Random;

class OosUsers extends \Phalcon\Mvc\Model
{
    CONST STATUS_ACTIVE = 'ACTIVE';
    CONST STATUS_DELETED = 'DELETE';
    CONST STATUS_CHECKED = 'CHECKED';

    public $id;
    public $params;
    public $chat_id;
    public $city_id;
    public $date_created;
    public $lang;
    public $phone;
    public $status;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'oos_users';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Files[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Files
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * wrapper for create method - generate and set id
     */
    public function add()
    {
        $this->create();
        $this->refresh();
    }

    public function getInfo()
    {
        $result = $this->toArray();
        unset($result['id']);
        return $result;
    }

    /**
     * @param string $date
     */
    public function setDateCreated($date)
    {
        $this->date_created = $date;
    }

    public function setChatId($chat_id)
    {
        $this->chat_id = $chat_id;
    }

    public function setCityId($chat_id)
    {
        $this->city_id = $chat_id;
    }

    public function setLang($lang)
    {
        $this->lang = $lang;
    }

    public function setPhone($phone)
    {
        $this->phone = self::formatPhone($phone);
    }

    public static function formatPhone($phone = '', $convert = false, $trim = true)
    {
        // If we have not entered a phone number just return empty
        if (empty($phone)) {
            return '';
        }

        // Strip out any extra characters that we do not need only keep letters and numbers
        $phone = preg_replace("/[^0-9A-Za-z]/", "", $phone);

        // Do we want to convert phone numbers with letters to their number equivalent?
        // Samples are: 1-800-TERMINIX, 1-800-FLOWERS, 1-800-Petmeds
        if ($convert == true) {
            $replace = array('2' => array('a', 'b', 'c'),
                '3' => array('d', 'e', 'f'),
                '4' => array('g', 'h', 'i'),
                '5' => array('j', 'k', 'l'),
                '6' => array('m', 'n', 'o'),
                '7' => array('p', 'q', 'r', 's'),
                '8' => array('t', 'u', 'v'), '9' => array('w', 'x', 'y', 'z'));

            // Replace each letter with a number
            // Notice this is case insensitive with the str_ireplace instead of str_replace
            foreach($replace as $digit => $letters) {
                $phone = str_ireplace($letters, $digit, $phone);
            }
        }

        // If we have a number longer than 11 digits cut the string down to only 11
        // This is also only ran if we want to limit only to 11 characters
        if ($trim == true && strlen($phone) > 11) {
            $phone = substr($phone, 0, 11);
        }

        // Perform phone number formatting here
        if (strlen($phone) == 7) {
            return preg_replace("/([0-9a-zA-Z]{3})([0-9a-zA-Z]{4})/", "$1-$2", $phone);
        } elseif (strlen($phone) == 10) {
            return preg_replace("/([0-9a-zA-Z]{3})([0-9a-zA-Z]{3})([0-9a-zA-Z]{4})/", "($1) $2-$3", $phone);
        } elseif (strlen($phone) == 11) {
            return preg_replace("/([0-9a-zA-Z]{1})([0-9a-zA-Z]{3})([0-9a-zA-Z]{3})([0-9a-zA-Z]{4})/", "+$1$2$3$4", $phone);
        }

        // Return original phone if not 7, 10 or 11 digits long
        return $phone;
    }

    public function setUserId($userId)
    {
        $this->user_id = $userId;
    }

    /**
     * @param string $params
     */
    public function setParams($params)
    {
        if (is_array($params)) {
            $params = json_encode($params);
        }
        $this->params = $params;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return json_decode($this->params, true);
    }
}
