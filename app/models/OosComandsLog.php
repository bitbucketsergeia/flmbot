<?php

namespace Models;

use Phalcon\Security\Random;

class OosComandsLog extends \Phalcon\Mvc\Model
{

    public $id;
    public $params;
    public $user_id;
    public $comand_id;
    public $date_created;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'oos_comands_log';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Files[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Files
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * wrapper for create method - generate and set id
     */
    public function add()
    {
        $this->create();
        $this->refresh();
    }

    public function getInfo()
    {
        $result = $this->toArray();
        unset($result['id']);
        return $result;
    }

    /**
     * @param string $date
     */
    public function setDateCreated($date)
    {
        $this->date = $date;
    }

    public function setComandId($comand_id)
    {
        $this->comand_id = $comand_id;
    }

    public function setUserId($userId)
    {
        $this->user_id = $userId;
    }

    /**
     * @param array $params
     */
    public function setParams($params)
    {
        if (is_array($params)) {
            $params = json_encode($params);
        }
        $this->params = $params;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return json_decode($this->params, true);
    }

    public static function getComandByUserId($id)
    {
        $parameters = [
            "user_id = $id",
            "order" => "date_created DESC"
        ];
        return parent::findFirst($parameters);
    }

    public static function getComandsByUserId($id, $limit = 0)
    {
        $parameters = [
            "user_id = $id",
            "order" => "date_created DESC"
        ];

        if ($limit > 0) {
            $parameters['limit'] = $limit;
        }

        return parent::find($parameters);
    }
}
