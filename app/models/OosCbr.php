<?php

namespace Models;

use Phalcon\Security\Random;

class OosCbr extends \Phalcon\Mvc\Model
{
    public $id;
    public $num_code;
    public $char_code;
    public $nominal;
    public $title_en;
    public $title_es;
    public $title_it;
    public $title_fr;
    public $title_de;
    public $title_ru;
    public $value;
    public $date_created;
    public $date_download;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'oos_cbr';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Files[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Files
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * wrapper for create method - generate and set id
     */
    public function add()
    {
        $this->create();
        $this->refresh();
    }

    public function getInfo()
    {
        $result = $this->toArray();
        unset($result['id']);
        return $result;
    }

}
