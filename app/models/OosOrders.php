<?php

namespace Models;

use Phalcon\Security\Random;

class OosOrders extends \Phalcon\Mvc\Model
{
    CONST STATUS_SAVE = 'SAVE';
    CONST STATUS_WAIT = 'WAIT';
    CONST STATUS_FINISH = 'FINISH';
    CONST STATUS_CANCEL = 'CANCEL';
    public $id;
    public $params;
    public $user_id;
    public $status;
    public $date_created;
    public $external_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("public");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'oos_orders';
    }

    /**
     * @param null $parameters
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * @param null $parameters
     * @return \Phalcon\Mvc\Model
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * wrapper for create method - generate and set id
     */
    public function add()
    {
        $this->create();
        $this->refresh();
    }

    public function getInfo()
    {
        $result = $this->toArray();
        unset($result['id']);
        return $result;
    }

    /**
     * @param string $date
     */
    public function setDateCreated($date)
    {
        $this->date_created = $date;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function setUserId($userId)
    {
        $this->user_id = $userId;
    }

    /**
     * @param string $params
     */
    public function setParams($params)
    {
        if (is_array($params)) {
            $params = json_encode($params);
        }
        $this->params = $params;
    }

    /**
     * @return mixed
     */
    public function getParams(){
        return json_decode($this->params, true);
    }
}
