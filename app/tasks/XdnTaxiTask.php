<?php

namespace Tasks;

use Plugins\ParserPlugin;
use Plugins\ExdanTaxiPlugin;
use Models\OosOrders;
use Models\OosCities;
use Models\OosLang;
use Models\OosUsers;
use Phalcon\Logger;
use \Phalcon\Mvc\Micro\MiddlewareInterface;
use \Phalcon\Mvc\Micro;
use Models\OosComands;
use Models\OosComandsLog;

class XdnTaxiTask extends BaseTask
{
    // $this->logger->debug('Request params: !!!!!');
    private $logger;

    //lang
    private $lang;

    //regions
    private $regions = [];

    //langs
    private $langs = [];

    //region
    private $currentRegion;

    /**
     * @var
     */
    private $api;

    public function mainAction(array $params)
    {
        $this->api = new ExdanTaxiPlugin($this->di);
        $dt = new \DateTime();
        $dt1 = clone $dt;
        $dt2 = clone $dt;
        $beginDate = $dt1->modify('-2 days');
        $endDate = $dt2->modify('+2 days');

        $orders = OosOrders::find(
            [
                'status = "' . OosOrders::STATUS_WAIT . '" AND date_created > "' . $beginDate->format('Y-m-d H:i:s') . '" AND date_created < "' . $endDate->format('Y-m-d H:i:s') . '"'
            ]
        );

        $config = $this->di->getConfig()->get('oosexchanger');
        $langHref = new OosLang();
        $citiesHref = new OosCities();
        $this->logger = $this->di->get('fileLogger');
        $this->langs = $langHref->find([]);
        $this->regions = $citiesHref->find([]);
        $bot = new \TelegramBot\Api\Client($config['bot_token']);
        $botCurrent = new \TelegramBot\Api\Botan($config['ya_metrika']);
        $keyboards = new \TelegramBot\Api\Types\ReplyKeyboardMarkup([], null, null);
        $logger = $this->logger;

        try {
            if (!empty($orders)) {
                foreach($orders as $order) {

                    $chat = OosUsers::findFirst(
                        [
                            "id = '" . $order->user_id . "' AND status<>'" . OosUsers::STATUS_DELETED . "'"
                        ]
                    );

                    if (!empty($chat)) {

                        foreach($this->regions as $region) {
                            if ($chat->city_id == $region->id) {
                                $this->currentRegion = $region;
                            }
                        }

                        $params = $order->getParams();

                        if (($this->currentRegion->po == OosCities::TAXIK_SYSTEM && $params['tarif'] == 509) || $this->currentRegion->po == OosCities::XDN_SYSTEM) {
                            $this->lang = $chat->lang;

                            if (empty($order->external_id)) {
                                continue;
                            }

                            $uP = $chat->getParams();
                            $this->api->setAccessKey($uP['out_key']);
                            $requestParamsData = $this->api->info(['orderId' => $order->external_id]);

                            if (!empty($requestParamsData['data'])) {
                                $requestParams = $requestParamsData['data'];

                                if (isset($requestParams['state'])) {

                                    if ($requestParams['state'] == 14) {
                                        $this->paintViewById($order, $requestParams, $chat, $bot, 650, null, true);
                                        $order->setStatus(OosOrders::STATUS_FINISH);
                                        $order->save();
                                    } elseif (in_array($requestParams['state'], [8, 9, 10])) {
                                        $this->paintViewById($order, $requestParams, $chat, $bot, 660, null, true);
                                        $order->setStatus(OosOrders::STATUS_CANCEL);
                                        $order->save();
                                    } elseif (in_array($requestParams['state'], [2, 3])) {
                                        $this->paintViewById($order, $requestParams, $chat, $bot, 601, null, true);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (\TelegramBot\Api\Exception $e) {
            $this->logger->debug($e->getMessage() . 'Trace: ' . $e->getTraceAsString());
        }

        die(json_encode(['status' => 2]));
    }

    /**
     * @param $order
     * @param $params
     * @param $chat
     * @param $bot
     * @param $id
     * @param null $comandC
     * @param bool $isScreen
     */
    private function paintViewById($order, $params, $chat, $bot, $id, $comandC = null, $isScreen = false)
    {
        $comandC = $comandC ?? OosComands::getComandById($id);
        $next = $isScreen ? $comandC->parent : ($comandC->next ?? $comandC->id);
        $comands = OosComands::getComandsByParent($next);
        $keyboardsArr = [];

        if ($comands) {
            foreach($comands as $comand) {
                if ($comand->request_contact == 1) {
                    $keyboardsArr[] = [['text' => $comand->{'title_' . $this->lang}, 'request_contact' => true]];
                } elseif ($comand->request_location == 1) {
                    $keyboardsArr[] = [['text' => $comand->{'title_' . $this->lang}, 'request_location' => true]];
                } else {
                    $keyboardsArr[] = [['text' => stripcslashes($comand->{'title_' . $this->lang})]];
                }
            }
        }

        if (!empty($comandC->{'description_' . $this->lang})) {
            $description = $comandC->{'description_' . $this->lang};

            switch ($description) {
                case '{check}':
                    $description = $this->viewStatus($order, $chat, $params);

                    if (empty($description)) {
                        die(json_encode(['status' => 3]));
                    }

                    break;
                case '{finish}':
                    $description = $this->viewFinish($order, $chat, $params);

                    if (empty($description)) {
                        die(json_encode(['status' => 3]));
                    }

                    break;
            }

            $keyboards = new \TelegramBot\Api\Types\ReplyKeyboardMarkup($keyboardsArr, null, true);

            if (is_array($description)) {
                $cArray = count($description);

                for($i = 0; $i < $cArray; $i++) {
                    $bot->sendMessage($chat->chat_id, $description[$i], 'HTML', null, null, $keyboards);
                }

            } else {
                $countChars = mb_strlen($description, "UTF-8");

                if ($countChars > 4096) {
                    $parts = $this->mb_str_split($description, 4096);
                    $c = count($parts);

                    if ($c > 0) {
                        for($i = 0; $i < $c; $i++) {
                            $bot->sendMessage($chat->chat_id, mb_substr($parts[$i], 0, 4095, 'UTF-8'), 'HTML', null, null, $keyboards);
                        }
                    }

                } else {
                    $bot->sendMessage($chat->chat_id, mb_substr($description, 0, 4095, 'UTF-8'), 'HTML', null, null, $keyboards);
                }
            }
        }
        $this->saveComandToLog($chat->chat_id, $comandC, $comandC->is_read == 1 ? ['message' => ''] : []);
    }

    /**
     * @param $order
     * @param $chat
     * @param $params
     * @return string
     */
    private function viewStatus($order, $chat, $params)
    {
        $description = '';

        if (!empty($params['driver'])) {
            $description = $this->getStatusData($params);
        }

        return $description;
    }

    /**
     * @param $order
     * @param $chat
     * @param $params
     * @return string
     */
    private function viewFinish($order, $chat, $params)
    {
        $description = $this->getFinishData($order, $params);
        return $description;
    }

    /**
     * @return string
     */
    private function getFinishData($order, $params)
    {
        $description = '';
        $params = $order->getParams();

        switch ($this->lang) {
            case 'ru':
                $description = 'Заказ закрыт на сумму ' . $params['price'] . ' ₽. Спасибо за то, что вы воспользовались сервисом Myconcierge.ru';
                break;
            case 'en':
                $description = 'Заказ закрыт на сумму ' . $params['price'] . ' ₽. Спасибо за то, что вы воспользовались сервисом Myconcierge.ru';
                break;
            case 'es':
                $description = 'Заказ закрыт на сумму ' . $params['price'] . ' ₽. Спасибо за то, что вы воспользовались сервисом Myconcierge.ru';
                break;
            case 'it':
                $description = 'Заказ закрыт на сумму ' . $params['price'] . ' ₽. Спасибо за то, что вы воспользовались сервисом Myconcierge.ru';
                break;
            case 'de':
                $description = 'Заказ закрыт на сумму ' . $params['price'] . ' ₽. Спасибо за то, что вы воспользовались сервисом Myconcierge.ru';
                break;
            case 'fr':
                $description = 'Заказ закрыт на сумму ' . $params['price'] . ' ₽. Спасибо за то, что вы воспользовались сервисом Myconcierge.ru';
                break;
        }

        return $description;
    }

    /**
     * @return string
     */
    private function getStatusData($params)
    {
        $description = '';

        $car = '';

        if ($this->lang != 'ru') {
            $paramsEr = [
                'text' => $params['driver']['car']['color'] . ' ' . $params['driver']['car']['brand'] . ' ' . $params['driver']['car']['model'],
                'napr' => 'ru-' . $this->lang
            ];

            $car = $this->parserPlugin->getTranslate($paramsEr);
        }

        switch ($this->lang) {
            case 'ru':
                switch ($params['state']) {
                    case 2:
                        $description .= '<i>Вам назначен ' . $params['driver']['car']['color'] . ' ' . $params['driver']['car']['brand'] . ' ' . $params['driver']['car']['model'] . ' ' . $params['driver']['car']['number'] . '</i>' . PHP_EOL;

                        /*if ($params['driver']['call']['allow'] == 'direct') {
                            $description .= '<i>Телефон водителя: ' . implode(', ', $params['driver']['call']['numbers']) . '</i>' . PHP_EOL;
                        }*/

                        $description .= '<b>Машина в пути. Я сообщу, как подъедет</b>' . PHP_EOL;
                        break;
                    case 3:

                        $description .= '<i>Вас ожидает ' . $params['driver']['car']['color'] . ' ' . $params['driver']['car']['brand'] . ' ' . $params['driver']['car']['model'] . ' ' . $params['driver']['car']['number'] . '</i>' . PHP_EOL;

                        /*if ($params['driver']['call']['allow'] == 'direct') {
                            $description .= '<i>Телефон водителя: ' . implode(', ', $params['driver']['call']['numbers']) . '</i>' . PHP_EOL;
                        }*/

                        $description .= '<b>Пожалуйста, выходите</b>' . PHP_EOL;
                        break;
                }
                break;
            case 'en':
                switch ($params['state']) {
                    case 2:
                        $description .= '<i>A ' . $car . ' ' . $params['driver']['car']['number'] . 'has been assigned to you.</i>' . PHP_EOL;

                        /*if ($params['driver']['call']['allow'] == 'direct') {
                            $description .= "<i>Driver's phone number: " . implode(', ', $params['driver']['call']['numbers']) . '</i>' . PHP_EOL;
                        }*/

                        $description .= '<b>Your car is on its way. I will notify you as soon as it arrives.</b>' . PHP_EOL;
                        break;
                    case 3:

                        $description .= '<i>А ' . $car . ' ' . $params['driver']['car']['number'] . 'is waiting for you.</i>' . PHP_EOL;

                        /*if ($params['driver']['call']['allow'] == 'direct') {
                            $description .= "<i>Driver's phone number: " . implode(', ', $params['driver']['call']['numbers']) . '</i>' . PHP_EOL;
                        }*/

                        $description .= '<b>Please come out</b>' . PHP_EOL;
                        break;
                }
                break;
            case 'es':
                switch ($params['state']) {
                    case 2:
                        $description .= '<i>Se le ha asignado un coche ' . $car . ' ' . $params['driver']['car']['number'] . '</i>' . PHP_EOL;

                        /*if ($params['driver']['call']['allow'] == 'direct') {
                            $description .= '<i>El teléfono del chofer: ' . implode(', ', $params['driver']['call']['numbers']) . '</i>' . PHP_EOL;
                        }*/

                        $description .= '<b>El coche ya está en camino. Le avisaré cuando llegue</b>' . PHP_EOL;
                        break;
                    case 3:

                        $description .= '<i>El coche ' . $car . ' ' . $params['driver']['car']['number'] . 'ya llegó y espera por Usted</i>' . PHP_EOL;

                        /*if ($params['driver']['call']['allow'] == 'direct') {
                            $description .= '<i>El teléfono del chofer: ' . implode(', ', $params['driver']['call']['numbers']) . '</i>' . PHP_EOL;
                        }*/

                        $description .= '<b>Por favor, ya puede salir</b>' . PHP_EOL;
                        break;
                }
                break;
            case 'it':
                switch ($params['state']) {
                    case 2:
                        $description .= '<i>La macchina assegnata è una ' . $car . ' ' . $params['driver']['car']['number'] . '</i>' . PHP_EOL;

                        /*if ($params['driver']['call']['allow'] == 'direct') {
                            $description .= '<i>Numero di telefono del conducente: ' . implode(', ', $params['driver']['call']['numbers']) . '</i>' . PHP_EOL;
                        }*/

                        $description .= '<b>La macchina è in viaggio. Ti informerò non appena arriverà.</b>' . PHP_EOL;
                        break;
                    case 3:

                        $description .= '<i>Ti aspetta una ' . $car . ' ' . $params['driver']['car']['number'] . '</i>' . PHP_EOL;

                        /*if ($params['driver']['call']['allow'] == 'direct') {
                            $description .= '<i>Numero di telefono del conducente: ' . implode(', ', $params['driver']['call']['numbers']) . '</i>' . PHP_EOL;
                        }*/

                        $description .= '<b>Per favore, vieni fuori</b>' . PHP_EOL;
                        break;
                }
                break;
            case 'de':
                switch ($params['state']) {
                    case 2:
                        $description .= '<i>Für Sie wurde ein ' . $car . ' ' . $params['driver']['car']['number'] . ' bestimmt.</i>' . PHP_EOL;

                        /*if ($params['driver']['call']['allow'] == 'direct') {
                            $description .= '<i>Telefonnummer des Fahrers: ' . implode(', ', $params['driver']['call']['numbers']) . '</i>' . PHP_EOL;
                        }*/

                        $description .= '<b>Ihr Auto ist unterwegs. Sobald das Auto ankommt, werden Sie benachrichtigt.</b>' . PHP_EOL;
                        break;
                    case 3:

                        $description .= '<i>Der ' . $car . ' ' . $params['driver']['car']['number'] . ' wartet auf Sie.</i>' . PHP_EOL;

                        /*if ($params['driver']['call']['allow'] == 'direct') {
                            $description .= '<i>Telefonnummer des Fahrers: ' . implode(', ', $params['driver']['call']['numbers']) . '</i>' . PHP_EOL;
                        }*/

                        $description .= '<b>Bitte komm heraus</b>' . PHP_EOL;
                        break;
                }
                break;
            case 'fr':
                switch ($params['state']) {
                    case 2:
                        $description .= '<i>Le ' . $car . ' ' . $params['driver']['car']['number'] . ' est commandé pour Vous.</i>' . PHP_EOL;

                        /*if ($params['driver']['call']['allow'] == 'direct') {
                            $description .= '<i>Téléphone de chauffeur: ' . implode(', ', $params['driver']['call']['numbers']) . '</i>' . PHP_EOL;
                        }*/

                        $description .= '<b>La vouture est en route. Je Vous informe, quand elle va arriver</b>' . PHP_EOL;
                        break;
                    case 3:

                        $description .= '<i>Le ' . $car . ' ' . $params['driver']['car']['number'] . 'noir Vous attend.</i>' . PHP_EOL;

                        /*if ($params['driver']['call']['allow'] == 'direct') {
                            $description .= '<i>Téléphone de chauffeur: ' . implode(', ', $params['driver']['call']['numbers']) . '</i>' . PHP_EOL;
                        }*/

                        $description .= "<b>Voulez-Vous sortir, s'il vous plait</b>" . PHP_EOL;
                        break;
                }
                break;
        }

        return $description;
    }

    /**
     * @param $string
     * @param int $string_length
     * @return array
     */
    private function mb_str_split($string, $string_length = 1)
    {
        if (mb_strlen($string) > $string_length || !$string_length) {
            do {
                $c = mb_strlen($string);
                $parts[] = mb_substr($string, 0, $string_length);
                $string = mb_substr($string, $string_length);
            } while (!empty($string));
        } else {
            $parts = array($string);
        }
        return $parts;
    }

    /**
     * @param $orderId
     * @return \Phalcon\Mvc\Model
     */
    private function getOrder($orderId)
    {
        $order = OosOrders::findFirst(
            [
                "external_id = '$orderId' AND status <> '" . OosOrders::STATUS_FINISH . "' AND status <> '" . OosOrders::STATUS_CANCEL . "'",
                "order" => "date_created DESC",
            ]
        );

        return $order;
    }

    /**
     * Логи
     *
     * @param $id
     * @param $comand
     * @param array $params
     */
    private function saveComandToLog($id, $comand, $params = [])
    {
        $oosComandsLog = new OosComandsLog();
        $oosComandsLog->comand_id = $comand->id;
        $oosComandsLog->user_id = $id;
        $oosComandsLog->date_created = (new \DateTime())->format("Y-m-d H:i:s");
        $oosComandsLog->setParams($params);
        $oosComandsLog->add();
    }
}