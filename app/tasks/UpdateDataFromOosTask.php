<?php

namespace Tasks;

use Plugins\ParserPlugin;

class UpdateDataFromOosTask extends BaseTask
{
    /**
     * @param array $params
     */
    public function mainAction(array $params)
    {
        $parserPlugin = new ParserPlugin($this->di);
        $els = $parserPlugin->parseCbr();
    }
}