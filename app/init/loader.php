<?php

/**
 * Registering an autoloader
 */
$loader = new \Phalcon\Loader();
$loader->registerNamespaces(
    [
        "Plugins" => $config->application->pluginsDir,
        "Lib" => $config->application->libDir,
        "Middleware" => $config->application->middlewareDir,
        "Models" => $config->application->modelsDir,
        "Services" => $config->application->servicesDir,
        "Services\\Exceptions" => $config->application->servicesExceptionsDir,
        "Services\\Interfaces" => $config->application->servicesInterfacesDir,
        "Services\\Validators" => $config->application->servicesValidatorsDir,
    ]
)->register();
