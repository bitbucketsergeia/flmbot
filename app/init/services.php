<?php

use Phalcon\Mvc\View\Simple as View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Logger\Adapter\File as FileAdapter;
use Phalcon\Logger\Adapter\Syslog as SyslogAdapter;
use Services\ErrorLogger;
use Services\EisLogger;
use Services\Storage as OosStorage;
use Phalcon\Mvc\Collection\Manager as CollectionManager;
use Phalcon\Db\Adapter\MongoDB\Client as MongoDbClient;
use Phalcon\Mvc\Model\Manager as ModelsManager;

/**
 * Sets the view component
 */
$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setViewsDir($config->application->viewsDir);

    return $view;
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
    $config = $this->getConfig();

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->db->adapter;
    $connection = new $class([
        'host' => $config->database->db->host,
        'username' => $config->database->db->username,
        'password' => $config->database->db->password,
        'dbname' => $config->database->db->dbname,
    ]);

    return $connection;
});

/**
 * MongoDb connection
 */
$di->setShared(
    "mongo",
    function () {
        $config = $this->getConfig();

        if (!$config->database->mongo->username || !$config->database->mongo->password) {
            $dsn = 'mongodb://' . $config->database->mongo->host;
        } else {
            $dsn = sprintf(
                'mongodb://%s:%s@%s',
                $config->database->mongo->username,
                $config->database->mongo->password,
                $config->database->mongo->host
            );
        }

        $mongo = new MongoDbClient($dsn);
        return $mongo->selectDatabase($config->database->mongo->dbname);
    }
);

$di->setShared('collectionManager', function () {
    return new CollectionManager();
});

$di->setShared('modelsManager', function() {
    return new ModelsManager();
});

/**
 * FTP
 */
$di->set(
    "ftp",
    [
        "className" => "Util\\Oos\\Ftp\\CoreFtp",
    ]
);

/**
 * FileManager
 */
$di->setShared(
    "fileManager",
    [
        "className" => "Util\\FileManager\\FileManager",
    ]
);

/**
 * XmlToArray
 */
$di->setShared(
    "xmlToArray",
    [
        "className" => "Util\\Oos\\Xml\\XmlToArrayConverter",
    ]
);

/**
 * EisConnector
 */
$di->set(
    "eisConnector",
    [
        "className" => "Services\\EisConnector",
        "properties" => [
            [
                "name" => "ftp",
                "value" => [
                    "type" => "service",
                    "name" => "ftp",
                ],
            ],
            [
                "name" => "fileManager",
                "value" => [
                    "type" => "service",
                    "name" => "fileManager",
                ],
            ]
        ]
    ]
);

/**
 * Processor
 */
$di->setShared(
    "oosProcessor",
    [
        "className" => "Services\\Processor",
        "properties" => [
            [
                "name" => "xmlToArray",
                "value" => [
                    "type" => "service",
                    "name" => "xmlToArray",
                ],
            ],
        ]
    ]
);

$di->setShared('eisLogger', function () {
    return new EisLogger();
});

$di->setShared('oosStorage', function () {
    return new OosStorage();
});

$di->setShared('dbLogger', function () {
    $logger = $this->getConfig()->get('dbLogger');

    return new $logger();
});

$di->setShared('sysLogger', function () {
    try {
        $logger = new SyslogAdapter('oosexchanger', $this->getConfig()->get('sysLogger'));
    } catch (\Exception $e) {
        return null;
    }

    return $logger;
});

$di->setShared('fileLogger', function () {
    try {
        $config = $this->getConfig()->get('fileLogger');
        if (!is_dir($config->logDir)) {
            @mkdir($config->logDir);
        }
        $logger = new FileAdapter($config->logDir . DIRECTORY_SEPARATOR . $config->logFile);
    } catch (\Exception $e) {
        return null;
    }

    return $logger;
});

$sysLogger = $di->get('sysLogger');
$fLogger = $di->get('fileLogger');

$di->setShared('errorLogger', function () use ($sysLogger, $fLogger) {
    $logger = new ErrorLogger($sysLogger, $fLogger);

    return $logger;
});

include APP_PATH . '/init/validator.php';
