<?php
/**
 * Created by PhpStorm.
 * User: kartashev
 * Date: 20.09.16
 * Time: 22:24
 */

use Phalcon\Validation;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\Between;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\InclusionIn;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;

/** @var Phalcon\Config $config */
$config = $di->getConfig()->get('oosexchanger');

/** @var Phalcon\Validation $validator */
$validator = new Validation();

$validator->add('source',
    new PresenceOf([
        'message' => 'Source not found'
    ])
);

/* TODO: Временное решение для того, чтобы овнер мог быть хоть строкой на русском языке, выпилить или оставить, когда будет ясность */
/*
$validator->add(
    'owner',
    new Regex([
        'pattern' => '/^[0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12}$/',
        'message' => 'Owner must be valid UUID'
    ])
);*/

$di->setShared('requestValidator', $validator);

// cliParamsValidator
$validator = new Validation();
$cronConfig = $config->get('cli');

$validator->add('fz', new InclusionIn([
    'message' => 'Invalid value of fz param.',
    'domain' => (array)$cronConfig['allowedParams']['fz'],
    'allowEmpty' => true,
]));

$validator->add('type', new InclusionIn([
    'message' => 'Invalid value of type param.',
    'domain' => (array)$cronConfig['allowedParams']['type'],
    'allowEmpty' => true,
]));

$validator->add('all', new InclusionIn([
    'message' => 'Invalid value of all param.',
    'domain' => (array)$cronConfig['allowedParams']['all'],
    'allowEmpty' => true,
]));

$di->setShared('updateDataFromOosTaskValidator', $validator);

// getParamsValidator
$validator = new Validation();
$appConfig = $config->get('app');

$validator->add('fz', new InclusionIn([
    'message' => 'Invalid value of fz param.',
    'domain' => (array)$appConfig['allowedParams']['fz'],
    'allowEmpty' => false,
]));

$validator->add('type',
    new PresenceOf([
        'message' => 'Invalid value of type param.'
    ])
);

$di->setShared('getParamsValidator', $validator);