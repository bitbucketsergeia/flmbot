<?php

error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

use Phalcon\Di\FactoryDefault\Cli as CliDI;
use Phalcon\Cli\Console as ConsoleApp;
use Phalcon\Loader;
use Phalcon\Cli\Dispatcher;
use Plugins\CliRequestValidatorPlugin;

// Используем стандартный для CLI контейнер зависимостей
$di = new CliDI();

// Загружаем файл конфигурации, если он есть

$configFile = __DIR__ . "/config/config.php";

if (is_readable($configFile)) {
    $config = include $configFile;

    $di->set("config", $config);
}

include __DIR__ . '/init/loader.php';
include __DIR__ . '/init/services.php';
include __DIR__ . '/init/validator.php';

/**
 * Include composer autoloader
 */
include BASE_PATH . "/vendor/autoload.php";

/**
 * Регистрируем автозагрузчик, и скажем ему, чтобы зарегистрировал каталог задач
 */
$loader = new Loader();
$loader->registerNamespaces(
    [
        "Tasks" => __DIR__ . "/tasks/",
    ]
);
$loader->register();

// Регистрация диспетчера
$di->set(
    "dispatcher",
    function () {
        $dispatcher = new Dispatcher();

        $dispatcher->setDefaultNamespace(
            "Tasks"
        );

        return $dispatcher;
    }
);

//Events Manager
$em = new Phalcon\Events\Manager();
$em->attach(
    "console:beforeHandleTask",
    new CliRequestValidatorPlugin()
);

// Создаем консольное приложение
$console = new ConsoleApp();
$console->setDI($di);
$console->setEventsManager($em);

/**
 * Определяем консольные аргументы
 */
$arguments = [];

foreach ($argv as $k => $arg) {
    if ($k === 1) {
        $arguments["task"] = $arg;
    } elseif ($k === 2) {
        $arguments["action"] = $arg;
    } elseif ($k >= 3) {
        $arguments["params"][] = $arg;
    }
}

try {
    // обрабатываем входящие аргументы
    $console->handle($arguments);
} catch (\Phalcon\Exception $e) {
    echo $e->getMessage();
    exit(255);
}