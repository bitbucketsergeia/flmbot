--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.6
-- Dumped by pg_dump version 9.5.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: oos_log_trigger(); Type: FUNCTION; Schema: public; Owner: mylogin
--
DROP FUNCTION IF EXISTS oos_log_trigger();

CREATE FUNCTION oos_log_trigger() RETURNS trigger
LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
  table_master varchar(255) := 'oos_log';
  table_part varchar(255) := '';
BEGIN
  -- Даём имя партиции --------------------------------------------------
  table_part := table_master
                || '_' || date_part( 'year', NEW.date )::text
                || '_' || date_part( 'month', NEW.date )::text;
  -- Проверяем партицию на существование --------------------------------
  PERFORM 1 FROM pg_class WHERE relname = table_part LIMIT 1;
  -- Если её ещё нет, то создаём --------------------------------------------
  IF NOT FOUND
  THEN
    -- Создаём партицию, наследуя мастер-таблицу --------------------------
    EXECUTE 'CREATE TABLE ' || table_part || ' (
                    id bigint NOT NULL DEFAULT nextval(''' || table_master || '_id_seq''::regclass),
                    CONSTRAINT ' || table_part || '_id_pk PRIMARY KEY (id)
            ) INHERITS ( ' || table_master || ' ) WITH ( OIDS=FALSE );';
    EXECUTE 'GRANT ALL ON TABLE ' || table_part || ' TO "mylogin";';
    EXECUTE 'GRANT ALL ON TABLE ' || table_part || ' TO "mylogin";';
    EXECUTE 'GRANT ALL ON TABLE ' || table_part || ' TO "mylogin";';
    EXECUTE 'GRANT SELECT ON TABLE ' || table_part || ' TO "mylogin";';

  END IF;
  -- Вставляем данные в партицию --------------------------------------------
  EXECUTE '
            INSERT INTO ' || table_part || ' SELECT ( (' || QUOTE_LITERAL(NEW) || ')::' || TG_RELNAME || ' ).*;';
  RETURN NULL;
END;
$$;


ALTER FUNCTION public.oos_log_trigger() OWNER TO mylogin;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: migration; Type: TABLE; Schema: public; Owner: mylogin
--
DROP TABLE IF EXISTS migration;

CREATE TABLE IF NOT EXISTS migration (
  version character varying(180) NOT NULL,
  apply_time integer
);


ALTER TABLE migration OWNER TO mylogin;

--
-- Name: oos_files; Type: TABLE; Schema: public; Owner: mylogin
--
DROP TABLE IF EXISTS oos_files;

CREATE TABLE oos_files (
  id character(36) NOT NULL,
  filename character varying(128) NOT NULL,
  extension character varying(6) NOT NULL,
  date_download timestamp(0) without time zone DEFAULT now()
);


ALTER TABLE oos_files OWNER TO mylogin;

--
-- Name: oos_log_id_seq; Type: SEQUENCE; Schema: public; Owner: mylogin
--

CREATE SEQUENCE oos_log_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


ALTER TABLE oos_log_id_seq OWNER TO mylogin;

--
-- Name: oos_log; Type: TABLE; Schema: public; Owner: mylogin
--
DROP TABLE IF EXISTS oos_log;

CREATE TABLE oos_log (
  id bigint DEFAULT nextval('oos_log_id_seq'::regclass) NOT NULL,
  date timestamp(0) without time zone DEFAULT now(),
  ip character varying(15),
  action character varying(32) NOT NULL,
  params json NOT NULL,
  source character varying(16),
  status integer,
  details json
);


ALTER TABLE oos_log OWNER TO mylogin;

--
-- Data for Name: migration; Type: TABLE DATA; Schema: public; Owner: mylogin
--

COPY migration (version, apply_time) FROM stdin;
m000000_000000_base	1489536860
m161129_093009_oos_files_table	1489536861
m161129_103512_oos_log	1489536861
\.


--
-- Data for Name: oos_files; Type: TABLE DATA; Schema: public; Owner: mylogin
--

COPY oos_files (id, filename, extension, date_download) FROM stdin;
\.


--
-- Data for Name: oos_log; Type: TABLE DATA; Schema: public; Owner: mylogin
--

COPY oos_log (id, date, ip, action, params, source, status, details) FROM stdin;
\.


--
-- Name: oos_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mylogin
--

SELECT pg_catalog.setval('oos_log_id_seq', 40, true);


--
-- Name: migration_pkey; Type: CONSTRAINT; Schema: public; Owner: mylogin
--

ALTER TABLE ONLY migration
  ADD CONSTRAINT migration_pkey PRIMARY KEY (version);


--
-- Name: oos_files_primary_key; Type: CONSTRAINT; Schema: public; Owner: mylogin
--

ALTER TABLE ONLY oos_files
  ADD CONSTRAINT oos_files_primary_key PRIMARY KEY (id);


--
-- Name: oos_files_date_download_idx; Type: INDEX; Schema: public; Owner: mylogin
--

CREATE INDEX oos_files_date_download_idx ON oos_files USING btree (date_download);


--
-- Name: oos_files_filename_idx; Type: INDEX; Schema: public; Owner: mylogin
--

CREATE INDEX oos_files_filename_idx ON oos_files USING btree (filename);


--
-- Name: oos_log_pkey; Type: INDEX; Schema: public; Owner: mylogin
--

CREATE INDEX oos_log_pkey ON oos_log USING btree (id);


--
-- Name: oos_log_trigger; Type: TRIGGER; Schema: public; Owner: mylogin
--

CREATE TRIGGER oos_log_trigger BEFORE INSERT ON oos_log FOR EACH ROW EXECUTE PROCEDURE oos_log_trigger();


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: oos_files; Type: ACL; Schema: public; Owner: mylogin
--

REVOKE ALL ON TABLE oos_files FROM PUBLIC;
REVOKE ALL ON TABLE oos_files FROM mylogin;
GRANT ALL ON TABLE oos_files TO mylogin;


--
-- Name: oos_log_id_seq; Type: ACL; Schema: public; Owner: mylogin
--

REVOKE ALL ON SEQUENCE oos_log_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE oos_log_id_seq FROM mylogin;
GRANT ALL ON SEQUENCE oos_log_id_seq TO mylogin;


--
-- Name: oos_log; Type: ACL; Schema: public; Owner: mylogin
--

REVOKE ALL ON TABLE oos_log FROM PUBLIC;
REVOKE ALL ON TABLE oos_log FROM mylogin;
GRANT ALL ON TABLE oos_log TO mylogin;


--
-- PostgreSQL database dump complete
--
