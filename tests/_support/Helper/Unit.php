<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Unit extends \Codeception\Module
{
    public function invokeMethod($object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    public function invokeParameter($object, $propName, $value)
    {
        $reflection = new \ReflectionClass(get_class($object));
        $prop = $reflection->getProperty($propName);
        $prop->setAccessible(true);
        $prop->setValue($object, $value);
    }

    public function invokeGetParameter($object, $propName)
    {
        $reflection = new \ReflectionClass(get_class($object));
        $prop = $reflection->getProperty($propName);
        $prop->setAccessible(true);

        return $prop->getValue($object);
    }
}
