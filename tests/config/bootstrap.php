<?php

use Phalcon\Di;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Micro;

error_reporting(E_ALL);

if (!defined('BASE_PATH')) {
    define('BASE_PATH', dirname(dirname(__DIR__)));
}
if (!defined('TEST_PATH')) {
    define('TEST_PATH', BASE_PATH . '/tests');
}
if (!defined('APP_PATH')) {
    define('APP_PATH', BASE_PATH . '/app');
}

/**
 * The FactoryDefault Dependency Injector automatically registers the services that
 * provide a full stack framework. These default services can be overidden with custom ones.
 */
$di = new FactoryDefault();

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include TEST_PATH . "/config/config.php";
});

/**
 * Get config service for use in inline setup below
 */
$config = $di->getConfig();

/**
 * Include Autoloader
 */
$loader = new \Phalcon\Loader();

$loader->registerDirs(
    [
        TEST_PATH . DIRECTORY_SEPARATOR . 'traits'
    ]
)->register();

$loader->registerNamespaces(
    [
        "Plugins" => $config->application->pluginsDir,
        "Lib" => $config->application->libDir,
        "Middleware" => $config->application->middlewareDir,
        "Models" => $config->application->modelsDir,
        "Services" => $config->application->servicesDir,
        "Services\\Exceptions" => $config->application->servicesExceptionsDir,
        "Services\\Interfaces" => $config->application->servicesInterfacesDir,
        "Services\\Validators" => $config->application->servicesValidatorsDir,
    ]
)->register();

/**
 * Include Services
 */
include APP_PATH . '/init/services.php';

/**
 * Starting the application
 * Assign service locator to the application
 */
$di->reset();

$app = new Micro($di);

$app->post('/get_ftp_data', function () use ($app) {
    return true;
});

Di::setDefault($di);

return $app;


