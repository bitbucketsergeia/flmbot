<?php

use \Phalcon\Di;
use \Phalcon\Logger\Adapter\Syslog;
use PHPUnit\Framework\TestCase;

class SysLoggerTest extends TestCase
{
    /**
     * @var Syslog;
     */
    static protected $sysLogger;

    protected function setUp()
    {
        $this->getSysLogger();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    static public function getSysLogger()
    {
        if (!self::$sysLogger) {
            $config = Di::getDefault()->getConfig();
            self::$sysLogger = new Syslog('oosexchanger', $config->get('sysLogger'));
        }

        return self::$sysLogger;
    }

    public function testLog()
    {
        $result = self::$sysLogger->log(LOG_WARNING, 'Message');
        $this->assertEquals(true, $result instanceof Syslog);
    }
}