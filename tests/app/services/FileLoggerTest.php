<?php

use \Phalcon\Di;
use \Phalcon\Logger\Adapter\File;
use PHPUnit\Framework\TestCase;

class FileLoggerTest extends TestCase
{
    /**
     * @var File;
     */
    static protected $fileLogger;

    protected function setUp()
    {
        $this->getFileLogger();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    static public function getFileLogger()
    {
        if (!self::$fileLogger) {
            $config = Di::getDefault()->getConfig();
            $config = $config->get('fileLogger');
            
            if (!is_dir($config->logDir)) {
                @mkdir($config->logDir);
            }
            self::$fileLogger = new File($config->logDir . DIRECTORY_SEPARATOR . $config->logFile);
        }

        return self::$fileLogger;
    }

    public function testLog()
    {
        $result = self::$fileLogger->log(LOG_WARNING, 'Message');
        $this->assertEquals(true, $result instanceof File);
    }
}