<?php

use \Phalcon\Di;
use \Phalcon\Logger\Adapter\Syslog;
use PHPUnit\Framework\TestCase;
use Services\ErrorLogger;

class ErrorLoggerTest extends TestCase
{
    protected function setUp()
    {
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    public function testLogWithoutSyslog()
    {
        $logger = new ErrorLogger(null, FileLoggerTest::getFileLogger());
        try {
            throw new Exception('Test exception');
        } catch (\Exception $e) {
            $result = $logger->logException($e);
            $this->assertEquals(ErrorLogger::class, get_class($result));
        }
        $result = $logger->log(LOG_WARNING, 'test');

        $this->assertEquals(ErrorLogger::class, get_class($result));
    }

    public function testLogWithoutFileLogger()
    {
        $logger = new ErrorLogger(SysLoggerTest::getSysLogger(), null);
        try {
            throw new Exception('Test exception');
        } catch (\Exception $e) {
            $result = $logger->logException($e);
            $this->assertEquals(ErrorLogger::class, get_class($result));
        }
        $result = $logger->log(LOG_WARNING, 'test');

        $this->assertEquals(ErrorLogger::class, get_class($result));
    }

    public function testLogWithoutAnyLogger()
    {
        $logger = new ErrorLogger(null, null);
        try {
            throw new Exception('Test exception');
        } catch (\Exception $e) {
            $result = $logger->logException($e);
            $this->assertEquals(ErrorLogger::class, get_class($result));
        }
        $result = $logger->log(LOG_WARNING, 'test');

        $this->assertEquals(ErrorLogger::class, get_class($result));
    }

}