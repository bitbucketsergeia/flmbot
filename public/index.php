<?php

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Micro;

error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

try {

    /**
     * The FactoryDefault Dependency Injector automatically registers the services that
     * provide a full stack framework. These default services can be overidden with custom ones.
     */
    $di = new FactoryDefault();

    /**
     * Shared configuration service
     */
    $di->setShared('config', function () {
        return include APP_PATH . "/config/config.php";
    });

    /**
     * Get config service for use in inline setup below
     */
    $config = $di->getConfig();

    /**
     * Include Autoloader
     */
    include APP_PATH . '/init/loader.php';

    /**
     * Include composer autoloader
     */
    require BASE_PATH . "/vendor/autoload.php";

    /**
     * Include Services
     */
    include APP_PATH . '/init/services.php';

    /**
     * Starting the application
     * Assign service locator to the application
     */
    $app = new Micro($di);

    /**
     * Include Application
     */
    include APP_PATH . '/app.php';

    /**
     * Handle the request
     */
    $app->handle();

} catch (\Throwable $e) {
    if ($app) {
        $errLogger = $app->getDI()->get('errorLogger');
    } else {
        echo $e->getMessage() . '<br>';
        echo '<pre>' . $e->getTraceAsString() . '</pre>';
    }

    if ($errLogger) {
        $errLogger->logException($e);
    } else {
        $app->getDI()->get('fileLogger')->error($e->getMessage());
    }
    $app->response->setJsonContent(['status' => STATUS_ERR, 'error' => 'Unknown error'])->send();
}
